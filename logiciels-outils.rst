..  _intro-logiciels-outils:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Logiciels, présentation et installation
==============================================

Dans cette fiche nous présenterons succinctement quelques logiciels et outils couramment utilisés en géomatique, aussi bien en SIG qu'en télédétection. Il s'agît de logiciels libres, gratuits et multi-plateformes que vous pouvez utiliser sans aucun restriction dans vos études, projets de recherche et projets professionnels.

Les logiciels libres ne sont pas des clones de leurs équivalents propriétaires et payants. Certaines capacités des logiciels payants ne s'y retrouvent pas, mais certaines autres leurs sont spécifiques. C'est au final à l'utilisateur de faire son choix.

Après la présentation des logiciels, les différentes procédures d’installation sont expliquées. L'installation d'extension est également présentée à la fin de cette partie.

.. contents:: Table des matières
    :local:

..  _logiciels-QGIS:

QGIS
------

`QGIS`_ (:numref:`interface-qgis`) est le logiciel phare de la géomatique libre. Très orienté pour les traitements vecteurs à l'origine, sa capacité de traitements rasters tend à se développer depuis quelques années. Des informations et les liens de téléchargement sont disponibles sur le site officiel.

En plus de nombreuses capacités intrinsèques, il est possible détendre QGIS en lui adjoignant des centaines d'extensions également gratuites. De plus, QGIS est maintenant capable d'interfacer d'autres logiciels plus orientés pour les traitements rasters comme GRASS GIS, SAGA GIS et Orfeo Toolbox. L'intégration de ces logiciels est inégale. Les modules de GRASS GIS sont inclus par défaut et fonctionnent bien. Les modules de SAGA GIS le sont aussi, mais correspondent à une ancienne version de SAGA. Enfin, l'intégration de Orfeo Toolbox est plus ou moins facile à paramétrer selon l'installation de l'utilisateur.

Une autre force de QGIS est de pouvoir très facilement interfacé une base de données PostGIS. Notons également que QGIS est basé en partie sur Python. Il est possible de créer son propre bout de code Python pour automatiser des tâches dans QGIS ou même sa propre extension. Si le code n'est pas votre spécialité, il existe un interface graphique pour construire ces propres chaînes de traitements (*model builder*).

Il y a une grande communauté d'utilisateurs de QGIS, ce qui fait que ce logiciel est relativement bien documenté et surtout nous trouvons, la plupart du temps, la réponse à d'éventuelles questions par une simple recherche sur le Web. Enfin, l'interface se veut ergonomique et est plutôt aboutie (:numref:`interface-qgis`), même si les goûts et les couleurs sont un vaste débat.

.. figure:: figures/fig_interface_qgis.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: interface-qgis
    
    Interface général de QGIS.

.. tip::
	L'UMR Passages a mis en ligne une `très grande documentation spécifiquement dédiée à QGIS`_. Vous pourrez y retrouver de nombreux tutoriels très pédagogiques sur son utilisation pour différentes applications.

..  _logiciels-SCP:

Module SCP dans QGIS
**********************

Bien que le module *Semi-Automatic Classification Plugin* (`SCP`_) ne soit pas un logiciel indépendant, c'est un module si riche de fonctionnalités qu'il a sa place ici. Il s'agît d'une extension à ajouter dans QGIS qui permet de faire des classifications supervisées d'images de télédétection. Ce module s'installe comme une extension classique.

Par contre, contrairement à la plupart des extensions, il ajoute de très nombreux outils. C'est quasiment un logiciel dans le logiciel. Ces outils sont parfaits pour faire de la télédétection sans quitter QGIS et sans passer par un outil tiers interfacé (comme GRASS GIS ou SAGA). Le cœur du module est de proposer des techniques classiques de classification supervisée (:numref:`plugin-scp`), basées sur les méthodes *Plus proche voisin*, *Maximum de vraisemblance* ou *Spectral angle mapping*. Les résultats sont très bons et la vitesse d'exécution est raisonnable. Un autre avantage de ce module est de proposer des outils connexes très pratiques. Nous y retrouvons la possibilité de générer les signatures spectrales de nos polygones d'apprentissage, de reclassifier un raster, de produire une matrice de validation, de découper en lot des bandes spectrales et même de télécharger des images sources (Landsat, MODIS, GOES ou Sentinel-1, 2 et 3).

Il est également possible de faire des classifications non supervisées de type *k-means* ou d'autres classifications supervisées plus complexes faisant appel à l'algorithme *random forest*. Pour utiliser cette fonctionnalité il est nécessaire d'avoir installé au préalable la `SNAP toolbox`_ de l'ESA.

Le seul point négatif de ce module est son interface beaucoup trop touffue à mon sens. Il est facile de se perdre dans les menus et sous menus, les cases à cocher, les barres d'outils, les icônes, les onglets... Mais si on dépasse cet aspect, on est en possession d'un outil très intéressant en restant dans QGIS. De plus, ce module est maintenu de façon soutenue et évolue très vite. De nouvelles fonctionnalités apparaissent régulièrement.

.. figure:: figures/fig_plugin_SCP.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: plugin-scp
    
    Classification supervisée dans SCP.


..  _installation_QGIS:

Installation de QGIS
***********************

Nous allons voir dans cette fiche comment installer le logiciel libre de SIG QGIS, sous Windows et sous Linux (Ubuntu / Debian). Nous verrons également comment installer des outils complémentaires.

.. contents:: Table des matières
    :local:

Installation de QGIS dans Windows
++++++++++++++++++++++++++++++++++++

La première chose à faire est d'aller sur le site de `QGIS`_. Sur la page d'accueil nous cliquons sur *Téléchargez*. Nous allons ensuite dans la rubrique *Téléchargement pour Windows*. Il existe deux façons d'installer QGIS : une façon rapide avec une configuration de base (largement suffisante dans la plupart des cas), et une façon personnalisable.

Façon rapide
++++++++++++++

Pour cette installation rapide, nous allons dans partie du *Téléchargement pour Windows* nommée *Installateurs autonomes (MSI) depuis les paquets OSGeo4W (recommandé pour les nouveaux utilisateurs)*. Nous avons alors le choix entre la toute dernière version ou une version long terme. Nous pouvons généralement choisir la dernière version afin de profiter des dernières avancées. Le fichier téléchargé est un exécutable au format *.msi*.

.. tip::

	Si QGIS vous paraît instable, vous pouvez installer la version long terme *LTR* (*Long Term Release*). Cette version LTR est plus stable mais en contrepartie moins fournie que la dernière version.

Une fois l'exécutable téléchargé, il suffit de le lancer en double cliquant sur le fichier *.msi*. Ensuite, classiquement, nous cliquons sur :guilabel:`Accepter` et autres :guilabel:`Installer`. Le processus prend plus ou moins de temps selon les ordinateurs. Ensuite QGIS peut être lancé via l'icône éventuellement créée sur le *Bureau* ou en le recherchant dans le menu *Démarrer*.

.. note::

	Par défaut, QGIS est installé avec les outils GRASS, d'où la présence de *with GRASS* dans le nom de l'installation. GRASS GIS est un autre logiciel de géomatique qui peut ainsi être utilisé de façon transparente dans QGIS. Cet interfaçage d'outils externes démultiplie les traitements possibles avec QGIS.

Nous pouvons maintenant ouvrir et utiliser QGIS. Nous constatons dans la *Boîte à outils de traitements* la présence des outils GRASS mais aussi GDAL et SAGA (:numref:`grass-saga`).

.. figure:: figures/fen_grass_saga_qgis.png
    :width: 300px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: grass-saga
    
    Présence des outils GRASS, GDAL et SAGA dans QGIS.

.. tip::

	Si au lancement de QGIS, vous vous apercevez que le menu *Raster* est incomplet et/ou que le menu *Vecteur* est vide il n'est pas nécessaire de tout réinstaller. Généralement, en allant dans le menu :menuselection:`Extensons --> Installer/Gérer les extensions` puis en allant dans l'onglet :guilabel:`Installées`, il faut cocher (ou décocher et recocher) l'extenson *Processing*. Suite à ça, les menus *vecteur* et *Raster* devraient être maintenant complets.


Façon personnalisable
+++++++++++++++++++++++

Pour sélectionner finement la version de QGIS à installer et les différentes briques sous-jacentes il faut télécharger *l'installateur réseau OSgeo4W*. Ensuite en lançant cet installateur et en choisissant l'installation avancée, il est possible de choisir la version de QGIS, les différentes librairies et même d'installer des logiciels tierces.

À ce stade nous avons une version de QGIS tout à fait suffisante pour la plupart des utilisations. Cependant, certains modules supplémentaires auront besoin de *briques* logicielles qui ne sont pas comprises dans la version de base. Dans cet exemple, nous allons installer une librairie Python nommée *Rasterio* qui permet de manipuler des données rasters.

Nous retournons dans le menu :guilabel:`Démarrer` > :guilabel:`QGIS X.XX` (où *X.XX* est la version installée et nous trouvons une entrée nommée :guilabel:`Setup` (:numref:`entree-setup`).

.. figure:: figures/install_osgeo_setup.png
    :width: 300px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: entree-setup
    
    Installation de modules complémentaires.

C'est cette entrée qui va nous permettre d'installer le module manquant.

.. warning::

	Il faut lancer ce *Setup* en mode *Administrateur*.

Dans la fenêtre qui s'affiche, nous choisissons :guilabel:`Advanced Install` puis :guilabel:`Suivant`. Ensuite, nous pouvons cliquer sur :guilabel:`Suivant` pendant quelques fenêtres jusqu'à parvenir à la fenêtre suivante (:numref:`choix-depot`).

.. figure:: figures/install_osgeo_site.png
    :width: 500px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: choix-depot
    
    Choix d'un dépôt distant.

Ici, nous choisissons sur quel site nous allons télécharger le module souhaité. Ça n'a aucune importance, nous pouvons sélectionner le premier dépôt et cliquer sur *Suivant*. Nous arrivons alors à la fenêtre qui nous intéresse vraiment. C'est ici que nous pourrons choisir les modules à installer.

Si nous déplions le menu :guilabel:`Desktop`, nous retrouvons les logiciels installés (:numref:`menu-desktop`).

.. figure:: figures/install_osgeo_desktop.png
    :width: 600px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: menu-desktop
    
    Les logiciels Desktop installés.

Ici, nous voyons que nous avons déjà installés GRASS GIS 7.8.5-1, QGIS 3.16.2-1 et SAGA 2.3.2-4. Nous pouvons passer par ici pour désinstaller ou mettre à jour les logiciels en cliquant sur :guilabel:`Keep`. Si nous voulions installer le dernier logiciel nommé *tora*, il suffirait de cliquer sur :guilabel:`Skip` et de faire apparaître la version à installer.

Dans la cas qui nous intéresse, nous n'allons pas installer un nouveau logiciel mais seulement ce que nous appelons un *Libs* (une *librairie*). Nous déplions donc l'onglet correspondant. De très nombreuses librairies sont disponibles, il nous faut trouver celle qui nous intéresse. Elles sont rangées par ordre alphabétique, nous regardons donc à *python3-rasterio* (:numref:`osgeo-rasterio`).

.. figure:: figures/install_osgeo_rasterio.png
    :width: 600px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osgeo-rasterio
    
    Installation de rasterio avec OsGeo.

Pour l'instant, la ligne correspondant est à :guilabel:`Skip` (passer), il suffit de cliquer sur ce *Skip* pour faire apparaître le numéro de version de la librairie qui sera installée (:numref:`install-version`).

.. figure:: figures/install_osgeo_rasterio_version.png
    :width: 600px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: install-version
    
    Version de la librairie installée.

Ici, le *Skip* s'est changé en *1.1.8-1*, c'est donc cette version qui sera installée. Il suffit ensuite de cliquer plusieurs fois sur *Suivant* et la librairie souhaitée est installée.

.. warning::

	Toutes les librairies ne sont malheureusement pas disponibles via cet installeur.


..  _extension-qgis:

Installation d'extensions dans QGIS
*************************************
Version de QGIS : 3.16.1

QGIS propose de très (très) nombreuses extensions supplémentaires librement téléchargeables et utilisables. Il est très facile de les ajouter afin de les utiliser directement depuis QGIS. Pour commencer, il est plus facile de connaître par avance le nom de l'extension à télécharger. Ce nom peut se trouver en faisant une recherche sur le web. Dans cet exemple, nous allons télécharger et installer l'extension nommée *Cadastre* qui permet de manipuler les données cadastrales françaises.

Pour l'installer, nous allons dans le menu :guilabel:`Extensions` > :guilabel:`Installer/gérer les extensions`. La fenêtre de
gestion des extensions apparaît alors (:numref:`extension-install`).

.. figure:: figures/fen_install_extension_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: extension-install
    
    Installation d'une extension dans QGIS.

.. tip::

	Dans l'onglet :guilabel:`Paramètres` vous pouvez ajouter un dépôt d'extensions si vous possédez son URL et vous pouvez également lister les extensions encore expérimentales.

Entrez le nom de l’extension recherchée ou un mot clé dans la barre de texte, ici :guilabel:`cadastre`. Puis sélectionnez la parmi les extensions filtrées. Une description s’affiche avec éventuellement un lien vers une page web décrivant le fonctionnement de l’extension et d'autres informations. Cliquez ensuite sur :guilabel:`Installer le plugin`. Une fois l’installation effectuée, l’extension est accessible dans le menu :guilabel:`Extensions` ou sous forme d’un menu autonome.

.. warning::

	La multitude des extensions et la diversité de la communauté les développant est un point fort mais également un point faible. En effet, les extensions seront plus ou moins traduites en français, certaines ne seront pas stables et beaucoup n'auront malheureusement pas de documentation détaillée. À utiliser sans modérations donc, mais avec un regard critique.


..  _logiciels-GDAL:

GDAL/OGR
-----------

`GDAL`_ n'est ni un logiciel ni un langage de programmation. GDAL est en fait une collections de codes informatiques qui permet de faire des traitements rasters de base. Il est également possible de faire des traitements vecteur avec la partie de GDAL appelée OGR.

.. _GDAL: https://gdal.org/

GDAL est utilisable directement en lignes de commandes. L’apprentissage de la syntaxe spécifique à GDAL est relativement facile et manier ces commandes peut s'avérer utile et pratique. Cependant, sans le savoir nous utilisons GDAL tout le temps lorsque nous utilisons les outils de géomatique libres. Les briques GDAL sont en effet à la base de très nombreux traitements dans d'autres logiciels, et notamment dans QGIS. D'ailleurs lorsque GDAL est implicitement utilisé dans QGIS, souvent la ligne de commande correspondante est visible. C'est très pratique lorsque nous souhaitons utiliser GDAL seul en lignes de syntaxe. Nous avons ainsi un exemple de syntaxe sur lequel baser notre script personnalisé.

..  _installation-GDAL:

Installation de GDAL/OGR
***************************

À l'installation de QGIS, GDAL (:ref:`logiciels-GDAL`) devrait être installé et reconnu automatiquement comme une variable système. Pour le vérifier, ouvrez une console de commandes de votre système d'exploitation et taper la commande suivante qui vous renverra simplement la version de GDAL installé.

.. code-block:: sh

   gdalinfo --version

Si GDAL est bien reconnu dans le *PATH* système, une sortie similaire à celle-ci devrait s'afficher : *GDAL 3.0.4, released 2020/01/28*. Si un commentaire vous disant que la commande n'est pas reconnue, vérifiez votre installation de GDAL selon votre système d'exploitation. Vérifiez également que GDAL est reconnu dans le *PATH* du système, selon que vous `utilisez Windows`_ ou `Ubuntu`_.

Une fois GDAL installé et paramétré, il s'utilise en lignes de commandes, assez simples à construire. La `documentation en ligne est bien faite pour s'y retrouver`_. Le plus simple est de se placer dans son répertoire de travail pour exécuter une commande GDAL. Mais il est également possible d'accéder aux fichiers de travail en indiquant leurs chemins relatifs ou absolus.

Par exemple, si nous souhaitons obtenir les métadonées d'un fichier raster nommé *B5_L93.tif*, nous pouvons ouvrir le répertoire le contenant puis y ouvrir une console. Ainsi, la console est bien localisée au niveau du raster et nous pouvons directement y entrer la commande dédiée, avec l'option *-stats* pour afficher les statistiques du raster en plus du reste.

.. code-block:: sh

   gdalinfo -stats B5_L93.tif

Dans la console, ça ressemble à la figure suivante (:numref:`gdalinfo1`).

.. figure:: figures/fig_gdalinfo_1.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gdalinfo1
    
    Commande GDAL dans le même répertoire que le fichier raster à traiter.

Si la commande ne s'exécute pas depuis le même répertoire que le raster, il est possible de pointer vers le raster via son chemin relatif (:numref:`gdalinfo2`).

.. figure:: figures/fig_gdalinfo_2.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gdalinfo2
    
    Commande GDAL avec chemin relatif.

C'est enfin possible d'exécuter GDAL en pointant vers un raster via son chemin absolu (:numref:`gdalinfo3`).

.. figure:: figures/fig_gdalinfo_3.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gdalinfo3
    
    Commande GDAL avec chemin absolu.

..  _cmd_gdal_python:

Lancer GDAL avec Python
+++++++++++++++++++++++++++

Une fois GDAL bien configuré, il est possible d'appeler une commande GDAL depuis un script Python. La solution présentée ici n'est pas très pythonesque et certains pourraient penser qu'elle tient du bricolage, mais c'est néanmoins une astuce bien pratique. Cette astuce consiste à construire une chaîne de caractères, i.e. un *string* Python, et de le faire exécuter comme une commande système grâce à la libraire *os*. Le string de la commande se construit par concaténation. Par exemple, la commande précédente incluse dans un script Python peut ressembler à ceci.

.. code-block:: Python

   # import des commandes os
   import os

   # le chemin vers le fichier à reprojeter
   src = '/home/poulpe/SIG/Demo/Demo_Python/Landsat_13/B5_L93.tif'

   # construction du string de la commande
   cmd = 'gdalinfo -stats ' + src

   # exécution de la commande
   os.system(cmd)

Le tout reste propre et lisible et fonctionne bien. Il faut juste bien faire attention aux espaces dans la commande.

À savoir qu'il existe également une API Python de GDAL afin d'utiliser GDAL de façon plus proprement intégré à Python.

..  _logiciels-grass:

GRASS GIS
------------

`GRASS GIS`_ (:numref:`session-grass`) est un logiciel de géomatique existant depuis très longtemps et plutôt orienté vers des traitements rasters, même si ses fonctionnalités vecteurs sont finalement très riches. À l'origine il ne tournait que sous Linux mais maintenant il fonctionne sans aucun problème sur Windows. Ce logiciel a une réputation d'être réservé aux geeks car à l'origine il ne s'utilisait qu'en lignes de commandes. Aujourd'hui, les lignes de commande sont toujours possibles mais il existe également une interface graphique tout à fait honnête (:numref:`session-grass`).

.. figure:: figures/fig_session_grass.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: session-grass
    
    Une session de travail sous GRASS GIS.

Le côté reboutant de son utilisation vient du fait que ce logiciel oblige l'utilisateur à structurer son projet dans une base de données GRASS. Même si il est extrêmement simple de construire cette base de données via l'interface graphique, ce côté continue à repousser les utilisateurs non avertis.

Comme dit précédemment, GRASS GIS est interfaçable par QGIS. Les deux logiciels fonctionnent très bien ensemble. Cependant, certains modules spécifiques de GRASS ne sont pas interfacés. Il peut donc être utile de travailler directement avec le logiciel en lui-même. Pour les utilisateurs avertis, la possibilité de travailler en lignes de commandes peut faire gagner un temps certain.

GRASS GIS, comme QGIS, gère Python et l'utilisateur peut y adjoindre ses propres codes pour automatiser ses tâches. Notons enfin que la documentation officielle est bien faite. Par contre, la communauté est moins nombreuse et il peut être plus compliqué que pour QGIS de trouver les réponses à ses questions.


..  _logiciels-SAGA:

SAGA GIS
--------------

`SAGA GIS`_ (:numref:`session-saga`) est un autre logiciel de géomatique plutôt orienté pour les traitements rasters. C'est un logiciel très complet dont les algorithmes sous-jacents fonctionnent bien et plutôt rapidement. Comme GRASS il peut s'utiliser aussi bien en lignes de commandes que via son interface graphique. Il faut avouer que les développeurs n'ont pas tout misé sur cet interface qui peut apparaître quelque peu vintage pour les années 2020 (:numref:`session-saga`).

.. figure:: figures/fig_session_saga.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: session-saga
    
    Une image Landsat 8 dans SAGA GIS.

Ce logiciel peut s'avérer très utile dans certains cas. Sa sous utilisation malgré ses capacités très intéressantes vient sûrement du fait de sa (très) mauvaise documentation. L'utilisateur est un peu lâché face à lui même dans l'utilisation des différents modules. Une documentation plus fournie, gage de confiance, pourrait propulser ce logiciel.


..  _logiciels-OTB:

Orfeo ToolBox
----------------

`Orfeo ToolBox`_ (OTB) (:numref:`session-otb`) est un logiciel initié par le CNES (*Centre National d'Études Spatiales*), l'agence spatiale française. C'est un logiciel entièrement dédié à la télédétection et à toutes ses composantes. Nous y retrouvons des outils pour les images de télédétection optiques, radar, à très haute résolution spatiale... Les algorithmes sur lesquels le logiciel reposent sont très puissants et optimisés pour tourner rapidement même sur des ordinateurs modestes.

OTB est utilisable en ligne de commande, via Python, via QGIS (au prix d'un paramétrage plus ou moins complexe) ou via un interface graphique dédié nommé *Monteverdi* (:numref:`session-otb`). Ce dernier choix est le plus simple pour l'utilisateur classique.

.. figure:: figures/fig_session_otb.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: session-otb
    
    Une ortho-photo chargée dans Orfeo ToolBox via l'interface Monteverdi.

La force de OTB est de proposer des outils très performants, notamment pour la segmentation d'images ou les classifications (Kmeans, Random forest, SVM, ...). La documentation officielle est complète mais pas toujours très *user friendly*. Ce logiciel mériterait d'être plus utilisé. Sa communauté reste encore faible et les questions risquent de ne pas trouver réponse facilement sur le Web. Sa prise en mains n'est pas forcément très évidente, mais au final, pour la télédétection, c'est un peu la Rolls Royce des logiciels.

..  _installation_OTB:

Installation de Orfeo Toolbox
********************************

Nous allons voir dans cette fiche comment installer le logiciel libre de télédétection Orfeo Toolbox abrégé en OTB.

Installation d'Orfeo ToolBox dans Windows
+++++++++++++++++++++++++++++++++++++++++++++
Ici nous allons voir comment installer le logiciel de télédétection Orfeo ToolBox (OTB) dans Windows. Pour rappel, OTB est en fait une "collection" de modules (appelés *Applications* dans la terminologie OTB) réalisant chacun une tâche spécifique (classification, calcul raster, segmentation, ...). Il est ensuite possible d'accéder à cette collection de modules via des lignes de commandes ou des interfaces graphiques. Une interface graphique nommée *Monteverdi* est fournie par défaut en même temps que les modules proprement dits. Mais il est également possible d'interfacer les modules directement depuis QGIS si nous souhaitons rester dans ce seul logiciel.

Comme OTB est distribué sous licence libre, il suffit de le télécharger depuis le `site officiel de OTB`_. Une fois sur la page, la dernière version est disponible sous l'icône *Download OTB X.X.X* où *X.X.X* correspond à la dernière version disponible. Nous choisissons ensuite la version correspondant à notre OS, ici Windows (:numref:`otb-site`). Notons que des versions Linux et Mac existent également.

.. figure:: figures/fig_otb_telechargement.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: otb-site
    
    Téléchargement de Orfeo ToolBox.

.. note::
	À priori maintenant tous les ordinateurs tournent en 64 bits, vous pouvez donc certainement prendre cette version.

Le gros avantage de OTB est de ne pas nécessiter d'installation. En effet, une fois l'archive téléchargée et décompressée, nous pouvons directement lancer l'interface *Monteverdi* et utiliser les modules d'OTB. C'est notamment pratique lorsque nous n'avons pas les droits administrateurs de la machine.

.. tip::
	Le logiciel de décompression par défaut de Windows n'est pas toujours des plus rapides. Il est possible d'utiliser le logiciel gratuit `7zip`_ qui fonctionne très bien.

Une fois l'archive décompressée, il suffit d'y entrer et de lancer l'interface *Monteverdi* en double-cliquant sur *monteverdi.bat*. L'interface de Monteverdi apparaît alors.

..  _otbcli_windows:

Utilisation de OTB en lignes de commandes dans Windows
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Une des forces de OTB est de pouvoir s'utiliser en lignes de commandes. C'est notamment très pratique lorsque nous souhaitons créer des scripts d'automatisation ou des bouts de programmes. La première chose à faire est de régler le *PATH* en y ajoutant le répertoire qui contient les différents modules. Ce répertoire se trouve dans le sous répertoire *bin* du répertoire principal de OTB. Imaginons que nous ayons stocké le répertoire téléchargé et décompressé directement sur le disque *C:*, le répertoire en question est : *C:\\OTB-7.3.0-Win64\bin*. Si nous l'ouvrons dans un explorateur de fichiers, nous trouvons bien tous les modules, qui correspondent aux fichiers *.bat* commençant par *otbcli_*. Pour rappel, la procédure pour ajouter un répertoire au *PATH* de Windows est `décrite sur ce site`_. Attention de ne pas effacer son PATH mais simplement d'ajouter le chemin vers les modules de OTB à la fin du PATH existant.

Une fois le PATH réglé, nous ouvrons une console Windows (vous gagnez alors 100 points de geeks !). Pour ouvrir cette console, dans le menu *Démarrer* vous chercher *cmd* et vous lancez le *cmd.exe* qui vous est proposé. Une fois la console ouverte vous pouvez par exemple tenter de lancer le module qui permet de faire des segmentations d'images, nommé *Segmentation*. Par défaut, les modules se lancent en entrant *otbcli_* (comme *OTB Command Line Interface*) suivi du nom du module désiré. Ici nous entrons donc :

.. code-block:: bash

   otbcli_Segmentation

Il suffit d'appuyer sur *Entrée* pour lancer la commande. Si tout est bien réglé, le module est bien lancé mais il vous est spécifié que des paramètres sont manquants et une liste des paramètres demandés s'affiche (:numref:`otbcli-windows`).

.. figure:: figures/fig_otbcli_windows.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: otbcli-windows
    
    Exécution d'une ligne de commande OTB dans une invite de commandes Windows.

Par exemple, pour calculer un NDVI à partir d'un raster multi-bandes disposant d'une bande infrarouge et d'une bande rouge, la commande à entrer serait :

.. code-block:: bash

   otbcli_BandMath -il xtr_dalle1.tif -exp "(im1b1 - im1b2)/(im1b1 + im1b2)" -out ndvi_dalle_1.tif

L'utilisation des commandes sera détaillée à chaque présentation de modules tout au long de cette documentation.

..  _OTB-Ubuntu:

Installation d'Orfeo ToolBox dans Ubuntu/Debian
++++++++++++++++++++++++++++++++++++++++++++++++++
La procédure est exactement la même que pour l'installation dans Windows sauf qu'il faut sélectionner l'icône *Linux* sur la page de téléchargement (:numref:`otb-site`). Une fois l'archive téléchargée et décompressée, OTB est utilisable directement, il n'est pas nécessaire d'installer un package en *.deb*. Pour lancer l'interface *Monteverdi*, il suffit d'aller dans le répertoire téléchargé et décompressé et de lancer le processus *monteverdi.sh* en double cliquant dessus. Il faut préciser que nous souhaitons l'exécuter et Monteverdi se lance.

Utilisation de OTB en lignes de commandes dans Linux
+++++++++++++++++++++++++++++++++++++++++++++++++++++++
Contrairement à Windows, Linux est très bien conçu pour être utilisé en lignes de commandes. Il est donc encore plus simple d'utiliser OTB en lignes de commandes sous cet OS. Il est simplement nécessaire d'installer le package *otb-bin* via la commande suivante :

.. code-block:: sh

   sudo apt-get install otb-bin

Le PATH est réglé automatiquement et les commandes peuvent maintenant être lancées. La syntaxe est la même que nous soyons sous Windows ou Linux.

Interfaces graphiques pour OTB
+++++++++++++++++++++++++++++++++
Comme dit précédemment, les modules (ce que les développeurs de OTB appellent les *applications*) peuvent être interfacées via Monteverdi ou QGIS.

OTB via Monteverdi
....................
Une fois Monteverdi lancé, l'interface est la même quelque soit son OS. L'interface est en anglais seulement et peut paraître peu sexy au premier abord (:numref:`otb-monteverdi`). L'accent est mis sur l'efficacité.

.. figure:: figures/fen_otb_interface.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: otb-monteverdi
    
    Monteverdi comme interface à Orfeo ToolBox.

Le panneau central est l'emplacement où les rasters sont affichés. En bas, nous avons la *Pile de couche* avec la liste des rasters chargés. Le plus important est en fait le panneau *Navigateur d'OTB-Applications* dans lequel nous retrouvons toutes les applications (i.e. tous les modules) de OTB. Par défaut ce panneau n'est pas affiché. Pour l'afficher nous allons dans le menu :menuselection:`Affichage --> Navigateur d'OTB-Applications`. Pour lancer un module, il suffit de double-cliquer sur son nom dans ce panneau.

OTB via QGIS
................
Une autre possibilité pratique est d'interfacer les applications d'OTB via QGIS. Pour ce faire, il est tout d'abord nécessaire de récupérer le chemin du répertoire OTB téléchargé. Par exemple, dans le cas d'une installation sous Windows, admettons que nous l'ayons directement stocké sur le disque *C:*. Une fois que nous avons ce chemin, nous ouvrons QGIS et nous allons ajouter OTB parmi les *Fournisseurs de traitements*.

Dans la *Boîte à outils de traitements*, nous cliquons sur l'icône ``Options`` |icone_reglages|. La fenêtre suivante apparaît (:numref:`qgis-otb`).

.. figure:: figures/fen_qgis_add_otb.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-otb
    
    Ajout de Orfeo ToolBox comme fournisseur de traitements à QGIS.

Nous déroulons le menu sous la ligne ``OTB``, nous cochons la case ``Activer``, nous réglons le chemin du ``Répertoire des applications OTB`` à ``C:/OTB-7.3.0-Win64/lib/otb/applications`` puis le chemin du ``Répertoire OTB`` à ``C:/OTB-7.3.0-Win64``. Puis nous cliquons sur :guilabel:`OK`. Les outils OTB sont maintenant disponibles depuis la *Boîte à outils de traitements* de QGIS dans le menu ``OTB`` (:numref:`qgis-otb-access`).

.. figure:: figures/fig_qgis_outils_otb.png
    :width: 18em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-otb-access
    
    Les outils OTB dans la *Boîte à outils de traitements* de QGIS.
    
    .. note::
	Évidemment les chemins sont à modifier selon votre installation et votre version de OTB. Si vous êtes sous Ubuntu, le chemin sera du genre */home/user/OTB-7.2.0-Linux64/lib/otb/applications*.

Vous venez alors de décupler les capacités de QGIS, qui devient alors extrêmement puissant.

..  _logiciels-Snap:

SNAP Toolbox
-------------

L'`Agence Spatiale Européenne`_ (*ESA*) a développé une série de boîtes à outils *toolboxes* pour traiter les données produites par le programme spatial européen : les données Sentinel, SMOS et PROBA-V (:numref:`session-snap`). Ces boîtes à outils sont connues sous le nom de `SNAP toolboxes`_ (*Sentinel Application Platform*). Il s'agît de cinq boîtes à outils indépendantes, une pour chacun des `Sentinel-1`_, `2`_ et `3`_, une pour `SMOS`_ et une pour `PROBA-V`_. Ces boîtes à outils sont `distribuées gratuitement`_ sous licence libre et sont multi plateformes. Il est possible de les télécharger une par une ou de télécharger une sorte de super boîte à outils qui les regroupe toutes les cinq. L'installation se fait facilement en choisissant la version qui correspond à votre OS, Windows, Mac ou Linux.

L'interface n'est pas des plus sexy mais les fonctionnalités proposées sont très intéressantes et très puissantes. Par exemple, pour les images optiques, sont proposés des modules de classification non supervisée, de classification supervisée basée sur la méthode de *random forest*, *svm*, ... de pansharpening, de segmentation... Pour les données radar, différents outils d'ortho-rectification, de calcul de polarisation, de filtres ... sont proposés.

Les boîtes à outils sont utilisables via un interface graphique ou via Python, ce qui en fait un outil intéressant dans une optique d'automatisation. Notons également que la classification supervisée basée sur le random forest est interfaçable par QGIS via le plugin SCP.

.. figure:: figures/fig_session_snap.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: session-snap
    
    Une bande Sentinel-2 chargée dans la SNAP Toolbox de l'ESA.

..  _installation_SNAP:

Installation de la SNAP toolbox de l'ESA
******************************************

Nous allons voir dans cette fiche comment installer le logiciel libre de télédétection développé par l'Agence Spatiale Européenne (ESA). Ce logiciel est en fait une série de *boîtes à outils* (*Toolboxes*) dédiées à l'exploitation des images des différentes missions de l'ESA : Sentinel, SMOS ou PROBA-V.

Installation de SNAP dans Ubuntu/Debian
++++++++++++++++++++++++++++++++++++++++++
Ici nous allons voir comment installer les boîtes à outils SNAP sur un ordinateur tournant sous Ubuntu ou Debian. Ces boîtes à outils sont librement téléchargeables sur `le site dédié`_, dans la catégorie *Téléchargements* (:numref:`snap-site`). 

.. figure:: figures/fig_SNAP_download.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: snap-site
    
    Téléchargement des boîtes à outils SNAP.

Nous avons le choix de ne télécharger que les boîtes à outils Sentinel ou SMOS ou toutes à la fois. Quelque soit le choix, nous choisissons la version *Unix 64-bit*. Le fichier téléchargé est un fichier *shell* d'installation nommé *esa-snap_all_unix_8_0.sh* (version 8 au moment de la rédaction de ces lignes). Une fois ce fichier téléchargé, nous l'exécutons via la commande suivante.

.. code-block:: sh

   sh esa-snap_sentinel_unix_8_0.sh

Le processus d'installation se lance, il n'y a qu'à suivre les instructions. Notons que si Python est installé, nous pouvons lier les outils SNAP à notre installation Pyhton. Le logiciel se lance alors en cherchant simplement *snap* dans le lanceur d'application. Le lanceur se nomme *SNAP Desktop*.

Installation de SNAP dans Windows
++++++++++++++++++++++++++++++++++++

Nous allons voir ici comment installer les boîtes à outils SNAP sur un ordinateur tournant sous Windows. Au niveau de la page de téléchargement (:numref:`snap-site`), nous sélectionnons simplement la version *Windows 64-bit*. Un fichier exécutable est alors téléchargé nommé *esa-snap_all_windows-x64_8_0.exe*. Le *8* correspond à la version du logiciel, version disponible au moment de la rédaction de ces lignes. Ensuite, le logiciel s'installe de façon classique en double cliquant sur ce fichier et en suivant les instructions. Si Python est installé, nous pouvons lier SNAP à Python. Le logiciel est ensuite disponible dans le menu *Démarrer*.

..  _logiciels-WBT:

WhiteboxTools
---------------

`WhiteboxTools`_ est un logiciel qui se présente sous la forme d'une collection de différents modules dédiés aux traitements vecteurs et rasters. C'est une plateforme d'analyse de données géospatiales initié par le professeur `John Lindsay`_ de l'Université de Guelph (Canada). Le cœur de cette plateforme est distribuée sous licence Open Source et donc utilisable par tout un chacun. Une extension proposant quelques outils supplémentaires est disponible mais payante. Mais notons que le cœur libre de la plateforme propose déjà plusieurs centaines de modules.

Une force de cette plateforme est de proposer des outils pointus dans le domaine du traitement raster, des traitements hydrologiques (:numref:`wbt-wetlands`) (extraction de réseau hydrographique, de bassins-versants, comblement de MNT ...) et des traitements Lidar, comme dériver un modèle numérique d'élévation à partir d'un nuage de points Lidar.

La plateforme WhiteboxTools est utilisable via une ligne de commande propre au logiciel, via une API Python, via un package R, via un interface développé spécialement pour son utilisation ou via QGIS. Ces différents moyens d'utilisation et sa puissance font de cet outil un outil très précieux pour des traitements poussés.

.. figure:: figures/fig_WBT_wetlands.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: wbt-wetlands
    
    Analyse stochastique pour analyse de zones humides (figure tirée du site officiel).

.. tip::
	La  `documentation de WhiteBoxTools`_ est très bien faite et très détaillée (mais en anglais).

..  _installation-WBT:

Installation de WhiteboxTools
*********************************

La procédure d'installation est similaire sous Windows ou sous Linux/Ubuntu. Il est nécessaire de télécharger le *WhiteboxTools Open Core* sur `la page dédiée au téléchargement`_. Une fois le téléchargement effectué selon la version de son OS, un répertoire nommé WTB compressé se trouve maintenant sur notre disque dur. Une fois décompressé, nous pouvons l'explorer. Nous y trouvons trois fichiers qui nous seront utiles pour faire tourner WhiteBoxTools : *wb_runner.py*, *whitebox_tools* et *whitebox_tools.py*.

.. _WBT-interface-natif:

WBT via l'interface native
++++++++++++++++++++++++++++

Le fichier Python *wb_runner.py* permet de lancer un interface graphique minimal mais qui permet d'accéder aux différents modules de WBT. Le plus simple est d'ouvrir ce script dans un éditeur Python comme *PyCharm* ou *Spyder* puis de le lancer comme un script normal. L'interface permettant d'accéder aux différents modules apparaît alors (:numref:`wbt-interf-natif`).

.. figure:: figures/fen_wbt_interface_natif.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: wbt-interf-natif
    
    Lancement de l'interface natif de WBT.

Cet interface est tout à fait basique mais permet de lancer facilement tous les modules.

.. _WBT-commandes:

WBT via la commande
++++++++++++++++++++

WBT peut se piloter via la ligne de commande. Ces commandes appellent en fait les modules contenus dans le fichier *whitebox_tools*. Il est donc nécessaire de lancer les commandes depuis le répertoire qui contient ce fichier ou bien d'ajouter ce fichier au *path* de son système. Par exemple, pour lancer le module permettant de calculer un raster de pentes à partir d'un MNT, la commande suivante doit être lancée.

.. code-block:: sh

   ./whitebox_tools -r=Slope -v --dem=srtm_roya_L93.tif --output=slopeWBT.tif --units="degrees"

où :
 * *whitebox_tools* : appel aux commandes
 * *-r=Slope* : appel du module *slope* qui permet de calculer un raster de pentes
 * *-v* : pour avoir des sorties *verbeuses* dans la console
 * *--dem* : le MNT à prendre en entrée
 * *--output* : le raster de pentes en sortie
 * *--units* : le choix de l'unités des pentes

.. _WBT-QGIS:

WBT via QGIS
+++++++++++++

WBT peut s'interfacer dans QGIS, ce qui est très confortable lorsqu'on est habitué à ce logiciel. Pour utiliser WBT dans QGIS, il est tout de même nécessaire de télécharger WhiteBoxTools sur son ordinateur. En fait, QGIS pointera vers le fichier contenant les commandes *whitebox_tools*.

.. warning::
	Pour interfacer WBT, la version de QGIS doit être au moins la 3.18.

Dans un premier temps il est nécessaire d'installer le plugin dédié. Il ne se trouve pas dans les dépôts officiels, il faut l'ajouter manuellement, dans le menu :menuselection:`Extensions --> Installer/Gérer les extensions` puis aller dans l'onglet ``Paramètres`` (:numref:`qgis-wbt-depot`).

.. figure:: figures/fen_qgis_wbt_depot.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-wbt-depot
    
    Ajout du dépôt WBT dans les dépôts d'extensions de QGIS.

Nous cliquons sur ``Ajouter`` puis dans la fenêtre qui apparaît nous entrons un ``Nom`` pour le dépôt. Ce ``Nom`` peut être *Alex Bruy Plugins* (du nom de la personne qui maintient le plugin). Nous entrons ensuite l'``URL`` du dépôt  : ``https://plugins.bruy.me/plugins/plugins.xml`` puis nous cliquons sur :guilabel:`OK`. Nous retournons ensuite dans l'onglet ``Toutes`` de la fenêtre de gestion des extensions et nous cherchons l'extension *WhiteBoxTools* dans la barre de recherche (:numref:`qgis-wbt-install`).

.. figure:: figures/fen_qgis_wbt_extension.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-wbt-install
    
    Installation de l'extension WhiteBoxTools pour QGIS.

Puis nous cliquons sur :guilabel:`Installer`. Une fois l'extension installée, il est maintenant nécessaire de dire à QGIS où se situe le fichier *whitebox_tools* téléchargé précédemment. Pour cela nous allons dans la ``Boîte à outils de traitements`` et nous cliquons sur l'icône ``Options`` |icone_reglages|. Dans la fenêtre qui s'affiche, nous déplions le menu ``Fournisseur de services`` puis le menu ``WhiteBoxTools`` (:numref:`qgis-wbt-chemin`).

.. figure:: figures/fen_qgis_wbt_chemin.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-wbt-chemin
    
    Définition du chemin vers les outils WhiteBoxTools dans les fournisseurs de services.

Une fois cliqué sur :guilabel:`OK`, les outils WhiteBoxTools apparaissent dans la ``Boîte à outils de traitements`` (:numref:`qgis-wbt-outils`).

.. figure:: figures/fig_qgis_wbt_outils.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-wbt-outils
    
    Les outils WhiteBoxTools dans la Boîte à outils de traitements de QGIS.

.. _WBT-Python:

WBT via Python
+++++++++++++++

WBT peut s'utiliser via Python. Il est ainsi possible d'utiliser WBT facilement dans des scripts Python. Cet interfaçage se fait via le fichier *whitebox_tools.py*. Dans le script il est donc nécessaire de régler le *Python Path* pour qu'il intègre le répertoire *WBT* dans lequel se situe le fichier Python *whitebox_tools.py*. Cela ce fait avec la fonction ``sys.path.append('/home/poulpe/WBT/')``. Concrètement, un script faisant appel à WBT doit commencer comme l'expemple ci-dessous.

.. code-block:: python

   # Réglage du Python Path
   import sys
   sys.path.append('/home/poulpe/WBT/')
   
   # Import des modules WhiteBoxTools
   from whitebox_tools import WhiteboxTools
   wbt = WhiteboxTools()
   
   # Lancement d'un module pour découper un raster avec un polygone
   wbt.clip_raster_to_polygon('raster_in.tif', 'polygone.gpkg', 'raster_out.tif', False)

Dans ce script, après avoir réglé le *Python Path*, nous appelons le module ``clip_raster_to_polygon`` de WhiteBoxTools en précisant un raster d'entrée, un polygone de découpage et un raster de sortie.
    
..  _logiciels-Python:

Python
---------

`Python`_ est un langage de programmation très utilisée dans la communauté scientifique (mais pas que). Il est virtuellement possible de tout faire avec Python (du calcul scientifique, gestion de bases de données, de l'intelligence artificielle, des interfaces graphiques, des sorties graphiques ...). Ce langage n'a pas été développé pour la géomatique, mais des modules pour traiter ces questions ont été développés. Tous les logiciels présentés plus haut interagissent de près ou de loin avec Python.

En ce qui concerne la géomatique, il est possible de faire des traitements vecteurs aussi bien que rasters avec Python. Il est également possible de facilement se connecter avec des bases de données PostGIS. De très nombreux modules portant sur des questions de géomatique existent. Il est parfois difficile de s'y retrouver et de bien les appréhender. Nous en présenterons deux ici, le premier pour traiter les données rasters et le second pour les données vecteur : **rasterio** et **geopandas**. Ces deux librairies s'installent de façon classique.

..  _Python_rasterio:

Librairie rasterio
********************
La `librairie rasterio`_ permet de manipuler des données rasters avec Python. Elle permet, l'import et l'export depuis et vers de nombreux formats. Une fois un raster importé, avec *rasterio* il est possible de le manipuler en le retaillant, en le modifiant via des calculs rasters, en le reprojetant... Cette librairie est très utilisée et propose une documentation en ligne bien faite.

..  _Python_geopandas:

Librairie geopandas
*********************
La `librairie geopandas`_ est en quelque sorte une extension de la `librairie pandas`_. Cette librairie *pandas* est dédiée à la manipulation de tableaux, qu'on appelle souvent par le terme anglais de *dataframe*. *Pandas* rend possible la gestion, l'interrogation et la manipulation de ces tableaux, un peu dans l'esprit de ce qui se fait avec R. *geopandas* fait exactement la même chose mais ajoute une colonne de géométrie au tableau. Grâce à cette colonne, il est possible de faire des traitements SIG géospatiaux sur ces objets, comme des sélections et jointures spatiales, des géotraitements (zones tampons, intersections...), des reprojections... Cette librairie est très utilisée et beaucoup de documentation se trouve facilement en ligne.


..  _logiciels-R:

R
-------

`R`_ est à l'origine un langage dédié à l'analyse statistique et à la fouille de données. Mais il a tellement évolué et s'étoffe tous les jours de fonctionnalités nouvelles que c'est maintenant considéré comme un langage de programmation à part entière. À la base R n'était pas fait pour traiter les données à caractère spatial comme les couches rasters et vecteurs. Mais aujourd'hui, des modules dédiés existent et faire de la géomatique ou de la cartographie avec R est très courant et facile. R est donc une solution optimale pour automatiser des chaînes de traitements géomatiques et produire des cartes de qualité professionnelle. Ici, nous ne nous intéresserons qu'à la partie géomatique, pour la partie cartographie, reportez aux ouvrages et modules dédiés comme `mapsf`_.

Pour traiter les données rasters avec R, il existe trois librairies (*packages*) phares : **raster**, **terra** et **stars**. Pour traiter les données vecteurs, la librairie montante est **sf**. Ils s'installent tous quatre très simplement de façon classique.

..  _R-Raster:

Librairie raster
*****************
La `librairie raster`_ est la librairire historique qui permet de traiter les données rasters. Elle est toujours largement employée mais souffre, sous certains aspects d'obsolescence. Par exemple, certaines de ces fonctionnalités sont mal optimisées d'un point de vue ressources et mettent beaucoup de temps à s'exécuter, comme la transformation d'un raster en vecteur par exemple. Un gros avantage tout de même de *raster* est qu'il est largement employé, avec une documentation bien fournie, et une communauté d'utilisateur prête à vous aider très importante.

..  _R-Terra:

Librairie terra
****************
La `librairie terra`_ se veut la nouvelle génération de la libraire *raster*. Nous y retrouvons la plupart de ses fonctionnalités mais en plus optimisées. Les traitements sont plus rapides. De plus, d'autres possibilités comme les classifications supervisées et non supervisées d'images sont directement disponibles dans *Terra*. Le passage d'un objet *Raster* à un objet *terra* se fait relativement facilement. Par contre, un point faible de *terra* est sa difficulté à dialoguer avec les objets vecteurs de type *sf*, qui deviennent pourtant la référence en ce qui concerne les vecteurs. Un point très positif de *terra* est sa documentation en ligne très fournie et très claire.

..  _R-Stars:

Librairie stars
****************
La `librairie stars`_ se veut également un descendant de *raster*. À la base *stars* n'est pas spécifique aux objets rasters car il peut également gérer les données vecteurs. La philosophie de *stars* est de créer et gérer des *cubes de données* (*data cubes*). Un cube de donnée dans *stars* est souvent un empilement de données décrivant un même espace mais variant dans le temps. Par exemple, si nous avons un NDVI pour chaque date pour une année donnée au-dessus d'une région donnée, l'empilement des rasters de NDVI sera un cube de données. Mais ce cube peut également être constitué de vecteurs. Quoiqu'il en soit, *stars* peut être utilisé même pour stocker un seul raster ou un cube de données composé de bandes spectrales.

Un désavantage de *stars* est sa philosophie un peu déroutante pour les géomaticiens. Sa documentation n'est pas non plus aussi bien faite que pour les deux précédentes librairies. Par contre, un gros avantage est son optimisation, ses modules s'exécutent rapidement. Un autre avantage non négligeable est que *stars* dialogue bien avec la libraire *Sf* de gestion de données vecteur. Il est par exemple facile, et rapide, de passer d'un raster à un polygone avec *stars*.

..  _R-sf:

Librairie sf
**************
La `librairie sf`_ (pour *simple features*) est devenue une des librairies les plus utilisées pour la gestion de données vecteur. Bien qu'historiquement il existe de nombreuses autres librairies pour lire et traiter les données vecteurs, *sf* tend à maintenant dominer. Avec cette librairie, il est possible de lire tous les formats vecteurs, de faire des traitements sur leurs tables attributaire et des géotraitements sur leurs géométries. Comme *sf* commence à être largement utilisée, beaucoup de documentation se trouve en ligne et la communauté est active. Enfin, *sf* dialogue bien avec la librairie *Stars* et permet de faire des cartes esthétiques avec `mapsf`_.


..  _logiciels-PostGIS:

PostgreSQL/PostGIS
-----------------------

`PostgreSQL`_ est un `système de gestion de bases de données relationnelles`_ (SGBDR). Il s'agît d'un logiciel qui permet de stocker, gérer et manipuler des bases de données. Une base de données peut être vue comme un ensemble de *tables* c'est-à-dire un ensemble de tableaux organisés en lignes et colonnes. Ces tables sont en relation les unes avec les autres. Ainsi, en *interrogeant* une table, il est possible d'interroger en même temps plusieurs tables par le jeu des relations qu'elles entretiennent.

.. _système de gestion de bases de données relationnelles: https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_base_de_donn%C3%A9es

L'intérêt d'utiliser une base de données et de travailler sur des données bien structurées et donc facilement interrogeables. Ces requêtes se font à l'aide du langage SQL. SQL est un langage très puissant, qui permet de faire des requêtes complexes en un minimum de manipulations.

Il existe différents SGBDR mais PostgreSQL a l'avantage d'être puissant, libre, gratuit et multi-OS. Un autre avantage qui nous intéresse particulièrement ici est qu'il peut être étendu avec l'extension *PostGIS* (:numref:`outils-BDD`). `PostGIS`_ permet de gérer des tables disposant d'informations géographiques. Ces tables sont vues comme des tables classiques disposant simplement d'une colonne supplémentaire renseignant sur la géométrie. Cette géométrie peut être ponctuelle, linéaire, surfacique ...

.. figure:: figures/fig_outils_BDD.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: outils-BDD
    
    Les trois outils intéressants à utiliser ensemble.

Avec PostgreSQL/PostGIS seuls, il est possible de gérer et interroger les bases de données en ligne de commandes, ce qui peut être un peu ardu. Heureusement, il existe des outils ergonomiques qui permettent d'interfacer les bases de donées stockées afin de les gérer et de les interroger plus facilement. Parmi ces outils, il y a QGIS, qui permet de charger les tables géographiques stockées et de les interroger. Pour une gestion plus fine et pour profiter pleinement des potentialités du SQL, il est également possible d'utiliser des interfaces non orientés SIG (:numref:`outils-BDD`) comme le logiciel (libre) `DBeaver`_. C'est cette solution que j'utilise personnellement.

.. note::

	La gestion de données géographiques à l'aide de *vraies* bases de données structurées avec les outils dédiés est souvent réservée aux *experts* de la question. Ça ne devrait pas être le cas. L'effort de prise en mains n'est pas du tout insurmontable et la puissance de ces outils les rendent très intéressants.

..  _installation_PostGIS:

Installation de PostGreSQL/PostGIS
*************************************

Dans cette section nous allons voir comment installer le SGBDR (*Système de Gestion de Bases de Données Relationnelles*) PostGreSQL et son extension spatiale PostGIS.

.. contents:: Table des matières
    :local:

Notions de bases pour bien comprendre
++++++++++++++++++++++++++++++++++++++++
Après avoir installé PostGreSQL sur notre ordinateur, nous disposerons d'un *serveur de bases de données* en local. Il ne faut pas être effrayé par le terme de *serveur*. À moins de déployer vos bases sur un réseau, la notion de serveur sera totalement transparente. Nos différentes bases seront stockées sur ce serveur.

Au cours de l'installation, une base sera créée par défaut. Cette base sera nommée *postgres*. Il s'agira de la *base mère* qui contiendra des données relatives au bon fonctionnement du serveur comme les identifiants des différents utilisateurs. Nous toucherons très peu à cette base, nous la laisserons tranquille dans son coin.

Comme nous sommes sur une logique de serveur, il sera nécessaire de créer des *utilisateurs* (aussi appelés *rôles* dans le jargon des bases de données) qui auront le droit, ou non, de se connecter et de modifier les différentes bases. À l'installation, un utilisateur spécial est créé. Cet utilisateur possède les droits d'administration de la partie serveur. Cet utilisateur se nomme *postgres* et nous lui associerons un mot de passe. C'est en prenant le rôle de cet utilisateur que nous pourrons créer d'autres utilisateurs. 

Il sera nécessaire de créer au moins un autre utilisateur. C'est l'utilisateur que nous utiliserons pour administrer, gérer et interroger les bases de données autres que la base mère. Il est en effet non recommandé de travailler avec le *super* utilisateur *postgres*. À priori, dans notre cas, nous ne créerons qu'un seul utilisateur qui sera nous, avec lequel nous travaillerons.

De même que nous ne travaillerons pas avec l'utilisateur *postgres*, nous ne travaillerons pas dans la base mère du même nom. Nous allons donc créer une, ou plusieurs autres bases de données. Ces bases seront associées à l'utilisateur *nous*. C'est au sein de ces bases que nous stockerons et interrogerons les données sur lesquelles nous travaillerons.

Au sein d'une base de données, il sera possible de créer ce que nous appelons un (ou plusieurs) *schéma*. Un schéma peut être vu comme un sous répertoire aidant à ranger les différentes données i.e. les différentes tables. Par exemple, nous pouvons créer une base de données *usage_du_sol* dans laquelle nous stockons les données relatives à l'usage du sol. Au sein de cette base, nous pouvons créer un schéma *Corine_Land_Cover* dans lequel nous stockerons les différentes couches (i.e. les différentes tables) de Corine Land Cover (2000, 2006, 2012 ...). Ensuite nous pouvons créer un schéma *oso_theia* dans lequel nous stockons les différentes tables relatives à l'usage du sol Theia (2015, 2016, 2017 ...). Cette organisation est tout à fait libre et laissée à l'appréciation de l'utilisateur.

Concrètement, nous ne toucherons jamais au logiciel PostGreSQL en lui-même. Toute la phase d'administration comme la création d'utilisateurs et de bases passera par le logiciel DBeaver. C'est également via ce logiciel que nous interrogerons nos bases à l'aide de requêtes SQL plus ou moins poussées. Enfin, nous visualiserons les tables spatiales (équivalentes à des couches) à l'aide de QGIS. Nous pourrons également faire quelques requêtes simples directement dans QGIS.

Pour récapituler, les grandes étapes d'installation et de configurations sont les suivantes :

* installation de PostGreSQL et de son extension spatiale PostGIS

* installation de DBeaver

* définition d'un mot de passe pour le super utilisateur administrateur nommé *postgres*

* à l'aide de DBeaver, création d'un utilisateur (aussi appelé *rôle*) normal

* toujours à l'aide de DBeaver, création d'une base de données appartenant à l'utilisateur normal

* ajouter à cette base de données l'extension PostGIS afin de pouvoir y stocker des données géographiques

* dans QGIS, se connecter à cette base de données géographiques

* à l'aide de QGIS, y importer des données

.. note::

	Tout cela est assez procédurier mais manipuler ses données à l'aide de ces outils et des requêtes SQL peut s'avérer extrêmement puissant. L'effort de prise en mains en vaut la peine.
	
Installation sous Debian/Ubuntu
+++++++++++++++++++++++++++++++++++++
Nous allons voir ici comment installer les différents outils sous un système Debian ou Ubuntu (une procédure pour Windows viendra plus tard).

Les dépôts de base ne contiennent pas les dernières versions des outils à installer. La première étape consiste donc à ajouter des dépôts spécifiques dans notre liste de dépôts. Pour cela, nous entrons la commande suivante dans une une console.

.. code-block:: sh

   sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

Nous ajoutons ensuite la clé d'identification de ce dépôt.

.. code-block:: sh

   wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

Nous rechargeons la liste des dépôts.

.. code-block:: sh

   sudo apt-get update

Nous pouvons ensuite installer les outils désirés.

.. code-block:: sh

   sudo apt-get -y install postgresql-13 postgis postgis-gui postgresql-13-postgis-3 postgresql-13-postgis-3-scripts

Ici, nous installons :

* postgresql-13 : la version 13 de PostGreSQL (la dernière disponible au moment de la rédaction de cette fiche)

* postgis postgis-gui : PostGIS et un interface graphique à PostGIS utile dans certains cas

* postgresql-13-postgis-3 postgresql-13-postgis-3-scripts : les outils de liaison entre PostGreSQL et PostGIS

À l'issue de cette phase, nous avons bien installé PostGreSQL et son extension PostGIS. Il est maintenant nécessaire de passer à la configuration du système.

Maintenant, nous installons le logiciel DBeaver qui nous servira pour administrer nos bases et les interroger. Là aussi, il est nécessaire d'ajouter un dépôt spécifique via la commande suivante.

.. code-block:: sh

   sudo add-apt-repository ppa:serge-rider/dbeaver-ce

Il n'y a plus qu'à recharger la liste des dépôts et à installer le logiciel comme suit.

.. code-block:: sh

   sudo apt-get update
   sudo apt-get install dbeaver-ce

Configuration de l'admin de PostGreSQL 
++++++++++++++++++++++++++++++++++++++++++
En premier lieu, il est nécessaire de définir un mot de passe pour l'utilisateur administrateur nommé *postgres*. Cette manipulation se fait en *root*. Nous ouvrons un terminal et nous entrons la commande suivante qui nous permet de nous identifer en *root*.

.. code-block:: sh

   sudo -i

Cette commande nous fait passer en *root*. Nous voyons que le prompt change en conséquence. Nous changeons ensuite d'utilisateur pour passer en utilisateur *postgres*. Cela se fait avec la commande suivante.

.. code-block:: sh

   su - postgres

Une fois identifié comme étant l'utilisateur *postgres*, nous spécifions un mot de passe grâce à la commande suivante.

.. code-block:: sh

   psql -c "ALTER USER postgres WITH password 'postgres'"

Avec cette commande nous spécifions le mot de passe *postgres* pour l'utilisateur *postgres*. Nous aurions pu mettre n'importe quel autre mot de passe.

Création d'un utilisateur lambda
++++++++++++++++++++++++++++++++++++
Comme dit précédemment, nous ne pouvons pas travailler sous l'identité de l'administrateur en chef *postgres*. Il nous faut créer un utilisateur *normal* que nous nommerons *my_user*. C'est avec cette identité que nous gérerons et interrogerons nos bases. Cette étape se fait à l'aide de DBeaver. Nous lançons donc ce logiciel.

À l'ouverture de DBeaver, dans le panneau de gauche, nous constatons que nous disposons de notre serveur identifié comme *localhost* dans lequel se trouve notre base de données mère nommée *postgres* (:numref:`dbeaver-init`).

.. figure:: figures/fen_dbeaver_init.png
    :width: 400px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: dbeaver-init
    
    Le serveur *localhost* et la base mère *postgres*.

Nous allons créer notre utilisateur *my_user* au sein de cette base mère. Pour cela nous faisons un clic droit sur la base :menuselection:`postgres --> Créer --> Rôle`. Dans la fenêtre qui s'affiche, nous spécifions le nom du nouvel utilisateur ``my_user`` et nous lui associons un mot de passe (:numref:`dbeaver-new-user`).

.. figure:: figures/fen_dbeaver_new_user.png
    :width: 200px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: dbeaver-new-user
    
    Création d'un nouvel utilisateur.

Une fois ce nouvel utilisateur créé, il est nécessaire de le garder *en dur* en cliquant sur :guilabel:`Sauvegarder`, en bas à droite de l'écran. Une nouvelle fenêtre s'affiche alors. Nous cliquons sur :guilabel:`Persister` (:numref:`dbeaver-save-user`).

.. figure:: figures/fen_dbeaver_sauvegarder_user.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: dbeaver-save-user
    
    Sauvegarde du nouvel utilisateur.

Ce nouvel utilisateur apparaît bien dans la liste des utilisateurs reconnus par la base *postgres* (:numref:`dbeaver-list-user`).

.. figure:: figures/fen_dbeaver_list_users.png
    :width: 250px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: dbeaver-list-user
    
    Liste des utilisateurs.

Création d'une nouvelle base de données (vide)
+++++++++++++++++++++++++++++++++++++++++++++++++++
Maintenant que nous avons créé un utilisateur de travail, nous allons pouvoir lui associer une base de données dans laquelle il pourra travailler. Pour cela, nous restons dans DBeaver et nous faisons un clic droit sur la ligne ``localhost``. Nous choisissons :menuselection:`Créer --> Database`. Dans la fenêtre qui apparaît, nous spécifions le nom de la base, par exemple *geodata* ainsi qu'un utilisateur associé *my_user* (:numref:`dbeaver-create-db`).

.. figure:: figures/fen_dbeaver_create_db.png
    :width: 280px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: dbeaver-create-db
    
    Création d'une base de données geodata associée à l'utilisateur my_user.
    
La nouvelle base apparaît bien dans le panneau des bases sous la base *postgres*.

.. warning::

	Si le nouvel utilisateur n'apparaît pas dans la liste, il peut être nécessaire de redémarrer DBeaver.

À ce stade, nous disposons d'une nouvelle base de données vide, qui appartient à *my_user*. Mais cette base n'est pas encore géographique. Il va falloir maintenant lui associer l'extension *PostGIS*.

Association de l'extension PostGIS à une base
+++++++++++++++++++++++++++++++++++++++++++++++++
Cette association se fait très simplement. Dans un premier temps il est nécessaire de spécifier à DBeaver que nous souhaitons maintenant travailler avec la nouvelle base. Pour cela, nous faisons un clic droit sur cette nouvelle base *geodata* et nous sélectionnons ``Définir l'objet défaut``. Nous remarquons au passage que la traduction en français de DBeaver n'est pas sensationnelle... La base *geodata* apparaît maintenant en gras ce qui signifie que c'est notre base de travail. Ensuite, nous déroulons la base afin de faire apparaître le sous menu ``Extensions`` (:numref:`dbeaver-menus-base`).

.. figure:: figures/fen_dbeaver_menus_base.png
    :width: 200px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: dbeaver-menus-base
    
    Les sous menus associés à la base.

Nous faisons ensuite un clic droit sur le menu *Extensions*. Une fenêtre de gestion des extensions apparaît (:numref:`dbeaver-extensions`).

.. figure:: figures/fen_dbeaver_extensions.png
    :width: 500px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: dbeaver-extensions
    
    Liste des extensions disponibles.

Nous y cherchons l'extension *postgis* et nous l'installons en cliquant sur :guilabel:`OK`. Nous refaisons la même manipulation pour installer l'extension *postgis_raster* qui nous permettra de manipuler des données rasters avec PostGIS. Une fois ces deux extensions installées, elles apparaissent bien dans le sous menu ``Extensions`` de notre base *geodata*.

Nous disposons maintenant d'une base géographique, nommée *geodata* et qui appartient à l'utilisateur normal *my_user*. Nous pouvons maintenant y ajouter des données.

Exploration des données via DBeaver
+++++++++++++++++++++++++++++++++++++++
Lorsqu'une couche a été ajoutée à notre base, il est possible de l'explorer via DBeaver. En retournant dans ce logiciel, nous allons dans la base qui a été mise à jour et nous constatons l'ajout d'une nouvelle table, i.e. couche. Cette couche est éventuellement dans un nouveau schéma, selon la façon dont elle a été ajoutée (:numref:`dbeaver-base-sync`).

.. figure:: figures/fig_dbeaver_base_sync.png
    :width: 250px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: dbeaver-base-sync
    
    La table est bien synchronisée dans DBeaver.

Nous pouvons faire un clic droit sur notre table et sélectionner ``View Data``. La table s'affiche alors dans le panneau central (:numref:`dbeaver-data`).

.. figure:: figures/fig_dbeaver_table_data.png
    :width: 600px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: dbeaver-data
    
    Les données associées à la table.

Outre les attributs initiaux de la couche (*scalerank*, *min_zoom*, ...) nous trouvons un nouvel attribut *geom*. Il s'agît simplement d'une colonne de géométrie stockant la géométrie de chaque entité. Ici, nous constatons qu'il s'agît de *multipolygon*. En logique base de données, la géométrie est un attribut comme un autre. Simplement, c'est un attribut qui nous permettra de faire des géotraitements comme des intersections, des zones tampons, des jointures spatiales ...


.. |icone_reglages| image:: figures/icone_qgis_reglages.png
              :width: 20 px

.. _site officiel de OTB: https://www.orfeo-toolbox.org/
.. _7zip: https://www.7-zip.org/
.. _décrite sur ce site: https://www.computerhope.com/issues/ch000549.htm
.. _le site dédié: https://www.qgis.org/fr/site/ https://step.esa.int/main/download/snap-download/
.. _QGIS: https://www.qgis.org/fr/site/
.. _SCP: https://fromgistors.blogspot.com/p/semi-automatic-classification-plugin.html?spref=sacp
.. _SNAP toolbox: http://step.esa.int/main/download/snap-download/
.. _GRASS GIS: https://grass.osgeo.org/
.. _SAGA GIS: http://www.saga-gis.org/
.. _Orfeo ToolBox: https://www.orfeo-toolbox.org/
.. _Agence Spatiale Européenne: https://www.esa.int/
.. _SNAP toolboxes: https://step.esa.int/main/toolboxes/
.. _distribuées gratuitement: https://step.esa.int/main/download/snap-download/
.. _Sentinel-1: http://www.esa.int/Applications/Observing_the_Earth/Copernicus/Sentinel-1
.. _2: https://www.esa.int/Applications/Observing_the_Earth/Copernicus/Sentinel-2
.. _3: https://www.esa.int/Applications/Observing_the_Earth/Copernicus/Sentinel-3
.. _SMOS: http://www.esa.int/Applications/Observing_the_Earth/SMOS
.. _PROBA-V: https://www.esa.int/Applications/Observing_the_Earth/Proba-V
.. _WhiteboxTools: https://jblindsay.github.io/wbt_book/intro.html
.. _John Lindsay: https://jblindsay.github.io/ghrg/index.html
.. _Python: https://pythonprogramming.net/
.. _R: https://www.r-project.org/
.. _PostgreSQL: https://www.postgresql.org/
.. _PostGIS: https://postgis.net/
.. _DBeaver: https://dbeaver.io/
.. _mapsf: https://riatelab.github.io/mapsf/
.. _librairie raster: https://rspatial.org/raster/RasterPackage.pdf
.. _librairie terra: https://rspatial.org/terra/pkg/1-introduction.html
.. _librairie stars: https://r-spatial.github.io/stars/
.. _librairie sf: https://r-spatial.github.io/sf/
.. _librairie rasterio: https://rasterio.readthedocs.io/en/latest/index.html
.. _librairie geopandas: https://geopandas.org/en/stable/docs.html
.. _librairie pandas: https://pandas.pydata.org/docs/user_guide/index.html
.. _utilisez Windows: https://www.computerhope.com/issues/ch000549.htm
.. _Ubuntu: https://techpiezo.com/linux/set-path-environment-variable-in-ubuntu/
.. _documentation en ligne est bien faite pour s'y retrouver: https://gdal.org/programs/index.html
.. _très grande documentation spécifiquement dédiée à QGIS: https://ouvrir.passages.cnrs.fr/tutoqgis/
.. _la page dédiée au téléchargement: https://www.whiteboxgeo.com/geospatial-software/
.. _documentation de WhiteBoxTools: https://www.whiteboxgeo.com/manual/wbt_book/preface.html
