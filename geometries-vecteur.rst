..  _geometries-vecteur:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Travailler sur les géométries vecteur
========================================

Dans ce chapitre vous trouverez quelques outils dédiés à la manipulation des géométries des couches vectorielles, comme les sélections spatiales, les géotraitements ou d'autres manipulations géométriques utiles.

.. contents:: Table des matières
    :local:


..  _erreurs-geom:

Erreurs de géométries
--------------------------

Il arrive que les fichiers de polygones vecteurs contenant beaucoup d'entités et présentant des tailles très variées contiennent des erreurs de géométries. C'est souvent le cas des couches de polygones obtenues par polygonisation, ou vectorisation, d'une couche raster (cf :ref:`raster-to-vecteur`). Ces erreurs sont généralement décelées lors du plantage d'un géotraitement ultérieur faisant appel à la couche en question comme une intersection ou une différence. Ces erreurs ne sont pas vraiment des erreurs dans la géométrie, il ne s'agît pas de *trous* ou de *décalage* mais plutôt des erreurs dans la définition *informatique* de certains nœuds des polygones. Lors d'un plantage avec le message d'erreur suivant ``L'entité (1) de “ma couche vecteur” a une géométrie non valide.`` cela signifie que la couche mentionnée dans le message a un souci de géométrie. 

..  _verifier-geom:

Vérifier la géométrie
************************

Il est possible d'extraire les nœuds qui posent problème via le menu ``Vérifier la validité`` qui se trouve dans la :menuselection:`Boîte à outils de traitements --> Géométrie vectorielle --> Vérifier la validité`. Une fois ce menu ouvert la fenêtre suivante apparaît (:numref:`predicats`).

.. figure:: figures/fen_qgis_verifier_geometrie.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: verifier-geometrie
    
    Vérification de la validité des géométries dans QGIS.

Dans le champ ``Couche source`` nous indiquons la couche à vérifier. Dans le panneau ``Méthode`` nous cochons simplement la méthode ``GEOS``. C'est en effet la méthode de vérification la plus pointilleuse et donc celle qui décèlera vraiment les erreurs. Enfin nous pouvons laisser les autres champs vides, les couches de validité et d'invalidité seront affichées en temporaire, ce qui n'est pas génant. Puis nous cliquons sur :guilabel:`Exécuter`. Les polygones contenant des erreurs ainsi que les noeuds qui posent problème apparaissent alors dans QGIS (:numref:`erreur-geometrie`).

.. figure:: figures/fig_qgis_erreurs_geometries.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: erreur-geometrie
    
    Les polygones présentant des erreurs de géométrie (en gris foncé) et le neud problématique (en rouge).

Sur la figure :numref:`erreur-geometrie`, les polygones présentant des erreurs sont en gris foncé et le nœud problématique est en rouge. Il y a aussi un problème car les deux carrés gris foncés sont en fait un seul polygone mais avec une forme de 8 avec un nœud qui se trouve à une sorte d'intersection. C'est cette configuration précise qui pose problème. Heureusement, il est possible de réparer facilement ce type de soucis.

.. tip::
	Lorsqu'un traitement plante à cause d'une géométrie *non valide* il n'est même pas nécessaire de vérifier où se trouve le problème. Nous pouvons *réparer* directement la couche comme présenté dans la section suivante.

..  _réparer-geom:

Réparer la géométrie
************************

Lorsqu'une couche présente une géométrie non valide, qu'on l'est vérifiée ou non, il est possible de la réparer dans QGIS. Nous utilisons le menu ``Réparer les géométries`` qui se trouve dans la :menuselection:`Boîte à outils de traitements --> Géométrie vectorielle --> Réparer la géométrie`. Le menu suivant s'ouvre (:numref:`reparer-geometrie`).

.. figure:: figures/fen_qgis_reparer_geometrie.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: reparer-geometrie
    
    Réparer les géométries dans QGIS.

Dans le champ ``Couche source`` nous indiquons la couche à réparer et dans le champ ``Réparer les géométries`` nous spécifions un chemin et un nom pour la couche vecteur après réparation. Puis nous cliquons sur :guilabel:`Exécuter`. La couche réparée apparaît alors dans QGIS. Cette couche est, visuellement, en tout point identique à la couche d'origine, c'est simplement la façon dont sont *codés* les nœuds qui change.


..  _predicats-geom:

Prédicats géométriques
--------------------------

La particularité fondamentale des SIG est de proposer une base de données qui contient une composante spatiale. Les entités manipulées, notamment les entités de type vecteur, possèdent une implantation dans l'espace. Il est donc possible de relier différentes couches de données en fonction de leurs localisations. Une couche *A* peut contenir des éléments d'une couche *B* ou intersecter des éléments d'une couche *C* et ainsi de suite. Toute cette série de relations spatiales peut se formaliser sous forme de *prédicats géométriques*. La figure suivante résume les plus courants (:numref:`predicats`).

.. figure:: figures/fig_predicats_geometriques.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: predicats
    
    Les principaux prédicats géométriques.

Ces prédicats géométriques sont à la base de deux grands types de manipulation : la :ref:`selection-spatiale` et la :ref:`jointure-spatiale`. Notons bien que les trois implantations (surfacique, linéaire et ponctuelle) se prêtent à ce jeu de prédicats.


..  _extract-vertex:

Extraction des sommets
--------------------------

Les polygones et les polylignes sont en fait constitués d'une série de points, appelés *vertex*, ou *sommets*, reliés entre eux par des morceaux de lignes. Dans le cas des polygones, ces morceaux de lignes sont fermés et une surface est comprise dans ce périmètre. Dans certains cas, il est intéressant d'extraire les vertex constitutifs des lignes ou des polygones. Cette manipulation se fait plus ou moins facilement selon les outils.

Dans les exemples suivants, nous allons voir comment extraire les vertex constitutifs du polygone présenté sur la figure suivante (:numref:`polygone-vertex`).

.. figure:: figures/fig_polygone_vertex.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: polygone-vertex
    
    Vertex constitutifs d'un polygone.

..  _extract-vertex-qgis:

Extraction avec QGIS
*********************
Version de QGIS : 3.22.0

L'outil à utiliser se trouve dans le menu :menuselection:`Boîte à outils de traitements --> Géométrie vectorielle --> Extraire les sommets`. Le menu suivant s'ouvre.

.. figure:: figures/fen_extraire_vertex_qgis.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: xtr-vertex-qgis
    
    Extraction des vertex (aussi appelés *sommets*) avec QGIS.

Dans le menu ``Couche source`` nous indiquons le nom de la couche de polygone dont il faut extraire les sommets. Puis à la ligne ``Sommets`` nous indiquons un chemin et un nom pour la couche qui contiendra les sommets. Notons que si nous le souhaitons nous pouvons cocher la case ``Entité(s) sélectionnée(s) uniquement`` si seuls les sommets du ou des polygones sélectionnés sont nécessaires. À la fin du traitement nous obtenons une couche vectorielle de type *points* dans laquelle chaque point correspond à un sommet du polygone.


..  _selection-spatiale:

Sélection spatiale
--------------------

La sélection spatiale, aussi appelée *requête spatiale*, permet de sélectionner des entités d'une couche selon leur position géographique par rapport aux entités d'une autre couche. Ce type de sélection repose sur les :ref:`predicats-geom`. Il est par exemple possible de sélectionner tous les éléments d'une couche *A* qui contiennent des éléments d'une couche *B*, ou tous les éléments d'une couche *A* qui croisent les éléments d'une couche *C*.

..  _selection-spatiale-qgis:

Sélection spatiale dans QGIS
********************************
Version de QGIS : 3.22.3

Dans QGIS, il est facile de faire des sélections spatiales. Il faut juste charger les couches vecteurs à croiser et s'assurer qu'elles sont bien dans le même système de coordonnées de référence pour éviter toute surprise. Pour l'exemple, nous allons sélectionner toutes les communes d'Île-de-France contenant un lieu de culte. Nous nous basons sur une couche des polygones des communes et une couche ponctuelle des lieux de culte (:numref:`idf-culte`).

.. figure:: figures/fig_idf_lieux_de_culte.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: idf-culte
    
    Les lieux de culte (sûrement non exhaustifs) en Île-de-France.

La requête que nous souhaitons faire est la suivante : *Je souhaite sélectionner toutes les communes qui contiennent un lieu de culte*. Pour cela nous allons dans le menu :menuselection:`Vecteur --> Outils de recherche --> Sélection par localisation...` La fenêtre suivante apparaît (:numref:`select-localisation-qgis`).

.. figure:: figures/fen_qgis_selection_par_localisation.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: select-localisation-qgis
    
    Sélection par localisation dans QGIS.

Dans le menu déroulant ``Sélectionnez les entités depuis``, nous sélectionnons la couche depuis laquelle nous souhaitons sélectionner des entités, dans notre cas la couche des communes. Dans le panneau ``Où les entités (prédicat géométrique)``, nous cochons le prédicat souhaité, ici ``contient``. Dans le menu déroulant ``En comparant les entités de``, nous sélectionnons la couche de comparaison, ici la couche des lieux de culte. Enfin dans le menu déroulant ``Modifier la sélection actuelle en``, nous laissons l'option par défaut. Puis nous cliquons sur :guilabel:`Exécuter`. Les communes qui contiennent un lieu de culte sont bien sélectionnées.

Notons que si nous avions choisi de sélectionner les lieux de culte qui sont *à l'intérieur* d'une commune, nous aurions sélectionné tous les lieux de culte... Le sens de la requête est donc important.

.. note::
	Il est possible de jouer sur le menu ``Modifier la sélection actuelle en``. Par exemple, nous pouvons ajouter la sélection à une sélection précédente via *Ajouter à la sélection actuelle*, ce qui est pratique pour combiner des sélections. Nous pouvons également enlever la sélection à une sélection précédente via *Enlever de la sélection actuelle*, pratique pour une requête de type *sauf*.


..  _jointure-spatiale:

Jointure spatiale
--------------------
La jointure spatiale est une manipulation très utile en SIG. La jointure spatiale permet de joindre les attributs de deux couches selon une caractéristique géographique qu'elles partagent. Par exemple, si nous disposons d'une couche ponctuelle des plus hauts sommets d'une région montagneuse et d'une couche de polygones des communes, il est possible de rapatrier les noms des communes à celle des sommets. La manipulation sera de lier les attributs des communes qui contiennent les sommets. D'une manière générale, la jointure spatiale repose sur les :ref:`predicats-geom`.

..  _jointure-spatiale-qgis:

Jointure spatiale dans QGIS
********************************
Version de QGIS : 3.22.3

La jointure spatiale se fait facilement dans QGIS. Il est d'abord nécessaire d'ouvrir les deux couches à joindre et de s'assurer qu'elles sont dans le même système de coordonnées de référence. Dans l'exemple, nous disposons d'une couche ponctuelle des lieux de culte d'Île-de-France et d'une couche de polygones des communes de cette même région (:numref:`idf-culte`). Nous souhaitons rapatrier le nom de la commune dans lequel se situe chaque lieu de culte. La jointure va donc être de la forme : *Je souhaite rapatrier le nom de la commune à l'intérieur de laquelle se trouve chaque lieu de culte*. Le prédicat géométrique est donc *à l'intérieur* (*within*). Le formulaire de jointure spatiale se trouve dans le menu :menuselection:`Vecteur --> Outils de gestion de données --> Joindre les attributs par localisation...` Le menu suivant apparaît (:numref:`join-spatiale-qgis`).

.. figure:: figures/fen_qgis_jointure_spatiale.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: join-spatiale-qgis
    
    Jointure spatiale dans QGIS.

Dans le champ ``Couche de base`` nous indiquons la couche sur laquelle nous souhaitons rapatrier les informations, ici la couche ponctuelle des lieux de culte. Dans le champ ``Joindre la couche`` nous indiquons la couche que nous souhaitons joindre à savoir la couche surfacique des communes. Dans le panneau ``Prédicat géométrique`` nous cochons le prédicat à utiliser, ici ``within`` (*à l'intérieur*). Dans le champ ``Champs à ajouter`` nous pouvons choisir le ou les champs à joindre, par défaut tous les champs sont joints. Dans le champ ``Type de jointure``, nous choisissons la façon dont doit se faire la jointure, ici ``Créer une entité distincte pour chaque entité correspondante (un à plusieurs)``. Ça signifie qu'à chaque lieu de culte, les attributs de la commune d'appartenance seront joints. Dans le champ ``Préfixe de champ joint`` nous pouvons indiquer un préfixe qui sera au début des noms des champs joints. Enfin, dans le champ ``Couche issue de la jointure spatiale`` nous pouvons indiquer un chemin d'export pour la nouvelle couche qui sera créée, sinon il s'agira d'une couche temporaire. Il ne rste plus qu'à cliquer sur :guilabel:`Exécuter`. Une nouvelle couche des lieux de culte s'ouvre, géographiquement tout à fait identique à l'originale. Par contre, nous constatons bien qu'un nouveau champ nommé *commune_NOM_COM* est apparu, contenant le nom de la commune dans laquelle se trouve le lieu de culte.

Il est important de noter que l'ordre de jointure est important. Ainsi, si nous souhaitons joindre les attributs de la couche des lieux de culte à la couche des communes (l'inverse de l'exemple précédent), c'est possible mais le résultat sera différent. En effet, si un lieu de culte n'appartient qu'à une seule commune, une commune peut très bien avoir plusieurs lieux de cultes. Que va-t-il ainsi se passer dans notre cas ? Si nous choisissons l'option ``Créer une entité distincte pour chaque entité correspondante (un à plusieurs)``, les communes disposant de plusieurs lieux de culte seront dupliquées. Ainsi, une commune disposant de 3 lieux de culte sera copié trois fois. Cette commune aura donc trois lignes dans la table attributaire, soit une par lieu de culte, mais aura aussi 3 polygones parfaitement superposés. Deux polygones seront cachés par le troisième. Il n'est pas conseillé de manipuler des couches vecteur avec des entités superposées, même si c'est possible. Sinon, nous pouvons sélectionner l'option ``Prendre uniquement les entités de la première entité correspondante (un à un)``, auquel cas simplement le premier lieu de culte rencontré sera joint au polygone de la commune. Ainsi, à une commune donnée, il n'y aura bien qu'une ligne et un seul polygone. Par contre, nous ne contrôlons pas ce que QGIS entend par *première entité correspondante*...


..  _conversion-polygones-lignes:

Conversion de polygones en lignes
-----------------------------------

Il est possible de convertir un polygone, donc constitué d'une limite et d'une surface, en une simple polyligne, i.e. juste la limite sans la surface. Ce type de traitements n'est pas forcément très fréquent mais peut être utile.

.. _conversion-polygones-lignes-qgis:

Avec QGIS
**********

QGIS propose un module pour convertir un polygone en polylignes. Dans l'exemple suivant, nous allons convertir le polygone du département de l'Essonne vers une couche de type polylignes qui ne contiendra que les limites de ce département. Le module à utiliser se trouve dans le menu :menuselection:`Vecteur --> Outils de géométrie --> Polygones vers lignes...` (:numref:`polygones-lignes`).

.. figure:: figures/fen_qgis_polygones_vers_lignes.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: polygones-lignes
    
    Conversion de polygones vers lignes dans QGIS.

Dans le champ ``Couche source`` nous indiquons la couche de polygones à convertir, ici *Essonne.gpkg*. Dans le champ ``Lignes`` nous spécifions un chemin et un nom pour le fichier de lignes issu de la conversion, ici *Essonne_limites.gpkg*. Puis nous cliquons sur :guilabel:`Exécuter`.


..  _desagreger-geom:

Désagréger des géométries
--------------------------

Sur les couches vecteurs, de type points, lignes ou polygones, les entités ne correspondent pas toujours à chaque géométrie individuelle. Par exemple, une entité peut être composée de plusieurs polygones. C'est le cas, par exemple, de l'entité *France*, composée du polygone de la France continentale mais aussi de la Corse et des autres îles comme Oléron, Ré, Ouessant... (:numref:`fr_multi_polygones`).

.. figure:: figures/fig_France_multi_polygones.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: fr_multi_polygones

    La France en multi-polygones. Une seule entité de la table attributaire correspond à un polygone en plusieurs parties.

Si il est intéressant de travailler avec des entités multi-polygones dans certains cas, pour certains traitements il peut être utile de désagréger ces polygones afin d'obtenir une entité par géométrie. Ce processus de désagrégation se fait simplement dans les logiciels de SIG ou les langages de programmation comme R ou Python.

..  _desagreger-geom-qgis:

Désagréger des géométries dans QGIS
*************************************
Version de QGIS : 3.34.0

Pour désagréger une couche multi polygones (ou multi points ou multi lignes) chargée dans QGIS, il suffit d'aller dans le menu :menuselection:`Vecteur --> Outils de géométrie --> De morceaux multiples à morceaux uniques...` Le menu suivant apparaît (:numref:`desag-qgis`).

.. figure:: figures/fen_qgis_desagreger.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: desag-qgis

    Désagréger une couche multi-polygones dans QGIS.

À la ligne ``Couche source`` nous indiquons la couche multi-polygones à désagréger et à la ligne ``Géométries simples``, nous indiquons un nom et un chemin pour stocker la couche en géométries simples qui sera générée. Il ne reste qu'à cliquer sur :guilabel:`Exécuter`. Nous obtenons une couche comme présentée ci-dessous (:numref:`fr-desag`).

.. figure:: figures/fig_France_desag.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: fr-desag

La France, avec chaque polygone, i.e. chaque île, individualisée. Évidemment, les champs de la table attributaire n'ont pas été mis à jour. Le champ existant a été dupliqué autant de fois qu'il y a de géométries indépendantes.


..  _desagreger-geom-R:

Désagréger des géométries dans R
**********************************
Version de R : 4.3.2

Désagréger une couche vecteur dans R est aussi possible, aussi bien avec la librairie *sf* que *terra*.

Désagréger avec terra
......................
Version de terra : 1.7.29

Nous verrons ici comment désagréger une couche vecteur chargée dans R avec *terra* sous forme d'un *SpatVector*. Dans l'exemple, nous allons désagréger les géométries de la France.

.. code-block:: R

   # import des limites de la France
    lim_fr <- terra::vect('france_adm_2154.gpkg', layer='limites')
    # désagrégation des géométries
    lim_fr_desag <- terra::disagg(lim_fr)

Si nous regardons les propriétés de la variable *lim_fr*, nous voyons qu'elle ne contient qu'une seule géométrie alors que la variable *lim_fr_desag* en contient 26.



..  _geotraitements:

Géotraitements
--------------------
Grâce à la spatialisation des SIG, il est possible de croiser les géométries des entités de différentes couches entre elles par le jeu de ce que nous appelons communément des *géotraitements*. Ces géotraitements peuvent être des intersections, des unions, des différences de polygones ou de lignes parfois. Les géotraitements sont très utilisés, c'est une des fonctionnalités de base à connaître des SIG. La figure suivante récapitule les principaux géotraitements en SIG (:numref:`geotraitements-principes`).

.. figure:: figures/fig_geotraitements_principes.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: geotraitements-principes
    
    Principes de quelques géotraitements fréquemment utilisés.

Les **zones tampons** sont aussi connues sous l'appellation anglaise de **buffers** (*tampon*). Les zones tampons peuvent être calculées autour d'un polygone, d'une ligne ou d'un point. La taille de la zone tampon est souvent exprimée dans les mêmes unités que le SCR employé. Il est donc plus facile de créer des zones tampons avec des SCR exprimés en mètres plutôt qu'en degrés. Certains modules permettent de calculer des tampons unilatéraux le long des lignes, soit juste un tampon *à droite* ou un *à gauche*.

.. warning::
	Lors d'un géotraitement faisant intervenir deux couches, il est fortement recommandé que les deux couches soient dans le même système de coordonnées de référence.


..  _zones-tampons:

Zones tampons
**************

Les zones tampons permettent de créer une zone, donc une surface autour d'une ou plusieurs géométries en entrée. La dimension du tampon est la plupart des cas définie par l'utilisateur dans les paramètres du module dédié. Cependant certains modules proposent de calculer une zone tampon pour chaque entité de la couche en fonction d'une valeur définie dans un champ attributaire. Il est préférable de réaliser des zones tampons sur des couches dont le SCR est exprimé en mètres. Enfin, les zones tampons sont réalisables autour de polygones, de lignes ou de points.

.. note::
	Les zones tampons ne sont pas des *anneaux* encerclant juste la géométrie de départ, mais sont les géométries de départ qui ont été *élargies*.

..  _zones-tampons-qgis:

Zones tampons dans QGIS
+++++++++++++++++++++++++++
Version de QGIS : 3.22.3

QGIS propose plusieurs modules pour calculer des zones tampons autour d'une couche vecteur préalablement chargée dans le logiciel. Dans cet exemple nous allons créer une zone tampon de 500 m de large autour du réseau hydrographique du bassin-versant de la Roya dans les Alpes-Maritimes. Un tampon de 500 m m signifie ici que nous allons défnir en fait une surface de 1000 m de large, 500 m de chaque côté, autour de chaque tronçon de cours d'eau. Le module le plus couramment utilisé se trouve dans le menu :menuselection:`Vecteur --> Outils de géotraitements --> Tampon...` La fenêtre suivante s'ouvre (:numref:`qgis-tampons`).

.. figure:: figures/fen_qgis_tampon.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-tampons
    
    Création de zones tampons dans QGIS.

Dans le champ ``Couche source`` nous indiquons la couche autour de laquelle nous souhaitons obtenir une zone tampon *hydro_Roya.gpkg*. Dans le champ ``Distance`` nous indiquons la largeur du tampon désiré, ici 500 et nous choisissons comme unité ``mètres`` dans le menu déroulant. Il est important de cocher ou pas sur ``Regrouper le résultat``. Si nous ne cochons pas cette option, il y aura autant de tampons que d'entités dans la table de départ. Si nous cochons l'option, toutes les zones tampons de formeront qu'une seule entité, éventuellement multi-parties (:numref:`tampons-groupe`). Dans le champ ``Mis en tampon`` nous spécifions un nom et un chemin pour le tampon qui sera créé. Les autres options peuvent être laissées par défaut, elles influenceront peu la forme du résultat.

.. figure:: figures/fig_tampon_groupe.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: tampons-groupe
    
    Zones tampons groupées (A) et non groupées (B).

Si nous ne groupons pas le résultat des zones tampons (:numref:`tampons-groupe` B), les tampons se chevauchent les uns les autres.


..  _zones-tampons-R:

Zones tampons dans R
+++++++++++++++++++++++++++

Il est facile de créer des zones tampons dans R aussi bien sur des objets *SpatVector* que *sf*. Nous verrons ici la procédure pour les deux formats.

..  _zones-tampons-terra:

Zones tampons avec terra
^^^^^^^^^^^^^^^^^^^^^^^^^
Version de R : 4.3.1

Version de terra : 1.7.29

Nous commencerons par charger le réseau hydrographique de la Roya sous forme de polylignes en un objet *SpatVector*. Nous ferons ensuite un tempon de 100 m autour de chaque rivière grâce à la fonction ``buffer`` de la librairie *terra*. Pour finir, de façon facultative, nous regrouperons le résultat afin de n'avoir qu'un seul polygone pour tout le réseau.

.. code-block:: R

   # import du réseau hydrographique
   hydro_roya <- terra::vect('hydro_Roya_L93.gpkg')
   # tampon de 100 m
   hydro_roya_100 <- terra::buffer(hydro_roya, width=100)
   # regroupement du résultat
   hydro_roya_100 <- terra::aggregate(hydro_roya_100)


..  _zones-tampons-sf:

Zones tampons avec sf
^^^^^^^^^^^^^^^^^^^^^^^
Version de R : 4.3.1

Version de sf : 1.0.12

Ici nous verrons la procédure à suivre pour créer une zone tampon autour d'un segment de rivière (polylignes) dont le SCR est *Lambert 93*. Nous commencerons par charger cette portion de rivière en un objet ``sf``. Puis nous calculerons une zone tampon de 100 m autour de cette rivière grâce à la fonction ``st_buffer()`` de la librairie *sf*.

.. code-block:: R
   
   # import de la rivière
   affluent_roya <- sf::st_read('affluent_Roya_L93.gpkg')
   
   # création de la zone tampon de 100 m
   affluent_buffer <- sf::st_buffer(affluent_roya, dist=100)
   
   # facultatif : export du résultat
   sf::st_write(hydro_buffer, 'affluent_tampon_100m.gpkg')
   
.. note::
	La distance du tampon, donnée grâce au paramètre ``dist=`` est exprimée dans les unités du SCR (:ref:`intro-SCR`).

..  _intersection-polygones:

Intersection de polygones
**************************

Un traitement SIG très fréquent est *l'intersection* de deux couches vecteurs de polygones. C'est une manipulation courante lorsque nous souhaitons par exemple croiser un raster d'occupation du sol avec des entités administratives ou biophysiques comme des bassins-versants ou des zones géologiques. Tous les outils SIG proposent des solutions pour ce genre de traitements.

..  _intersection-polygones_qgis:

Intersection de polygones dans QGIS
++++++++++++++++++++++++++++++++++++++
Version de QGIS : 3.22.3

Il est tout d'abord nécessaire de charger dans QGIS les deux couches de polygones que nous souhaitons intersecter. Ces deux couches doivent être de préférence dans le même SCR. Dans l'exemple suivant nous allons intersecter un raster d'occupation du sol (*Corine Land Cover 2018*) avec les départements d'Île-de-France. Cette manipulation permet de connaître l'occupation du sol par département francilien. Nous allons dans le menu :menuselection:`Vecteur --> Outils de géotraitements --> Intersection...` Le menu suivant s'affiche (:numref:`intersect-polygones-qgis`).

.. figure:: figures/fig_qgis_intersection.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: intersect-polygones-qgis
    
    Intersection de couches de polygones dans QGIS.

Dans le champ ``Couche source`` nous indiquons la couche à intersecter, ici l'occupation du sol *CLC_2018_IDF.gpkg*. Dans le champ ``Couche de superposition`` nous indiquons la couche qui nous sert à intersecter, ici les départements *departements_IDF.gpkg*. Dans le champ ``Champs d'entrée à conserver`` nous pouvons indiquer quels champs attributaires de la première couche nous souhaitons conserver dans la couche finale, ici simplement l'identifiant CLC de l'occupation du sol. Dans le champ ``Champs à conserver`` nous pouvons indiquer quels champs attributaires de la seconde couche nous souhaitons conserver dans la couche finale, ici le code des départements, la population départementale et la superficie départementale par exemple. Puis dans le champ ``Intersection`` nous spécifions un chemin et un nom pour la couche résultat. Enfin, nous cliquons sur :guilabel:`Exécuter`. La couche des polygones d'occupation du sol intersectée par les départements apparaît bien dans QGIS.

.. note::
	Il est fréquent que dans l'une ou l'autre (ou les deux) couches d'entrées d'une intersection il existe un attribut *superficie*. Dans ce cas, cet attribut *superficie* sera également présent dans la couche final mais ne correspondra plus à la superficie des entités, puisqu'elles auront été intersectées. Attention donc à ne pas s'embrouiller à ce niveau. Il peut être utile de renommer l'attribut en question en *superficie_departement* par exemple (cf :ref:`renommer-champ`).


..  _intersection-polygones-R:

Intersection de polygones dans R
++++++++++++++++++++++++++++++++++++++

Il est possible d'intersecter deux couches vecteurs de type polygone dans R. Ici, nous allons intersecter les communes du Val-d'Oise (95) (*communes_95.gpkg*) avec la couche d'occupation du sol Corine Land Cover de l'Île-de-France (*CLC_2018_IDF.gpkg*). Nous verrons la procédure pour des objets de type *sf* et de type *SpatVector*.

..  _intersection-polygone-terra:

Intersection de polygone avec terra
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Version de R : 4.3.1

Version de terra : 1.7.29

Nous commençons par charger les deux couches vecteur en format *SpatVector*, puis nous utilisons la fonction ``intersect`` de la librairie *terra*.

.. code-block:: R

   # import des communes du 95
   communes_95 <- terra::vect('communes_95.gpkg')
   # import du CLC d'IDF
   clc_IDF <- terra::vect('CLC_2018_IDF.gpkg')
   # Intersection du CLC par commune
   clc_communes_95 <- terra::intersect(communes_95, clc_IDF)

..  _intersection-polygones-sf:

Intersection de polygones avec sf
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Version de R : 4.3.1

Version de sf : 1.0.12

Nous commençons par charger les deux couches vecteur en format *sf*, puis nous utilisons la fonction ``st_intersection`` de la librairie *sf*.

.. code-block:: R
   
   # import des communes du 95
   communes_95 <- sf::st_read('communes_95.gpkg')
   # import du CLC d'IDF
   clc_IDF <- sf::st_read('CLC_2018_IDF.gpkg')
   
   # intersection des deux couches
   clc_communes_95 <- sf::st_intersection(communes_95, clc_IDF)

.. note::
	Cette méthode fonctionne aussi pour des couches de type *lignes*.

..  _difference-polygones:

Différence de polygones
**************************

Faire la différence entre deux couches de polygones est une manipulation courante en SIG. Elle s'utilise par exemple dans le cas où nous souhaitons exclure une zone d'une autre. Par exemple nous souhaitons conserver simplement le territoire d'une région donnée qui ne soit pas couvert d'une certaine occupation du sol.

..  _difference-polygones_qgis:

Différence de polygones dans QGIS
++++++++++++++++++++++++++++++++++++++
Version de QGIS : 3.22.3

Dans l'exemple, nous souhaitons connaître la portion de territoire du département du Val-d'Oise qui n'est pas recouvert de forêt. Pour cela  nous chargeons dans QGIS le polygone du département du Val-d'Oise et le polygone des forêts d'Île-de-France. Une fois ces deux couches chargées dans QGIS, nous allons dans le menu :menuselection:`Vecteur --> Outils de géotraitements --> Différence...` Le menu suivant apparaît (:numref:`difference-polygones-qgis`).

.. figure:: figures/fen_qgis_difference.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: difference-polygones-qgis
    
    Différence de couches de polygones dans QGIS.

Dans le champ ``Couche source`` nous indiquons la couche de laquelle nous souhaitons exclure des portions, ici la couche du département *val-d-oise.gpkg*. Dans le champ ``Couche de superposition`` nous indiquons la couche à enlever, à savoir les forêts. Puis dans le champ ``Différence`` nous spécifions un chemin et un nom pour la couche résultat. Enfin nous cliquons sur :guilabel:`Exécuter`. La couche du Val-d'Oise *trouée* par les forêts apparaît alors dans QGIS (:numref:`difference-val_d_oise`).

.. figure:: figures/fig_difference_val_d_oise.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: difference-val_d_oise
    
    Les polygones de forêts superposés au polygone du Val-d'Oise (A) et le résultat de la différence qui permet d'obtenir les portions du Val-d'Oise non recouverts de forêt (B).

.. warning::
	Contrairement à l'intersection, le sens à ici un sens. Si nous avions mis la couche des forêts en premier et le département en second, nous aurions obtenu en résultat les forêts qui ne sont pas dans le Val-d'Oise.


..  _difference-polygones-R:

Différence de polygones dans R
++++++++++++++++++++++++++++++++++++++

Il est possible de faire une différence entre deux couches de polygones dans R, mais la manipulation diffère selon que nous travaillons avec des objets *sf* ou *SpatVector*. Nous verrons les deux. Nous allons récupérer les portions de communes du Val d'Oise qui ne sont pas recouvertes de forêts. Les communes du Val d'Oise proviennent de la couche *communes_95.gpkg* et les forêts proviennent de l'occupation du sol Corine Land Cover (codes 311, 312 et 313) stockées dans la couche *CLC_2018_forets_IDF.gpkg*.

..  _difference-polygone-terra:

Différence de polygones avec terra
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Version de R : 4.3.1

Version de terra : 1.7.29

Nous commençons par charger les deux couches au format *SpatVector*. Nous utiliserons ensuite la fonction ``erase`` de la librairie *terra*. L'ordre des couches a son importance. Dans le premier cas, nous enlèverons de la couche des communes du 95 toutes les forêts, et dans le second cas nous enlèverons de la couche des forêts toutes les communes du 95.

.. code-block:: R

   # import des communes du 95 (Val d'Oise)
   communes_95 <- terra::vect('communes_95.gpkg')
   # import des forrêts CLC IDF
   clc_forets_IDF <- terra::vect('CLC_2018_forets_IDF.gpkg')

   # difference pour avoir les communes sans les forêts
   comm_sans_forets <- terra::erase(communes_95, clc_forets_IDF)
   # différence pour avoir les forêts sans les communes du 95
   forets_sans_comm <- terra::erase(clc_forets_IDF, communes_95)


..  _difference-polygones-sf:

Différence de polygones avec sf
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Version de R : 4.3.1

Version de sf : 1.0.12

Il est nécessaire de passer par une fonction personnalisée. Les deux couches sont au préalable chargées en format ``sf``.

.. code-block:: R
   
   # import des communes du 95 (Val d'Oise)
    communes_95 <- sf::st_read('communes_95.gpkg')
    # import des forrêts CLC d'Île-de-France (IDF)
    clc_forets_IDF <- sf::st_read('CLC_2018_forets_IDF.gpkg')
    
    # écriture d'une fonction personnalisée st_erase
    sf::st_erase = function(x, y) st_difference(x, st_union(st_combine(y)))

    # on récupère les portions de communes non forestières grâce à la fonction st_erase
    communes_non_foret <- sf::st_erase(communes_95, clc_forets_IDF)
    
    # facultatif : on exporte le résultat
    sf::st_write(communes_non_foret, 'communes_non_foret.gpkg')


..  _fusion-polygones:

Fusion de polygones
**********************

Il est souvent utile de fusionner tous les polygones d'une couche vecteur afin de créer une nouvelle couche vecteur de type polygone qui correspondra en quelque sorte à l'enveloppe globale. Par exemple si nous disposons de l'ensemble des communes d'une région, il est possible de les fusionner afin de calculer les limites générales de la région. Dans les exemples suivants, nous travaillerons avec les communes d'Île-de-France (*communes_IDF.gpkg*) et nous les fusionnerons afin d'obtenir les limites de la région Île-de-France.

..  _fusion-polygones_qgis:

Fusion de polygones dans QGIS
++++++++++++++++++++++++++++++++
Version de QGIS : 3.26.1

Au préalable, nous chargeons la couche vecteur à fusionner dans QGIS. Une fois cette couche chargée, nous allons dans le menu :menuselection:`Vecteur --> Outils de géotraitement --> Regrouper...`. La fenêtre suivante s'affiche (:numref:`fusion-polygones-qgis`).

.. figure:: figures/fen_qgis_fusion_polygones.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: fusion-polygones-qgis
    
    Fusion de polygones dans QGIS.

Dans le menu ``Couche source`` nous indiquons la couche à fusionner, à savoir *communes_IDF.gpkg*. Dans le menu ``Champ de regroupement`` nous n'indiquons rien. Ainsi tous les polygones de la couche seront regroupés. Dans le menu ``Couche regroupée`` nous indiquons un num et un chemin pour la couche fusionnée, *limites_IDF.gpkg* par exemple. Puis nous cliquons sur :guilabel:`Exécuter`.

Une nouvelle couche des limites de l'Île-de-France apparaît bien.

..  _fusion-polygones_R:

Fusion de polygones avec R
++++++++++++++++++++++++++++++++

Il est possible de fusionner une couche de polygones avec R. La méthode diffère selon que nous travaillons sur un objet *sf* ou *SpatVector*. Nous verrons les deux cas.

..  _fusion-polygones_terra:

Fusion de polygones avec terra
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Version de R : 4.3.1

Version de terra : 1.7.29

Avec *terra* nous utiliserons la fonction ``aggregate``.

.. code-block:: R

   # import des communes d'Île-de-France
   communes_IDF <- terra::vect('communes_IDF.gpkg')

   # fusion des communes
   limites_IDF <- terra::aggregate(communes_IDF)


..  _fusion-polygones_sf:

Fusion de polygones avec sf
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Version de R : 4.3.1

Version de sf : 1.0.12

Dans un premier temps nous chargeons la couche à fusionner en tant qu'objet ``sf``. Puis nous effectuons la fusion grâce à la fonction ``st_union()`` de cette librairie ``sf``. Un exemple est donné ci-dessous.

.. code-block:: R
   
   # import des communes d'Île-de-France
   communes_IDF <- sf::st_read('communes_IDF.gpkg')

   # fusion des communes
   limites_IDF <- sf::st_union(communes_IDF)
   
   # facultatif : export du résultat
   sf::st_write(limites_IDF, 'limites_IDF.gpkg')


..  _intersection-lignes:

Intersection de lignes
*************************

Il est possible d'intersecter des lignes entre elles. Le résultat sera une couche vecteur de type *points* où chaque point correspondra à une intersection.

..  _intersection-lignes-qgis:

Intersection de lignes dans QGIS
++++++++++++++++++++++++++++++++++++
Version de QGIS : 3.22.3

Il est possible d'intersecter deux couches de type *lignes* dans QGIS, de préférence dans le même système de coordonnées de référence. À l'issue de ce traitement, une couche de points sera créée. À chaque point correspondra une intersection entre 2 lignes. Dans cet exemple, nous allons intersecter une couche linéaire de routes avec une couche linéaire de rivières. Les intersections correspondront ainsi finalement aux emplacements des ponts routiers (:numref:`intersect-lignes` A).

.. figure:: figures/fig_intersection_lignes.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: intersect-lignes
    
    intersection de lignes.

Pour ouvrir le menu d'intersection de lignes, nous allons dans le menu :menuselection:`Vecteur --> Outils d'analyse --> Intersection de lignes...` Le menu suivant s'ouvre (:numref:`intersect-lignes-qgis`).

.. figure:: figures/fen_qgis_intersection_lignes.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: intersect-lignes-qgis
    
    Intersection de lignes dans QGIS.

Dans le champ ``Couche source`` nous indiquons la première couche de lignes, les voies de communication par exemple. Dans le champ ``Couche d'intersection`` nous indiquons la couche à intersecter, ici le réseau hydrographique. Aux deux lignes ``Champ d'entrée à conserver`` nous pouvons indiquer quels attributs conserver sur les deux couches. Ce sont les attributs qui seront présents sur la couche ponctuelle issue du traitement. Nous pouvons également indiquer un chemin et un nom pour la couche qui sera créée dans le champ ``Intersections``. Nous cliquons pour finir sur :guilabel:`Exécuter`. Nous obtenons bien une nouvelle couche de points où chaque point correspond à une intersection entre 2 lignes (:numref:`intersect-lignes` B).

..  _intersection-lignes-R:

Intersection de lignes dans R
++++++++++++++++++++++++++++++++++++

L'intersection de lignes dans R se fait exactement comme pour l'intersection de polygones. Il suffit de charger deux couches de type *lignes* et faire l'intersection dessus à l'aide de la fonction d'intersection selon que nous travaillons avec des objets *sf* ou *terra*. Le résultat de l'intersection est alors une couche de points. Reportez vous à la partie dédiée : :ref:`intersection-polygones-R`.


..  _creation-grilles:

Création de grilles
----------------------

De temps en temps, il peut être nécessaire de générer des grilles vecteurs régulières sur un espace géographique donné. Ces grilles peuvent être sous forme de lignes, de points ou de polygones. Et les polygones peuvent être carrés, rectangulaires, hexagonaux ou autres. Cette manipulation peut être intéressante pour créer une grille cartographique avec les latitudes et longitudes, disposer des points de façon régulière pour un échantillonnage ou bien encore pour découper un grand raster en dalles plus petites et régulières. Cette création de grilles peut se faire en utilisant différents outils.

..  _creation-grilles-qgis:

Création de grilles dans QGIS
**********************************
Version de QGIS : 3.24.3

Dans QGIS il est facile de créer une grille vecteur personnalisée sur un espace donné. Pour cet exemple, nous allons créer différentes grilles régulières sur l'emprise d'une portion de scène Landsat 8 prise au-dessus de la ville de Shanghai en Chine (:numref:`grille-emprise-qgis`).

.. figure:: figures/fig_qgis_grille_emprise.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: grille-emprise-qgis
    
    Raster sur lequel créer la grille régulière et ses propriétés.

Avant de créer la grille il est important de savoir en combien de parties nous voulons découper notre espace. Dans notre exemple, nous allons créer une grille de 5 colonnes sur 5 lignes, mais nous aurions pu prendre tout autre chose, même un nombre de colonnes différents du nombre de lignes. Une fois ces nombres de lignes et colonnes choisis, il est nécessaire de relever les dimensions en *X* et en *Y* du raster qui sert de référence (:ref:`explorer-raster-qgis`). Ces dimensions doivent être exprimées en unités du système de coordonnées de référence et non en nombre de pixels. Ici (:numref:`grille-emprise-qgis`), notre raster s'étend en *X* de 298935 à 335235 et en *Y* de 3435555 à 3470715. Notre raster étant géoréférencé en UTM Zone 51N, il s'agît de coordonnées métriques. L'étendue en *X* de notre raster est donc de 335235 m - 298935 m soit 36300 m. De même, en *Y* notre raster s'étend sur 3470715 m - 3435555 m soit 35160 m. Comme nous souhaitons construire une grille de 5 colonnes sur 5 lignes, nous divisons ces deux étendues par 5. Ce qui donnera des *cases* de 7260 m de largeur (36300 / 5) et de 7032 m de hauteur (35160 / 5).

Une fois en tête ces éléments, nous pouvons lancer le menu de création de grilles de QGIS. Nous cliquons sur le menu :menuselection:`Vecteur --> Outils de recherche --> Créer une grille...` Le menu suivant s'ouvre (:numref:`grille-creation-qgis`).

.. figure:: figures/fen_qgis_grille_creation.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: grille-creation-qgis
    
    Création d'une grille vecteur dans QGIS.

Dans le menu ``Type de grille`` nous choisissons ``Rectangle (polygone)``. Dans la zone de texte ``Étendue de la grille``, soit nous entrons une étendue à la main exprimée dans le SCR désiré et sous la forme *Xmin,Xmax,Ymin,Ymax*, soit (plus simple) nous sélectionnons une emprise selon la couche raster chargée. Cette étendue se trouve en cliquant sur la petite flèche dirigée vers le bas à droite de la zone de texte puis en utilisant le menu ``Calculer depuis la couche`` et en sélectionnant la couche de référence, ici notre image Landsat.
Dans le menu ``Espacement horizontal``, nous entrons la dimension horizontale d'une *case* telle que calculée au préalable, à savoir *7260*. De même, dans le menu ``Espacement vertical``, nous entrons la dimension en hauteur d'une case, telle que calculée plus haut, à savoir 7032. Nous pourrions changer les unités de ces espacements si désiré. Il est possible de faire superposer les éléments de la grille les uns sur les autres grâce aux menus ``Superposition horizontale`` et ``Superposition verticale``. Ici, nous ne voulons pas de superposition, alors nous laissons ces valeurs à 0. Il est ensuite important de choisir le SCR dans lequel sera créée la grille. Par défaut c'est celui du projet. Enfin dans le menu ``Grille`` nous indiquons un chemin et un nom pour la grille qui sera créée. Il ne reste plus qu'à cliquer sur :guilabel:`Exécuter`. Le résultat s'affiche directement dans QGIS (:numref:`grille-rsl-qgis` A).

.. figure:: figures/fig_grilles.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: grille-rsl-qgis
    
    Grilles créées dans QGIS : grille en polygones (A), en lignes (B), en points (C) et en hexagones (D).

    .. warning::
	La grille peut être un tout petit peu plus petite que le raster initial. Pour être sûr de superposer le raster dans son entièreté il est possible d'ajouter quelques mètres (5 ou un peu plus) aux espacements horizontaux et verticaux.


..  _exploser-vecteurs:

Exploser des couches vecteurs
------------------------------

Par *exploser* une couche vecteur nous entendons enregistrer chaque entité d'une couche vecteur en une couche indépendante. Cette manipulation est aussi connue sous le nom de *séparer* une couche vecteur. Avec ce traitement, chaque enregistrement d'une table attributaire est individualisé et stocké dans une couche vecteur indépendante. Par exemple, si nous disposons d'une couche vecteur contenant les 96 départements de France métropolitaine et que nous la *séparons*, nous obtiendrons 96 couches indépendantes (au format *shp*, *gpkg*, ou autre), une par département. Cette manipulation n'est pas très courante mais peut s'avérer utile.

..  _exploser-vecteurs-qgis:

Séparer une couche vecteur dans QGIS
**************************************
Version de QGIS : 3.24.3

Dans cet exemple, nous allons créer une couche vecteur par département français en partant d'une couche des départements pour toute la France (:numref:`couche-departements`).

.. figure:: figures/fig_couche_departements.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: couche-departements
    
    Couche vecteur des départements et sa table attributaire avec un département par ligne.

Pour exploser cette couche, nous allons dans le menu :menuselection:`Vecteur --> Outils de gestion de données --> Séparer une couche vecteur...` Le menu suivant s'ouvre (:numref:`qgis-separer-vecteur`).

.. figure:: figures/fen_qgis_separer_vecteur.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-separer-vecteur
    
    Menu QGIS pour séparer une couche vecteur.

Dans le menu ``Couche source`` nous indiquons la couche à séparer, ici *departements_france_L93*. Dans le menu ``Champ d'identifiant unique`` nous indiquons le champ attributaire qui servira à nommer les couches en sortie, ici le champ renfermant le code du département par exemple, *Code_Depar*. Dans le menu ``Type de fichier de sortie``, nous choisissons un format pour les couches individuelles qui seront créées, par exemple *gpkg*. Enfin, dans le menu ``Répertoire de destination``, nous indiquons le répertoire qui contiendra toutes les couches créées. Il ne reste plus qu'à cliquer sur :guilabel:`Exécuter`. Les départements sont maintenant explosés en une couche chacun (:numref:`departements-separes`).

.. figure:: figures/fig_departements_separes.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: departements-separes
    
    Les départements stockés chacun dans une couche indépendante.
