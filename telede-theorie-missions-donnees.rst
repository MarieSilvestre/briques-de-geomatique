..  _theorie_telede_optique:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Télédétection : théorie, missions, données
==============================================

Cette fiche présente quelques éléments de théorie relatifs à la télédétection optique. Cette théorie est à dessein très vulgarisée et ne fera pas appel à des bases physiques poussées. L'idée est de comprendre de façon qualitative les grands principes sous-jacents à la télédétection optique afin de manipuler les images de façon éclairée. Si vous êtes intéressés par la physique du signal et des images, reportez vous à des cours ou des ouvrages spécialisés dans ce domaine. Dans cette présentation, nous nous efforcerons de lier la théorie à la pratique en nous appuyant sur des exemples concrets.

Suit une partie présentant les grandes missions d'observation de la Terre par satellite couramment utilisées en géographie.

Pour finir, quelques portails permettant de télécharger facilement les données sont présentés de façon pratique.

.. contents:: Table des matières
    :local:

Quelques points de théorie
----------------------------

**Télédétection optique**

Commençons par définir ce qu'est la télédétection. Selon le Larousse, la télédétection est la "*Technique d'acquisition à distance d'informations sur la surface terrestre, principalement fondée sur l'analyse d'images obtenues dans différentes gammes de longueurs d'onde à partir d'aéronefs ou de satellites.*" Il faut traduire le terme d'*aéronef* par *avion ou drone*. Cette définition sera amplement suffisante pour nos besoins. Dans cette documentation, nous verrons en effet différentes techniques pour exploiter des images multi-spectrales prises à partir de satellites en orbite autour de la Terre. Nous verrons également, moins en détails, des techniques pour exploiter des images issues de prises de vue par avion ou par drone. Dans la quasi totalité des cas, nous traiterons en effet d'images prises dans différentes gammes de longueurs d'ondes.

Ici, nous ajoutons le qualificatif *optique* à *télédétection* car cette partie théorie ne se focalisera que sur les images qualifiées de *optique* opposées aux images qualifiées de *radar*. Les images *optiques* sont les images prises dans les gammes de longueurs d'ondes correspondantes au domaines du visible et de l'infrarouge, à savoir de 0.4 µm à 13 µm à peu près. Les images *radar*, quant à elles, sont prises dans les gammes de longueurs d'ondes de l'ordre de quelques décimètres correspondantes aux *micro-ondes*. Les techniques d'analyses de ces deux familles d'images sont sensiblement différentes, c'est pourquoi ces deux techniques sont souvent séparées.

**Télédétection passive**

La télédétection peut être divisée en télédétection *passive* et télédétection *active*. La télédétection passive se "contente" d'analyser l'énergie telle que réfléchie ou émise par la surface terrestre. Le capteur embarqué ne joue que le rôle de *récepteur*. La télédétection *active*, quant à elle, émet un signal en direction de la surface terrestre et analyse l'écho de se signal reçu en retour. La télédétection optique est dans la plupart des cas *passive* mais elle peut également être *active* dans le cas du Lidar. Par contre, la télédétection *radar* est toujours *active*.

Pour récapituler, dans le domaine optique, la télédétection est le plus souvent passive mais peut être active dans le cas du Lidar. Dans le domaine des micro-ondes, la télédétection est le plus souvent active - cas du radar - mais est également passive dans certains cas. Dans cette fiche, nous ne présenterons que la télédétection optique passive.

À la base de la télédétection optique : le Soleil
*****************************************************

Dans cette partie théorie, nous suivrons le cheminement d'un rayon de lumière, ou d'un photon, depuis son émission par le Soleil, jusqu'à sa réception par un capteur embarqué sur un satellite, après avoir interagi avec la surface terrestre.

Le Soleil est l'étoile la plus proche de la Terre et est l'étoile autour de laquelle notre planète orbite. Le Soleil est situé à peu près à 150 millions de kilomètres de la Terre et sa lumière met environ 8 minutes pour nous parvenir. En fait, la "lumière" que nous percevons n'est que la fraction que nous pouvons percevoir avec nos yeux de l'ensemble du rayonnement électromagnétique émis par notre étoile. En effet, le Soleil, comme la plupart des étoiles, émet des rayonnements électromagnétiques dans des gammes de longueurs d'ondes très différentes.

Le Soleil est la source de la télédétection car c'est son rayonnement qui, après interaction avec la surface terrestre, sera mesuré par les capteurs embarqués sur les satellites. Si un jour le Soleil s'éteint, seule la télédétection *active* sera possible (mais ce ne sera sûrement pas le problème principal à ce moment là...).

Onde et longueur d'onde
++++++++++++++++++++++++

Toute onde électromagnétique, ou tout rayonnement électromagnétique, est le résultat de la vibration couplée d'un champ électrique et d'un champ magnétique (:numref:`SCP-band-set` B). Toute onde est caractérisée par une longueur d'onde. Cette longueur d'onde se note fréquemment par la lettre grecque λ et représente la longueur séparant deux points identiques de l'onde, deux sommets par exemple. Elle représente la périodicité spatiale des oscillations. Celle longueur d'onde s'exprime en mètre, ou en subdivision du mètre. La longueur d'onde est aussi la distance parcourue par l'onde pendant une période d'oscillation. Elle est donc inversement proportionnelle à la fréquence. Les ondes peuvent avoir une longueur d'onde tendant vers des valeurs extrêmement petites (< 1pm) ou extrêmement grandes (milliers de kilomètres). Le champ des longueurs d'ondes est continue, c'est-à-dire qu'une infinité de valeurs est possible (:numref:`SCP-band-set` B). C'est pour cette raison que chaque onde peut être repérée dans ce que nous appelons le *spectre électromagnétique*.

.. figure:: figures/fig_ondes.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: ondes
    
    Déplacement d'une onde dans le champ électromagnétique (A) et différentes longueurs d'ondes (B), de la plus petite à la plus grande (de B1 à B3).

Il existe une relation directe entre longueur d'onde et fréquence :

.. math::
    \lambda = \frac{v}{f}

où :

* *λ* est la longueur d'onde exprimée en mètre
* *v* est la vitesse de déplacement de l'onde, qui est assimilable à la vitesse de la lumière dans le vide dans la plupart des cas, soit 300 000 km/s
* f est la fréquence exprimée en Hertz

Ainsi, plus la longueur d'onde est petite plus la fréquence est élevée et inversement, plus la longueur d'onde est grande plus la fréquence est faible.


Le spectre électromagnétique
++++++++++++++++++++++++++++++++
Comme dit précédemment, l'ensemble des longueurs d'ondes, de la plus petite à la plus grande, forme le *spectre électromagnétique* (:numref:`spectre`). Ce spectre permet de regrouper les longueurs d'ondes par grand domaine comme le *visible*, l'*infrarouge*, les ondes *radios*, ... Ce spectre est typiquement représenté avec les plus petites longueurs d'ondes sur la gauche et les plus grandes sur la droite. Chaque longueur d'onde possède un niveau d'énergie. Ce niveau est d'autant plus élevé que la longueur d'onde est petite.

.. figure:: figures/fig_spectre.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: spectre
    
    Le spectre électromagnétique et les principaux domaines de longueurs d'ondes.

Les longueurs d'ondes les plus faibles appartiennent au domaine des rayons *gamma*. Ces rayons se retrouvent notamment dans les processus liés à la radioactivité. Ces rayons sont mortels pour le vivant. Nous retrouvons ensuite les rayons X, utilisés par exemple en radiographie médicale. Ces rayons sont nocifs mais acceptables à faible dose. Vient ensuite le domaine des ultra-violets (UV), dont il ne faut pas abuser pour la santé. Le domaine suivant est le domaine du *visible*, c'est-à-dire le domaine perceptible par l’œil humain. C'est bien sûr le domaine le plus important pour les êtres humains mais pour toute la vie animale en général. C'est également un domaine très utilisé en télédétection. Ce domaine a une amplitude très faible, de 400 nm pour le bleu à 700 nm pour le rouge. Ensuite, le domaine de l'infrarouge s'étend sur une large amplitude. Il s'agît du deuxième domaine important en télédétection et de loin le plus intéressant pour l'étude de la végétation. Ensuite nous trouvons le domaine des ondes radios utilisé par de nombreuses activités : radio, wi-fi, téléphone cellulaire, ... Et enfin, le domaine des grandes ondes regroupe toutes les ondes dont la longueur d'onde est supérieure à quelques centaines de mètres. Chacun de ces grands domaines est découpé de `façon officielle`_ en sous domaines par l'Union Internationale des Télécommunications.

.. note::
    Nous pouvons noter que souvent nous désignons les rayonnements les plus petits par leurs longueurs d'ondes, mais à partir du domaine des micro-ondes nous les exprimons par leurs fréquences. Ainsi, nous dirons que le bleu correspond à une longueur d'onde de 400 nm mais que Radio Libertaire émet sur une onde qui a une fréquence 89.4 MHz.

Il est utile d'avoir en tête les `longueurs d'ondes qui correspondent aux principales couleurs`_, nous verrons par la suite qu'elles correspondent à des bandes spectrales de nombreux capteurs. La figure suivante récapitule les longueurs d'onde du domaine du visible (:numref:`spectre_visible`).

.. figure:: figures/fig_spectre_visible.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: spectre_visible
    
    Couleurs et longueurs d'ondes.

D'une manière générale, le tableau suivant présente les bandes de longueurs d'ondes approximatives des domaines les plus utilisés en télédétection optique. Les bornes inférieures et supérieures ne sont données qu'à titre indicatif, il n'y a en fait pas de valeurs absolues définies une fois pour toutes.

.. list-table:: Quelques domaines utiles pour la télédétection optique
   :widths: 25 15 15
   :header-rows: 1

   * - Domaine
     - λ inférieure (µm)
     - λ supérieure (µm)
   * - Bleu
     - 0.45
     - 0.52
   * - Vert
     - 0.52
     - 0.60
   * - Rouge
     - 0.63
     - 0.69
   * - Red Edge
     - 0.71
     - 0.80
   * - Proche infrarouge
     - 0.75
     - 0.90
   * - Infrarouge moyen
     - 1.20
     - 2.50
   * - Infrarouge thermique
     - 10.40
     - 12.50

Interaction avec l'atmosphère
*******************************

Nous avons vu que le Soleil émet un rayonnement électromagnétique dans une très grande partie du spectre électromagnétique et dans toutes les directions de l'espace. Une petite partie de ce rayonnement parvient donc à notre planète et va devoir traverser (ou pas) l'atmosphère terrestre avant de parvenir à la surface. De pad les propriétés physico-chimiques des gaz constitutifs de l'atmosphère, certaines longueurs d'ondes seront stoppées par l'atmosphère et d'autres pourront la traverser. Les ondes qui peuvent la traverser sont situées dans ce que nous appelons des *fenêtres atmosphériques*. La figure suivante répertorie les fenêtres atmosphériques et les portions opaques de l'atmosphère (:numref:`fenetres-atm`).

.. figure:: figures/fig_fenetres_atmospheriques.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: fenetres-atm
    
    Fenêtres atmosphériques.

Sur cette figure (:numref:`fenetres-atm`), une opacité de 100 % correspond à un domaine de longueurs d'ondes pour lequel l'atmosphère est totalement opaque. C'est-à-dire que les ondes de ce domaine sont totalement stoppées par l'atmosphère et ne parviennent pas à la surface terrestre. C'est notamment le cas de toutes les ondes à forte énergie représentées par les ondes à longueurs d'ondes très petites. Ainsi, les rayons gamma, les rayons X et la majorité des UV sont bloqués par l'atmosphère. D'ailleurs, sans ce blocage, il n'y aurait pas de vie sur Terre, car ces rayonnements provoquent des mutations au niveau de l'ADN des cellules vivantes. Seule une petite partie des UV, les UV C, pénètrent l'atmosphère et est responsable du bronzage ou des coups de Soleil.

Ensuite, le rayonnement dans le domaine du visible se trouve évidemment dans une fenêtre atmosphérique. C'est pourquoi l'évolution a "produit" un l’œil des animaux sensible à cette gamme de longueurs d'ondes. Si l'atmosphère 
devenait opaque à ces longueurs d'ondes, nous aurions l'impression de vivre dans une nuit permanente. Les satellites en orbite, donc au delà des couches supérieures de l'atmosphère, profite également de cette fenêtre pour scanner la surface terrestre dans plusieurs bandes de ce domaine.

Les domaines de l'infrarouge proche et moyen, aux alentours de 1 - 2 µm sont également des des fenêtres atmosphériques. La transparence n'est pas de 100 % dans ce domaine mais largement suffisante pour voir la surface dans ces gammes de longueurs d'ondes depuis l'espace. Nous retrouvons ensuite une fenêtre étroite aux alentours de l'infrarouge thermique dans les longueurs d'ondes de l'ordre de 10 - 12 µm. Cette gamme est également suivie par certains satellites. Mais la plus grande partie du domaine de l'infrarouge est opaque, entre 50 µm et quelques millimètres. C'est justement dans cette gamme de longueurs d'ondes que la Terre émet un rayonnement, appelé *rayonnement tellurique*, qui est à l'origine de *l'effet de serre*.

Une grande fenêtre totalement transparente se trouve ensuite pour les longueurs d'ondes radio comprises entre quelques centimètres et quelques mètres. Il est ainsi possible d'observer l'espace lointain par *radioastronomie* dans cette gamme de longueurs d'ondes. Grâce à cette fenêtre, il est également possible de communiquer en ondes radios avec les vaisseaux spatiaux. Enfin, l'atmosphère est opaque pour les ondes radios les plus grandes.

Interaction avec la surface
*********************************

Les trois composantes de l'interaction
++++++++++++++++++++++++++++++++++++++++++

Le rayonnement électromagnétique qui n'a pas été bloqué par l'atmosphère peut parvenir jusqu'à la surface terrestre. Ce rayonnement va interagir avec la surface de trois façons possibles. Une part du rayonnement sera *réfléchi*, une part sera *absorbée* et une autre sera *transmise*. Cette interaction est présentée dans la figure suivante (:numref:`rayon-surf`).

.. figure:: figures/fig_rayonnement_surface.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: rayon-surf
    
    Devenir d'un rayon incident sur la surface terrestre.

Dans tous les cas, la somme des énergies réfléchies, transmises et absorbées est égale à l'énergie incidente. Ainsi pour un rayon incident de 1, 0.5 peut être réfléchi, 0.25 transmis et 0.25 absorbée. Dans le cas d'une surface réfléchissant parfaitement, l'énergie réfléchie sera de 1 et les énergies transmise et absorbée seront de 0. En télédétection, c'est la part du rayonnement réfléchi qui va nous intéresser, car c'est celui-ci qui sera mesuré par le capteur à bord du satellite, de l'avion ou du drone.

Le concept de signature spectrale
+++++++++++++++++++++++++++++++++++++

Cette discrétisation en trois composantes du rayonnement incident dépend bien sûr de la nature de la surface mais aussi de la longueur d'onde du rayonnement incident. Ainsi, une surface d'eau ou de végétation réfléchiront différemment le rayonnement solaire incident. Mais en plus, une surface donnée réfléchira différemment la composante *bleu* du rayonnement incident de la la composante *vert* et ainsi de suite. Ainsi, chaque état de surface est associée à une *signature spectrale* qui lui est propre. Tout le jeu de la télédétection est finalement d'identifier les états de surface en fonction de leurs signatures spectrales. C'est vraiment sur ce concept de signature spectrale que repose toute la télédétection.

Les signatures spectrales peuvent être mesurées en laboratoire ou sur le terrain à partir de spectro-radiomètre. Ces signatures seront proches des signatures "pures", alors qu'en télédétection nous aurons affaire à des signatures plus ou moins bruitées selon la résolution spatiale du capteur et le niveau d'hétérogénéité de la surface. Par exemple, un lac aux eaux claires et sans algues aura une surface homogène et donc une signature spectrale proche de la signature parfaite de l'eau. Par contre, une forêt aura bien une signature typique de la végétation mais dégradée du fait des ombres, des éventuels arbres morts et des éventuelles portions de sol nu qui seraient visibles depuis le capteur.

La figure suivante présente quelques signatures spectrales de surface couramment rencontrées (:numref:`sign-spec`). 

.. figure:: figures/fig_signatures_spectrales.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: sign-spec
    
    Quelques signatures spectrales emblématiques.

L'eau présente une signature spectrale caractéristique. L'eau réfléchit très peu dans toutes les longueurs d'ondes et quasiment pas du tout dans le domaine de l'infrarouge. Elle présente simplement un très léger pic dans le domaine du bleu. D'où sa coloration bleu sombre. Cette signature est valable pour de l'eau pure. Dès que l'eau observée sera chargée en matières en suspension ou en algues, sa signature sera légèrement différente.

À l'opposé, la neige réfléchit énormément dans toutes les longueurs d'ondes, dans le visible comme dans la première partie de l'infrarouge. C'est pour cette raison que naturellement la neige apparaît d'une blancheur éblouissante. Cette blancheur peut tirer un peu sur le bleuté car la neige a tendance à réfléchir encore un peu plus dans le domaine du bleu. Par contre, le pouvoir de réflexion de la neige s'effondre totalement dans le moyen infrarouge. Si nous pouvons voir la neige avec nos yeux à une longueur d'onde de 1.5 µm, elle nous apparaîtrait noire.

Le sable réfléchit également beaucoup dans toutes les longueurs d'ondes, mais moins que la neige. Même si il réfléchit beaucoup, il réfléchit de plus en plus en partant du domaine du bleu. C'est pour cette raison que nous voyons le sable de couleur jaune clair.

La végétation présente également une signature tyique qu'il faut retenir. Elle réfléchit peu dans le bleu et peu dans le rouge mais présente un pic dans le domaine du vert. Cette signature dans le domaine du visible explique pourquoi nous voyons la végétation en vert. Par contre, la spécificité de la végétation est de présenter un très fort pic dans le proche infrarouge, aux alentours de 0.75 µm jusqu'à 1.3 µm. Ce pic est caractéristique de la végétation. Si nous étions capables de voir dans l'infrarouge, la végétation ne nous apparaîtrait pas vert mais d'un bel infrarouge pétant. Cette signature typique de la végétation est à moduler en fonction du type de végétation. Par exemple sur la figure précédente, nous constatons que l'herbe présente un pic dans le proche infrarouge plus marqué que le chêne. Il est donc possible, dans une certaine mesure de distinguer les différents types de végétation, voire même certaines espèces. 

Mesure du rayonnement réfléchi
***********************************

Satellite et capteur
+++++++++++++++++++++++

Comme dit précédemment, ce qui va nous intéresser en télédétection c'est le rayonnement réfléchi par la surface. C'est ce rayonnement réfléchi qui va être mesuré par le capteur embarqué sur le satellite (ou l'avion ou le drone). Même si par abus de langage nous faisons la confusion entre capteur et satellite, les deux sont différents. Le satellite est la plateforme qui supporte le capteur. Le capteur est, quant à lui, l'appareil qui mesure le rayonnement réfléchi par la surface lui parvenant. C'est bien le capteur l'élément central. Le satellite est là pour maintenir le capteur en orbite, l'alimenter en énergie via des panneaux solaires et pour le faire communiquer avec la surface pour la transmission des données par exemple (:numref:`sat-capteur`).

Dans la plupart des cas le satellite et le capteur ont des noms différents. Par exemple, le satellite Landsat 8 embarque un capteur nommé *OLI* (*Optical Land Imager*). Ainsi, en toute rigueur nous ne devrions pas parler *d'image Landsat 8* mais *d'image OLI*. D'autant plus, que plusieurs capteurs peuvent être embraqués sur un même satellite. Toujours dans le cas de Landsat 8, en plus du capteur OLI, nous y trouvons le capteur *TIRS* (*Thermal Infrared Sensor*) qui mesure le rayonnement en provenance de la surface dans le domaine de l'infrarouge thermique. Il en va de même pour le satellite européen Sentinel-2 qui embarque le capteur *MSI* (*Multispectral Instrument*).

.. figure:: figures/fig_satellite_sensor.jpg
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: sat-capteur
    
    Les capteur OLI et TIRS sur le *Space craft Bus* de Landsat 8 (`image Wikimedia Commons`_).

Le satellite, embarquant le ou les capteurs, est mis en orbite autour de la Terre selon des critères précis. Son orbite définira, entre autres, son temps de retour au-dessus du même zone (16 jours pour Landsat, 5 jours pour Sentinel-2), la largeur de sa fauchée ou son angle de prise de vue. Même si les satellites sont en orbite à plusieurs centaines de kilomètres au-dessus de la surface, ils sont encore soumis aux frottements des dernières traces de l'atmosphère terrestre. Ce frottement ralentit le satellite, ce qui modifie son orbite. Des corrections d'altitude de vol sont alors nécessaires régulièrement. Lorsque le carburant nécessaire à ces manœuvres vient à manquer, le satellite est en fin de vie. C'est souvent le critère déterminant la durée de vie d'un satellite. Au final, l'orbitographie est une science en soi.

.. tip::
	Il existe des outils pour visulaiser les orbites des différents satellites et ainsi connaître les dates de survol pour un lieu donné de la surface terrestre (ou d'une autre planète). Un de ces outils est le logiciel libre `Ixion`_ développé par des chercheurs du CNRS et de l'École polytechnique.

Décomposition en pixels
+++++++++++++++++++++++++++

Le capteur à bord du satellite mesure le rayonnement réfléchi par la surface lui parvenant. Lorsque le satellite survole une portion de la surface, appelée *scène*, le capteur mesure le rayonnement réfléchi par cette scène et la stocke sous forme d'une image décomposée en pixels (:numref:`scene-pixels`).

.. figure:: figures/fig_scene_pixels.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: scene-pixels
    
    Quelques signatures spectrales emblématiques.

La figure précédente (:numref:`scene-pixels`) présente un exemple simple qui nous permettra de comprendre comment le capteur enregistre le rayonnement qui lui parvient. Dans cet exemple, le satellite survole une scène simple sur laquelle nous trouvons de la forêt, de l'eau et une ville. Cette scène est décomposée en pixels par le capteur. Le raster résultant présente 42 pixels, agencés en 9 colonnes et 7 lignes. Ses dimensions sont donc de 9 × 7. Chaque pixel enregistre le rayonnement moyen réfléchi par la portion de surface correspondante. Ainsi, le pixel en haut à gauche n'enregistre que du rayonnement réfléchi par la forêt. Ce rayonnement sera donc caractéristique de la végétation. De même, le pixel en bas à droite n'enregistre que du rayonnement réfléchi par le lac. Il sera donc caractéristique d'une surface en eau. Par contre, les pixels du milieu qui sont à cheval entre de la forêt et l'eau enregistrent un rayonnement qui est un mixte entre celui de la forêt et de l'eau. Ils ne seront donc caractéristiques ni de l'un ni de l'autre.

.. warning::
	En réalité, dans la quasi totalité des cas, les pixels ne sont jamais homogènes. C'est d'autant plus vrai pour les résolutions spatiales les plus grossières. Même un pixel d'océan peut présenter une hétérogénéité due à une efflorescence algale, à la présence d'un récif, au passage d'un bateau, ...

Décomposition en bandes spectrales
+++++++++++++++++++++++++++++++++++++

En plus d'une décomposition en pixels, le capteur décompose le rayonnement reçu en bandes spectrales. C'est-à-dire que le capteur n'enregistre pas la totalité du rayonnement d'un coup, mais l'enregistre gamme de longueurs d'ondes par gamme de longueurs d'ondes. Dans notre, exemple, nous considérerons que le capteur mesure le rayonnement réfléchi dans quatre bandes spectrales : le domaine du bleu, du vert, du rouge et du proche infrarouge. Tout se passe comme si le capteur regardait la même scène mais dans quatre bandes spectrales différentes. Pour chaque bande spectrale, il enregistre le niveau d'énergie réfléchie correspondant. Ainsi, en sortie nous n'obtiendrons pas une mais quatre images de la même zone : une image dans la bande spectrale du bleu, une dans celle du vert, une dans celle du rouge et une dernière dans celle du proche infrarouge (:numref:`scene-bandes`).

.. figure:: figures/fig_scene_bandes_spectrales.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: scene-bandes
    
    Une même scène vue dans quatre bandes spectrales différentes.

Sur la figure précédente, nous retrouvons notre scène initiale composée d'eau, de forêt et de ville mais vue dans les quatre bandes spectrales du satellite. Dans la première bande, le capteur a mesuré l'énergie qui lui parvient depuis la surface dans le domaine du Bleu. Comme vu plus haut, l'eau réfléchit un peu dans le bleu, le capteur va donc enregistrer un peu d'énergie pour les pixels correspondant à l'eau. Par convention, sur un raster en niveaux de gris, un pixel recevant beaucoup d'énergie va apparaître clair, et inversement un pixel recevant peu d'énergie va apparaître foncé. Ici, les pixels d'eau apparaissent donc en gris pas trop foncé. Par contre, la végétation comme le bâti réfléchissent peu dans le domaine du bleu. Les pixels correspondants à ces surfaces vont donc apparaître très foncés.

Dans la deuxième bande spectrale correspondant au domaine du Vert, le capteur mesure l'énergie réfléchie dans ce domaine. Seule la végétation qui réfléchit un peu dans le vert va apparaître claire. L'eau comme le bâti réfléchissent peu et vont donc apparaître très foncé. Dans la troisième bande spectrale, celle du rouge, les trois surfaces réfléchissent très peu, la scène va donc apparaître très sombre partout.

Enfin, la quatrième bande spectrale, celle du proche infrarouge est très intéressante pour la végétation. Nous avons vu plus haut que la végétation réfléchit très fortement dans le domaine du proche infrarouge, c'est une de ces caractéristiques les plus fortes. Les pixels correspondant à la forêt vont ainsi apparaître en gris très clair. Par contre l'eau ne réfléchit pas du tout dans le proche infrarouge. Les pixels d'eau vont donc apparaître  noirs. Le bâti réfléchit juste un peu plus que l'eau dans ce domaine, il apparaîtra donc très foncé mais pas totalement noir.


Ce que mesure le capteur
++++++++++++++++++++++++++

Le capteur en lui-même mesure des *niveaux d'énergie* dans différentes bandes spectrales que nous pouvons assimiler à des *puissances*. Ce niveau de mesure n'est pas suffisant pour étudier les états de surface, il est nécessaire de lui apporter une série de corrections. Le nombre de corrections à apporter dépend du but poursuivi par l'utilisateur et du niveau de traitement des données récupérées (tableau suivant). Le tableau suivant récapitule les différents niveaux de corrections. Des détails de chacun de ces niveaux sont donnés par la suite. La théorie reste la même pour tous les capteurs, mais pour plus de clarté nous nous focaliserons plus spécifiquement sur le capteur Landsat (:ref:`data_Landsat`).

.. list-table:: Niveaux de produits d'images
   :widths: 25 15 15 15 15
   :header-rows: 1

   * - Grandeur mesurée
     - Phénomène impactant
     - Correction à apporter
     - Niveau de produit
     - Exemples de domaine d'utilisation
   * - Niveau d'énergie / puissance
     - Propriétés du capteur et orbite
     - Transformation en luminance
     - L1 (Landsat) (1)
     - Peu d'utilisation
   * - Luminance
     - Éclairement
     - Transformation en réflectance *TOA* (2)
     - --
     - Peu d'utilisation
   * - Réflectance *TOA*
     - Effets atmosphériques
     - Transformation en réflectance *BOA* (3)
     - L1C (Sentinel-2)
     - Étude mono-date
   * - Réflectance *BOA*
     - Effets topographiques et directionnels
     - Transformation en réflectance *de surface*
     - L2
     - Séries temporelles
   * - Réflectance de *surface* après corrections topographiques
     - Effets directionnels
     - --
     - Selon les missions
     - Étude en milieu montagneux
   * - Réflectance de *surface* après corrections des effets directionnels
     - --
     - --
     - Selon les missions
     - Qualité de l'eau

(1) Les images Sentinel-2 ne sont pas distribuées à ce niveau de correction au grand public.
(2) TOA pour *Top Of Atmosphere* (*Haut de l'Atmosphère*)
(3) BOA pour *Bottom Of Atmosphere* (*Bas de l'Atmosphère*), souvent assimilée par simplification à la réflectance de surface

Image en niveau d'énergie
..............................

Les images en *niveaux d'énergies* ne reflètent pas directement les états de la surface. Les niveaux d'énergie enregistrés par le capteur dépendent en effet de l'altitude du satellite, de sa vitesse de défilement, de la durée de la mesure et de l'angle solide de l'observation. Un même capteur à bord de deux satellites différents n'enregistrera pas le même niveau d'énergie pour une même surface si les deux satellites sont sur des orbites différentes (:numref:`angles-solides`).

.. figure:: figures/fig_angles_solides_observation.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: angles-solides
    
    Deux capteurs identiques pointant la même portion de surface. Le capteur A mesurera un plus grand niveau d'énergie que le capteur B de par son altitude moins élevée alors que la surface est la même.

Les pixels des images en niveaux d'énergie sont exprimées en *Comptes Numériques (CN)* aussi appelés *Digital Numbers (DN)* en anglais. Les valeurs des pixels s'étalent de 0 à plusieurs milliers selon l'encodage de l'image. Les images Landsat sont disponibles en niveaux d'énergie, elles sont alors en niveau *L1*, mais il est rare de travailler directement sur ces images. Il est dans la plupart des cas nécessaire de au moins les transformer en *réflectande haut de l'atmosphère (TOA)*. Les images Sentinel-2, quant à elles, ne sont pas distribuées à ce niveau pour le grand public. Pour transformer ces niveaux d'énergie en une grandeur réellement reliée à l'état de surface il faut convertir ce niveau d'énergie en *luminance spectrale*. L'USGS fournit une `documentation pour effectuer ces différents traitements`_.

..  _image-luminance:

Image en luminance
......................

Afin de rendre la mesure indépendante des propriétés physiques du capteur et de l'orbite du satellite, nous devons transformer les *Comptes Numériques* en *luminance spectrale*. Cette transformation est un calcul simple qui prend en entrée deux paramètres, un *gain* et un *offset*, fournis dans les métadonnées de l'image. La luminance se calcule pour chacune des bandes spectrales et les paramètres de gain et de offset peuvent varier d'une bande spectrale à l'autre. L'équation à appliquer est la suivante.

.. math::

   L_{\lambda} =  gain_{\lambda} * CN_{\lambda} + offset_{\lambda}

où :
 * L\ :sub:`λ`\ : luminance pour une bande spectrale donnée
 * gain\ :sub:`λ`\ : le paramètre de gain pour une bande spectrale donnée
 * CN\ :sub:`λ`\ : l'image en *Comptes Numériques* d'une bande spectrale donnée
 * offset\ :sub:`λ`\ : le paramètre de offset pour une bande spectrale donnée

Ce calcul peut tout à fait se faire à la main en utilisant simplement la calculatrice raster de notre logiciel de géomatique préféré. Pour une image Landsat 8, dans le fichier texte de métadonnées (*_MTL.txt*) le *gain* est connu sous le nom de *RADIANCE_MULT_BAND* et le offset par *RADIANCE_ADD_BAND* avec une déclinaison par bande (:numref:`gain-offset`).

.. figure:: figures/fig_L8_MTD_gain_offset.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gain-offset
    
    Gain et Offset d'une image Landsat 8 pour chacune des 8 bandes spectrales.

.. note::
   La *luminance* est aussi souvent vue sous son nom anglais de *radiance*.

Une fois l'image convertie en luminance, les valeurs des pixels sont directement liées à l'énergie effectivement réfléchie par la surface. Cependant, des bruits sont encore présents, notamment ceux liés à aux conditions d'éclairement de la scène au moment de la prise de vue. Ainsi, il est très rare de travailler avec des images en luminance. La luminance n'est qu'une étape pour calculer des *réflectances TOA*. Les images en luminance ne sont d'ailleurs jamais fournies directement.

Image en réflectance TOA
..........................

Sur une image en luminance, une partie de l'énergie enregistrée ne provient pas de la surface mais de l'éclairement directement reçu du Soleil (:numref:`toa-boa`). Il est donc nécessaire d'enlever cette composante pour analyser l'état de la surface. Cette correction peut se faire de deux façons différentes pour les images Landsat, une méthode historique et une méthode plus actuelle.

.. note::
   Les images Sentinel-2 sont directement fournis en réflectance TOA. Ce sont les images de niveau L1C.

**La méthode historique**

Cette correction se fait en prenant en entrée différents paramètres. Une simple équation, à appliquer à chacune des bandes avec les paramètres spécifiques, est à utiliser. Cette équation est la suivante.

.. math::

   ρ_{\lambda} = \frac{π * L_{\lambda} * d^2}{ESUN_{\lambda} * cos(θ_s)}

où :
 * ρ\ :sub:`λ`\ : réflectance TOA de la bande spectrale
 * L\ :sub:`λ`\ : luminance de la bande spectrale
 * d : la distance Terre - Soleil en `unités astronomiques`_ au moment d'acquisition
 * ESUN\ :sub:`λ`\ : éclairement exoatmosphérique de la bande spectrale
 * θ\ :sub:`s`\ : angle zénithal du Soleil

La luminance (L\ :sub:`λ`\) se calcule comme indiqué dans le paragraphe précédent (:ref:`image-luminance`). La distance Terre - Soleil du moment d'acquisition se trouve soit dans le fichier de métadonnées sous le nom *EARTH_SUN_DISTANCE* soit dans des `éphémérides trouvables en ligne`_. Le paramètre d'éclairement ESUN\ :sub:`λ`\ est une caractéristique du capteur à retrouver dans la `documentation du fournisseur`_. Enfin, l'angle zénithal est la différence entre l'angle droit (90°) et l'angle d'élévation du Soleil au moment de la prise de vue (*SUN_ELEVATION* du fichier de métadonnées) (:numref:`angle-zenithal`).

.. figure:: figures/fig_angle_zenithal.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: angle-zenithal
    
    Angle zénithal et angle d'élévation du Soleil.

.. note::
   Le cosinus de l'angle zénithal est aussi égal au sinus de l'angle d'élévation du Soleil.

**La méthode actuelle**

Avec les dernières générations de produits Landsat, il existe une `méthode plus directe`_ pour passer des images en Comptes Numériques en réflectance TOA. Cette méthode ne repose que sur des paramètres disponibles dans le fichier de métadonnées et s'affranchit du passage par la luminance et des paramètres ESUN\ :sub:`λ`\  et de distance Terre - Soleil. L'équation à employer est la suivante.

.. math::

   ρ_{\lambda} = \frac{M_{\lambda} * CN_{\lambda} + A_{\lambda}}{sin(θ_{se})}

où :
 * ρ\ :sub:`λ`\ : réflectance TOA de la bande spectrale
 * M\ :sub:`λ`\ : facteur multiplificatif pour la bande spectrale (*REFLECTANCE_MULT_BAND*)
 * A\ :sub:`λ`\ : facteur additif pour la bande spectrale (*REFLECTANCE_MULT_BAND*)
 * CN\ :sub:`λ`\ : l'image en Comptes Numériques de la bande spectrale
 * θ\ :sub:`se`\  : angle d'élévation du Soleil (*SUN_ELEVATION*)

.. warning::
   Depuis Landsat 8 il n'est plus possible d'utiliser la méthode historique car les paramètres ESUN associés aux capteurs ne sont plus diffusés par la NASA, même si `certains sites annexes`_ les répertorient. Il faut donc utiliser la méthode actuelle.

À ce niveau, que ce soit avec la méthode historique ou la méthode actuelle, nous avons des images exprimées en réflectance TOA (*Haut de l'Atmosphère*). Cette réflectance est représentative de la réflectance de la surface mais présente encore un *bruit* dû à la présence de l'atmosphère. En effet, l'atmosphère, de par sa composition physico-chimique va avoir tendance à atténuer ou brouiller l'énergie réfléchie par la surface. Il est donc conseillé de poursuivre les corrections en appliquant une *correction atmosphérique* qui va permettre d'obtenir une image en *réflectance BOA* (*Bottom Of Atmosphere*, *Réflectance Bas de l'Atmosphère*).

.. tip::
   Dans les logiciels de géomatique il existe souvent des modules qui font ces corrections radiométriques automatiquement (:ref:`correction-radiometrique`). Il n'y a donc pas à entre à la main les différentes équations. Le module SCP de QGIS le propose par exemple (:ref:`correction-radiometrique-SCP`).

Image en réflectance BOA
..........................

Pour se convaincre des effets atmosphériques il suffit de se mettre à l'ombre d'un arbre ou d'un bâtiment par une belle journée ensoleillée. Même en étant dans l'ombre, donc n'étant pas éclairé directement par le Soleil vous pourrez voir autour de vous. La zone à l'ombre sera plus obscure que la zone directement éclairée mais vous y verrez tout de même clairement. Cette clarté des zones en ombre vient du fait qu'une partie de l'éclairement solaire est réfléchi dans toutes les directions par l'atmosphère. Ainsi, le rayon lumineux qui vous parvient lorsque vous êtes à l'ombre, ne parvient pas directement du Soleil mais provient d'un rayon solaire qui a été réfléchi dans votre direction par une particule de l'atmosphère. C'est en somme un rayonnement indirect.

Il est intéressant de voir que sur la Lune, où il n'y a pas d'atmosphère, les zones en ombre sont totalement obscures, elles apparaissent comme des zones de néant. C'est largement visible sur les photos prises pendant les missions Apollo. Sur la figure suivante (:numref:`apollo17-ombres`) nous constatons que les zones dans l'ombre de l'astronaute et du Rover sont totalement noires, impossible d'y voir quoi que ce soit.

.. figure:: figures/fig_Apollo17_Cernan.jpg
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: apollo17-ombres
    
    Gene Cernan sur la Lune lors de la mission Apollo 17. Les zones en ombre sont totalement obscures. (Photo NASA)

Les deux principaux facteurs d'influence sont la *diffusion* et l'*absorption*. Le premier est surtout dû à la présence d'humidité dans l'atmosphère (:numref:`toa-boa`). Par une journée humide la diffusion sera plus importante que par une journée sèche. Le second est dû à la présence d'aérosols comme des poussières, du sable ou des suies. Les jours de vent de sable (dans les régions arides) ou les jours de fortes pollutions, le rayonnement réfléchi par la surface sera très atténué.

.. figure:: figures/fig_TOA_BOA.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: toa-boa
    
    Corrections radiométriques et atmosphériques. La correction radiométrique enlève les composantes 1 et 2 du rayonnement. La correction atmosphérique enlève (tout au moins réduit) la diffusion (3) et l'atténuation (4).

Par définition, les effets atmosphériques sont très fluctuants dans le temps car dépendants des conditions météorologiques locales et même variables d'un endroit à l'autre de la scène. Ils sont également dépendants de la pollution atmosphérique qui change elle aussi rapidement. Il n'est donc pas simple de corriger ces effets. Deux types de méthodes existent, celles basées sur des modèles *physiques* et celles basées sur des relations *empiriques* entre bandes spectrales de l'image.

Les méthodes à bases physiques sont gourmandes en données d'entrée et font appel à des données météos et de qualité de l'air. Ces données peuvent provenir de mesures sur le terrain mais aussi d'autres produits satellites orientés sur l'étude de l'atmosphère. Ces corrections ne sont pas simples à mettre en œuvre, mais ce sont celles-ci qui sont utilisées par les grands producteurs de données (USGS, ESA, ...). Ainsi, les images Landsat proposées en réflectance BOA sur les différents portails ont été produites sur la base de ces méthodes. La méthode à `base physique 6S`_ est souvent employée. Mais lorsque nous disposons d'une image en réflectance TOA et que nous souhaitons la transformer en réflectance BOA par nous mêmes, nous utilisons plutôt une méthode empirique de type *Dark Object Soustraction* (*DOS*).

Les méthodes *DOS* partent de l'hypothèse que certains pixels de l'image devraient être totalement sombres, i.e. avoir une réflectance à 0, dans certaines longueurs d'ondes. Typiquement, les pixels en eau devraient avoir une réflectance nulle dans l'infrarouge si il n'y avait pas d'effets atmosphériques. Ces méthodes ne sont pas parfaites mais donnent des résultats satisfaisants.

.. note::
    Il existe plusieurs méthodes *DOS*, dénommées *DOS1*, *DOS2*, etc. Pour les détails, consulter la `documentation de l'outil utilisé`_.

Il est possible d'appliquer ces corrections atmosphériques à l'aide de modules tout prêts dans les logiciels de géomatique (:ref:`correction-atmospherique`). Le module de SCP de QGIS propose une méthode *DOS* (:ref:`correction-atmospherique-SCP`).

.. tip::
    La réflectance *BOA* est souvent assimilée à la *Réflectance de Surface* et il est tout à fait possible de s'en contenter. Elle en est en effet une bonne approximation dans la plupart des cas. Mais en toute rigueur, il faut encore prendre en compte les effets topographiques et les effets spéculaires.

Des bandes spectrales aux signatures radiométriques
+++++++++++++++++++++++++++++++++++++++++++++++++++++

Au sein d'une même scène, les pixels sont spatialement cohérents, ils ont exactement les mêmes coordonnées géographiques. Ils se superposent parfaitement. Il est donc possible de "superposer" les différentes bandes spectrales et de tracer les signatures radiométriques d'un pixel en relevant ses valeurs d'énergies réfléchies dans chaque bande spectrale (:numref:`scene-bandes` B).

.. figure:: figures/fig_scene_signatures.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: scene-signatures
    
    Dérivation de signatures radiométriques (C) des pixels 1 et 2 (A) selon 4 bandes spectrales (B).

Dans notre exemple, nous allons tracer les signatures radiométriques des pixels *Px1* et *Px2* (:numref:`scene-bandes` A). Nous commençons par relever les valeurs des niveaux d'énergies réfléchies pour ces deux pixels dans chacune des bandes spectrales (:numref:`scene-bandes` B). Une fois ces valeurs relevées nous les traçons sur un graphique dont l'axe des ordonnées est le niveau d'énergie enregistré dans chaque bande spectrale et l'axe des abscisses correspond aux quatre bandes spectrales. Nous construisons ainsi les signatures radiométriques des deux pixels (:numref:`scene-bandes` C). Le travail est ensuite d'analyser ces signatures et d'en déduite la nature de la surface sous-jacente.

Le pixel 1 présente une faible valeur dans le bleu et le rouge mais un léger pic dans le vert. Il présente surtout un très fort pic dans le proche infrarouge. Cette signature est caractéristique de la végétation. Nous pouvons donc en déduire que le pixel 1 est couvert de végétation en bonne santé. Le pixel 2, quant à lui, présente un léger pic dans le domaine du bleu, puis diminue très vite et descend quasiment à 0 dans le proche infrarouge. Cette signature est typique de l'eau. Le pixel 2 correspond donc à une surface en eau.

Les quatre résolutions associées à un capteur
+++++++++++++++++++++++++++++++++++++++++++++++++

Chaque capteur à bord d'un satellite est caractérisé par quatre résolutions différentes : résolution *spatiale*, *spectrale*, *temporelle* et *radiométrique*.

La **résolution spatiale** correspond à la taille élémentaire de de la surface au sol mesurée par le capteur embarqué. Par exemple, un capteur présentant une résolution spatiale de 10 m, mesurera le rayonnement réfléchi par la surface sur un carré au sol de 10 m de côté. Le rayonnement enregistré par le capteur sera la moyenne de tous les rayonnements en provenance de ce carré de 10 mètres de côté. Tout objet inférieur à 10 m de côté ne sera pas perçu individuellement par le capteur. Lorsque les images sont fournies aux utilisateurs, la résolution des rasters fournis correspond à la résolution spatiale du capteur.

.. warning::
    Par abus de langage nous sommes tentés de dire que la résolution spatiale d'un capteur est égale à la résolution du raster de l'image mais c'est un raccourci à éviter. Ce sont en fait deux résolutions indépendantes mais que nous faisons la plupart du temps coïncider. Mais il est tout à fait possible de construire un raster à une résolution de 100 m ✕ 100 m en moyennant 100 pixels de 10 m de côté à partir d'une image issue d'un capteur à une résolution spatiale de 10 m. Au final, nous aurons bien une distinction entre la résolution spatiale du capteur (10 m) et la résolution du raster (100 m).

La **résolution spectrale** est, à l'origine, la largeur en termes de longueurs d'ondes, de chaque bande spectrale du capteur. En effet, lorsque nous parlons de *bande spectrale*, nous parlons en fait d'un intervalle entre deux longueurs d'ondes. Par exemple, la bande du proche infrarouge du capteur OLI sur Landsat 8 s'étale de de 0.85 à 0.88 µm, soit 0.33 µm alors que la bande du proche infrarouge de MSI sur Sentinel-2 s'étale de 0.8542 à 0.8752 µm soit 0.021 µm. La résolution spectrale de MSI est donc plus fine que celle de OLI pour cette bande. Par extension, il est fréquent également d'associer la résolution spectrale d'un capteur au nombre de bandes spectrales du capteur. Par exemple, un capteur disposant de 20 bandes spectrales aura ainsi une résolution spectrale de 20.

La **résolution temporelle** correspond à la fréquence de revisite d'un site, c'est-à-dire à la durée de temps nécessaire pour qu'un capteur survole deux fois la même zone. Une résolution temporelle fine correspond à un temps de revisite court. Cette résolution ne dépend pas capteur mais à l'orbite du satellite ainsi qu'à la latitude du point d'observation. Par exemple, le satellite Sentinel-3 à un temps de revisite de 1 à 2 jours selon la latitude. Concrètement ça signifie que le satellite passer tous les jours au-dessus d'une ville située proche de l'Équateur et tous les deux jours pour une ville située en Arctique.

La **résolution radiométrique** correspond à la sensibilité du capteur. Elle correspond à sa capacité à mesurer et à permettre la distinction dans une même bande spectrale de différences dans l'énergie réfléchie par la surface. Un capteur avec une résolution radiométrique très fine devra enregistrer un nombre de niveaux d'intensité plus important. Une résolution radiométrique fine demandera donc d'enregistrer les images dans un encodage permettant de stocker beaucoup de valeurs différentes.

Le segment sol
******************

Une fois ces mesures effectuées par le capteur à bord du satellite, les données sont stockées sur une mémoire interne mais elles doivent ensuite être transmises à la Terre pour être traitées par ce qu'on appelle le *segment sol*. Ce segment sol est l'ensemble des infrastructures à la surface terrestre qui vont réceptionner les images, les traiter, les calibrer et les distribuer auprès des scientifiques, des gestionnaires ou tout autre acteur.

La transmission des données
++++++++++++++++++++++++++++++

Au tout début de l'âge de la télédétection spatiale, les images étaient simplement des photographies prises sur pellicules. Ces pellicules étaient protégées dans une capsule blindée qui était alors éjectée vers la Terre. Lors de son entrée dans l'atmosphère un parachute était automatiquement déployé et un avion était en charge de la repérer dans le ciel et de la récupérer en plein vol grâce à une sorte de rateau qu'il traînait derrière lui. Ça demandait évidemment pas mal de logistique et quelques ratés étaient possibles. Cette technique était notamment celle employée par le programme d'espionnage étasunien `Corona`_ des années 1960 (:numref:`mission-corona`).

.. figure:: figures/fig_corona.jpg
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: mission-corona
    
    Un avion de l'US Air Force récupérant une capsule contenant des films Corona vers les années 1960 (Image NASA).

Bien sûr, aujourd'hui les données sont transmises par ondes radios de façon tout à fait dématérialisée. Cette transmission nécessite par conséquent un large réseau d'antennes réceptrices au sol. Ces antennes sont souvent localisées proches des régions polaires. Ces régions ont en effet l'avantage d'être survolées à chaque révolution du satellite. Le satellite peut ainsi se décharger de ses données à chaque orbite. Le volume de données étant très important, le réseau d'antennes doit être étendu afin de multiplier les points de réception (:numref:`segment-sol`).

.. figure:: figures/fig_segment_sol.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: segment-sol
    
    Carte des stations de réception utilisées par le programme Landsat (à gauche); station de réception de `Kiruna`_ en `Laponie`_ utilisée par l'ESA (à droite, image de l'ESA).

Une fois ces données réceptionnées, elles doivent être prétraitées et calibrées avant d'être distribuées à différents niveaux de corrections.

..  _theorie_telede_optique_distribution:

La distribution des données
++++++++++++++++++++++++++++++

Les satellite transmettent les données dans un niveau très brut intitulé *Niveau 0* ou *Level 0* en anglais, abrégé en *L0*. Ce niveau n'est pas proposé aux utilisateurs finaux. Avant distribution, les données sont au moins transformées en *niveau 1* (*L1*). À ce niveau les données sont fournies sous formes d'images orthorectifiées avec plus ou moins de finesse. Mais les valeurs associées aux pixels ne sont pas encore des réflectances mais des niveaux d'énergie exprimés en *comptes numériques* ou *Digital numbers* (*DN*). À ce stade, si l'utilisateur souhaite obtenir des images en réflectance en haut de l'atmosphère (*Top Of Atmosphere (TOA)*), il est nécessaire d'appliquer des corrections radiométriques. Ces corrections corrigent l'image en prenant en compte des paramètres comme l'inclinaison du Soleil sur l'horizon au moment de la prise de vue ou l'orientation du Soleil par rapport au capteur. Ces corrections se font grâce au fichier de métadonnées associé à chaque image. Il est ensuite possible d'obtenie une *réflectance de surface* ou *Top Of Canopy (TOC) reflectance* en appliquant en plus une correction atmosphérique. Cette correction permet de corriger les artefacts dus aux aérosols ou à l'humidité de la colonne d'air se trouvant sur le chemin de la surface au capteur. Cette correction peut se faire de façon empirique ou sur la base d'un modèle physique.

Les images sont fournies en réflectance à partir du *niveau 2* (*L2*). Il s'agît dans la plupart des cas de réflectances de surface (:numref:`im-c2l2`). L'intérêt c'est que les corrections radiométriques et atmosphériques ont déjà été appliquées par le fournisseur de l'image. La correction atmosphérique est généralement mieux faite par le fournisseur que par l'utilisateur. Parfois, une bande de *qualité* est associée à l'image. Dans cette bande de qualité, à chaque pixel est associée une valeur indiquant si il s'agît d'un pixel clair, nuageux, couvert d'aérosol ou de cirrus. Cette bande est utile pour faire un masque des pixels non valides avant de faire des traitements comme des classifications.

.. figure:: figures/fig_C2L2_Landsat.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: im-c2l2
    
    Image en réflectance en haut de l'atmosphère (TOA, à gauche), en réflectance de surface (SR, au centre) et en température de surface (à droite). L'image en réflectance de surface est nettement plus contrastée. Image issue de la documentation de la NASA sur les images de la `Collection 2 Level 2`_.

Il existe également un *niveau 3* (*L3*) qui correspond à un produit plus élaboré qu'une simple image en réflectance. Par exemple, il peut s'agir d'une image composite sur une période donnée, où à chaque pixel est associée la meilleure valeur de réflectance de la période. Les images composites de cette sorte présentent l'avantage de minimiser la présence de nuages. Par contre, ces images mélangent des réponses spectrales de dates différentes. Les produits de niveaux 3 peuvent également être les surfaces en eau dérivées de l'image, ou des indices comme le NDVI.

Selon missions la définition des niveaux peut légèrement changer. Il est bien de se référer à la documentation associée aux images pour être sûr. Pour plus de détails sur les corrections et leurs mise en œuvre, vous pouvez vous référer aux fiches correspondantes.

Les grandes missions d'observation de la Terre
************************************************

Il existe de très nombreuses missions d'observation de la Terre par satellite, mais quelques unes sont plus importantes ou plus utilisées que d'autres. Le tableau suivant présente quelques missions dont les images sont très employées. Seules les missions de télédétection optique et consacrées à l'étude de la surface sont présentées.

.. list-table:: Quelques grandes missions satellites d'observation de la surface
   :widths: 15 15 15 15 15 15 15
   :header-rows: 1

   * - Mission
     - Période
     - Résolution (m)
     - Bandes
     - Temps de retour
     - Agence
     - Accès
   * - Landsat
     - 1972 - →
     - 15 - 90
     - 4 - 11
     - ≈ 16 jours
     - NASA
     - Libre
   * - SPOT 1- 5
     - 1986 - 2015
     - 10 - 20
     - 4 + 2 PAN
     - 
     - CNES
     - Restreint
   * - SPOT 6- 7
     - 2012 - →
     - 1.5 - 6
     - 4 + PAN
     - 
     - CNES
     - Restreint
   * - Sentinel-2
     - 2015 - →
     - 10 - 20
     - 13
     - ≈ 5 jours
     - ESA
     - Libre
   * - Pléiades
     - 2011 - →
     - 0.5 - 2
     - 4 + PAN
     - ≈ 26 jours
     - CNES
     - Restreint
   * - Sentinel-3
     - 2014 - →
     - 300
     - 21
     - 1 à 2 jours
     - ESA
     - Libre
   * - MODIS
     - 1999 - →
     - 250 - 1000
     - 36
     - 1 à 2 jours
     - NASA
     - Libre
   * - ResourceSat
     - 2003 - →
     - 5.8 - 56
     - 4
     - 
     - ISRO
     - Libre

La résolution spatiale peut différer d'une bande à l'autre d'un même capteur et change avec l'évolution des missions. Par exemple, les premières images Landsat avaient une résolution spatiale de 90 m mais seulement de 15 ou 30 mètres aujourd'hui. Vous trouverez plus de détails sur les missions Landsat sur la fiche dédiée : :ref:`data_Landsat` et pour Sentinel-2 : :ref:`data_Sentinel_2`.

Missions d'observation de la Terre
-------------------------------------

Il existe de nombreux programmes d'observations de la Terre par satellites. Nous présenterons ici quelques un des programmes les plus utilisés en géographie ainsi que quelques portails Web qui permettent de télécharger les images des missions correspondantes.

.. contents:: Table des matières
    :local:

..  _data_Landsat:
              
Les images Landsat
***********************

Le programme Landsat est un programme étasunien d'observation de la Terre depuis l'espace. Il est maintenu depuis 1972 par la NASA et l'USGS (*United States Geological Survey*). C'est le programme le plus employé en télédétection spatial. Cette fiche présente un bref historique du programme ainsi que les principales caractéristiques des images au cours du temps.

Bref historique
+++++++++++++++++

Les missions Landsat ont débuté en 1972, avec Landsat 1, et se sont poursuivies jusqu'à aujourd'hui (presque) sans interruption (:numref:`chrono-Landsat`). Ce programme est voué à continuer avec la mise sur Orbite de Lanbsat 9 en septembre 2021. Grâce à ce programme nous disposons de près de 50 ans de données relatives à l'observation des surfaces terrestres depuis l'espace. De plus, ces images sont mises à disposition du grand public librement et gratuitement au travers de portails très ergonomiques. La résolution spatiale des images est de 30 mètres, depuis le capteur TM mis en service en 1982, ce qui est déjà largement suffisant pour beaucoup d'applications. Ces caractéristiques spatiales et temporelles font que ces images sont utilisées dans tous les domaines d'application possibles : suivi de l'usage des sols, hydrologie, glaciologie, géologie, suivi des risques ...

.. figure:: figures/fig_chrono_Landsat.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: chrono-Landsat
    
    Chronologie des missions Landsat.

Il est important de noter que les satellites Landsat (1 à 9) sont les plateformes et que les capteurs portent des noms différents. Ainsi, à bord des premières générations de Landsat 1 à Landsat 3, nous retrouvons le capteur *MSS* qui possède certaines caractéristiques physiques. Ensuite, à bord des Landsat 4 et 5, lancés en 1982 et 1984, nous retrouvons conjointement les capteurs *MSS* et *TM* (*Thematic Mapper*). Le capteur *TM* est plus évolué que le *MSS* et présente des résolutions radiométriques et spatiales plus fines. Ensuite, à bord de Landsat 6, un nouveau capteur fait son apparition *ETM*. Mais malheureusement Landsat 6 n'est pas parvenu à se mettre en orbite et a donc échoué. Suit Landsat 7, lancé en 1999, avec à son bord le capteur *ETM+* (*Enhanced Thematic Mapper +*). Ce satellite a pris des images jusqu'au 6 avril 2022, mais suite à une défaillance survenue en mai 2003, ses images sont difficilement exploitables (:numref:`landsat7-issues`). Ainsi, les deux satellites en orbite sont Landsat 8 et Landsat 9. Ils embarquent respectivement les capteurs *OLI* (*Operational Land Imager*) et *TIRS* (*Thermal Infrared Sensor*) pour l'infrarouge thermique pour Landsat 8 et *OLI-2* et *TIRS-2* pour Landsat 9. Ces deux capteurs *OLI* et *OLI-2* (tout comme *TIRS* et *TIRS-2*) sont identiques en termes de bandes spectrales. Notons que nous mélangeons fréquemment les noms des images, nous pouvons parler *d'images Landsat 9* ou *d'images OLI-2* de façon interchangeable.

.. figure:: figures/fig_Landsat_7_issues.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: landsat7-issues
    
    Problème sur l'optique de ETM+ à bord de Landsat 7. Seule la bande centrale est nette, les reste de l'image est striée de bandes sans données.

Nous pouvons noter la très longue longévité de Landsat 5, près de 28 ans d'observations à lui tout seul. Entre 1984 et 1999 le capteur MSS a acquis des prises de vue en parallèle du capteur TP. Puis, à partir de 1999,  seul le capteur TM a été maintenu en activité. Ce capteur est tombé en panne en novembre 2011 et les ingénieurs de la NASA ont réussi à remettre en route le capteur MSS en juin 2012 bien qu'il soit resté éteint pendant 12 ans. Ainsi, de juin 2012 à septembre 2013 nous retrouvons des images MSS. Malgré cette remise en route, du fait de la défaillance des capteurs TM et ETM+, l'année 2012 est un *trou* dans les données Landsat. La figure suivante résume les disponibilités temporelles de chaque capteur (:numref:`chrono-capteurs`).

.. figure:: figures/fig_chrono_capteurs_Landsat.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: chrono-capteurs
    
    Disponibilité temporelle des différents capteurs Landsat.

.. note::
	Derrière toutes ces données numériques, il y a bien sûr des femmes et des hommes et notamment `Virginia Tower Norwod`_ qui a mis au point le capteur MSS de Landsat 1 à la fin des années 1960.


Caractéristiques
+++++++++++++++++++

Le capteur MSS avait une résolution spatiale de 60 m et depuis le capteur TM la résolution spatiale est de 30 m. Si la résolution spatiale n'évolue plus, la résolution radiométrique, elle, évolue. Au cours des générations, de nouvelles bandes spectrales sont disponibles. Un des gros avantages des images Landsat est leur superposition parfaite au cours du temps. Un pixel TM de 30 mètres sur mètres de 1984 est spatialement rigoureusement identique au pixel de 30 mètres sur 30 mètres d'une image OLI prise en 2021 au même endroit. Cette concordance spatiale permet une analyse temporelle très robuste.

Les tableaux suivants résument les caractéristiques radiométriques des différents capteurs.

..  _data_Landsat_MSS:

MSS - Landsat 1-5
++++++++++++++++++++++

.. list-table:: Capteur MSS. *PIR : Proche Infrarouge ; MIR Moyen Infrarouge*
   :widths: 15 15 25 25 25
   :header-rows: 1

   * - Capteur
     - Bande
     - Domaine
     - λ (µm)
     - Résolution (m)
   * - MSS
     - 1
     - Vert
     - 0.5 - 0.6
     - 60
   * - MSS
     - 2
     - Rouge
     - 0.6 - 0.7
     - 60
   * - MSS
     - 3
     - PIR
     - 0.7 - 0.8
     - 60
   * - MSS
     - 4
     - MIR
     - 0.8 - 1.1
     - 60

..  _data_Landsat_TM:
     
TM - Landsat 4-5
++++++++++++++++++++++

.. list-table:: Capteur TM. *PIR : Proche Infrarouge ; MIR Moyen Infrarouge*
   :widths: 15 15 25 25 25
   :header-rows: 1

   * - Capteur
     - Bande
     - Domaine
     - λ (µm)
     - Résolution (m)
   * - TM
     - 1
     - Bleu
     - 0.45 - 0.52
     - 30
   * - TM
     - 2
     - Vert
     - 0.52 - 0.60
     - 30
   * - TM
     - 3
     - Rouge
     - 0.63 - 0.69
     - 30
   * - TM
     - 4
     - PIR
     - 0.76 - 0.90
     - 30
   * - TM
     - 5
     - MIR 1
     - 1.55 - 1.75
     - 30
   * - TM
     - 6
     - Thermique
     - 10.40 - 12.50
     - 120 (30)
   * - TM
     - 7
     - MIR 2
     - 2.08-2.35
     - 30

La résolution spatiale de la bande 6 (Thermique) est de 120 m mais est rééchantillonnée à 30 m a posteriori.

..  _data_Landsat_ETM+:

ETM+ - Landsat 7
++++++++++++++++++++++

.. list-table:: Capteur ETM+. *PIR : Proche Infrarouge ; MIR Moyen Infrarouge*
   :widths: 15 15 25 25 25
   :header-rows: 1

   * - Capteur
     - Bande
     - Domaine
     - λ (µm)
     - Résolution (m)
   * - ETM+
     - 1
     - Bleu
     - 0.45 - 0.52
     - 30
   * - ETM+
     - 2
     - Vert
     - 0.52 - 0.60
     - 30
   * - ETM+
     - 3
     - Rouge
     - 0.63 - 0.69
     - 30
   * - ETM+
     - 4
     - PIR
     - 0.77 - 0.90
     - 30
   * - ETM+
     - 5
     - MIR 1
     - 1.55 - 1.75
     - 30
   * - ETM+
     - 6.1
     - Thermique
     - 10.40 - 12.50
     - 60 (30)
   * - ETM+
     - 6.2
     - Thermique
     - 10.40 - 12.50
     - 60 (30)
   * - ETM+
     - 7
     - MIR 2
     - 2.09 - 2.35
     - 30
   * - ETM+
     - 8
     - Panchromatique
     - 0.52 - 0.90
     - 15

Une bande panchromatique apparaît avec une résolution spatiale plus fine, 15 m au lieu de 30 m. La bande thermique (10.40 µm - 12.50 µm) est acquise deux fois, une fois en *high gain* et une en *low gain*. Cette même bande a une résolution spatiale de 60 m mais est rééchantillonnée à 30 m a posteriori.

..  _data_Landsat_OLI:

OLI/TIRS - Landsat 8 / OLI-2/TIRS-2 - Landsat 9
++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. list-table:: Capteurs OLI/TIRS et OLI-2/TIRS-2. *PIR : Proche Infrarouge ; MIR Moyen Infrarouge*
   :widths: 15 15 25 25 25
   :header-rows: 1

   * - Capteur
     - Bande
     - Domaine
     - λ (µm)
     - Résolution (m)
   * - OLI
     - 1
     - Aérosol côtier
     - 0.43-0.45
     - 30
   * - OLI
     - 2
     - Bleu
     - 0.45 - 0.51
     - 30
   * - OLI
     - 3
     - Vert
     - 0.53 - 0.59
     - 30
   * - OLI
     - 4
     - Rouge
     - 0.64 - 0.67
     - 30
   * - OLI
     - 5
     - PIR
     - 0.85 - 0.88 	
     - 30
   * - OLI
     - 6
     - MIR 1
     - 1.57 - 1.65
     - 30
   * - OLI
     - 7
     - MIR 2
     - 2.11 - 2.29
     - 30
   * - OLI
     - 8
     - Panchromatique
     - 0.50 - 0.68
     - 15
   * - OLI
     - 9
     - Cirrus
     - 1.36 - 1.38
     - 30
   * - TIRS
     - 10
     - Thermique 1
     - 10.6 - 11.19
     - 100
   * - TIRS
     - 11
     - Thermique 2
     - 11.50 - 12.51
     - 100

La bande 1 de OLI (OLI-2) n'est plus le Bleu mais une bande nommé *Coastal aerosol* utilisé notamment pour le suivi des littoraux. De ce fait, les bandes Bleu, Verte et Rouge sont respectivement les bandes 2, 3 et 4. La bande panchromatique à une résolution de 15 m est la bande 8.

..  _data_Landsat_path_row:

Path and Row
++++++++++++++

Comme toutes les images satellites, les images Landsat sont founies sous forme de *scènes*. Une scène coorespond à une portion de la surface terrestre telle que vue par Landsat. Dit autrement, la surface terrestre est divisée  en colonnes, appelées *Paths* et en lignes, appelées *Rows*. À l'intersection d'une ligne et d'une colonne nous trouvons une scène. Ce découpage permet de fournir des images dans un référentiel stable dans le temps. Par exemple, la ville de Paris se trouve sur la scène à l'intersection du path 199 et du row 26. Ainsi, si nous voulons voir l'évolution de Paris au cours du temps, il suffira de télécharger toutes les dates des images Landsat pour le couple path and row 199 - 26 (:numref:`path-row`).

.. figure:: figures/fig_Landsat_Path_and_Row_France.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: path-row
    
    Path and Row (WRS 2) Landsat au-dessus de la France métropolitaine. En orange le *path* 199 et en bleu le *row* 26. En rouge, la scène à l'intersection de ce path et row dans lequel nous trouvons Paris.

Sur la figure précédente, nous trouvons l'ensemble des scènes qui recouvrent la France métropolitaine. Le path 199 est représenté en orange et le row 26 en bleu. À l'intersection des deux nous trouvons la scène qui contient Paris, en rouge. Les shapefiles et les kml des paths et rows du monde entier sont disponibles `à ce lien`_. L'extraction pour la France métropolitaine peut être téléchargée ici :download:`Path and Row WRS 2 Fance <_static/WRS2_Descending_France.gpkg>`

Il existe en fait deux systèmes de path et row différents, un premier pour les orbites *descendantes* (*descending*) et un pour les orbites *ascendantes* (*ascending*). Cela s'explique par le fait que les satellites tournent autour de la Terre en passant par les pôles. Au-dessus de la face éclairée de la planète, le satellite se déplace du Pôle Nord vers le Pôle Sud. Inversement, au-dessus de la face à l'ombre, il se déplace du Pôle Sud vers le Pôle Nord. C'est pourquoi les images Landsat de jour, c'est-à-dire l'immense majorité des images, sont référencées dans le système d'orbite descendante. Les quelques (rares) images Landsat de nuit seront quant à elles référencées dans le système d'orbite ascendante.

.. warning::
	Le système de path et row n'a pas toujours été le même pour tous les satellites de Landsat. De Landsat 1 à 3, il s'agissait du WRS 1 (*world Reference System 1*) et depuis Landsat 4 nous sommes en WRS 2. Par conséquent nous aurons la plupart du temps affaire au WRS 2. Il faudra simplement être vigilant si nous souhaitons manipuler les plus anciennes des images Landsat, leur système de path et row sera différent.

..  _data_Landsat_traitements:

Niveaux de traitements
++++++++++++++++++++++++++

Les images Landsat sont distribuées sous différents niveaux de traitements. Pour une généralité sur les niveaux de traitements vous pouvez vous référer à la fiche :ref:`theorie_telede_optique_distribution`. Les images Landsat sont disponibles en `niveau L1`_. c'est-à-dire en comptes numériques, en niveau *L2* i.e. en réflectance et en niveau *L3* soit des produits déjà plus élaborés comme les surfaces en eau extraites des images.

..  _data_Landsat_L1:

Niveau L1
.............

Le niveau L1 correspond à une image orthorectifiée. C’est-à-dire une image qui a été corrigée pour prendre en compte les distorsions relatives à l’optique du capteur et celles liées au relief terrestre. Néanmoins, cette ortho-rectification peut avoir été obtenue selon trois méthodes différentes, de la plus robuste à la moins robuste.

**Niveau L1TP** : Pour ce niveau, les images ont été corrigées sur la base d’un Modèle Numérique de Terrain (MNT ou DEM en anglais) et de points de contrôle (*Ground Control Points, GCP*) mesurés directement sur le terrain. Il s’agît de la correction la plus robuste du niveau L1.

**Niveau L1GT** : Pour ce niveau, seul un MNT a été utilisé pour corriger les distorsions géométriques. Aucun point de contrôle n’a été utilisé. Ce niveau est souvent le meilleur possible lorsque la couverture nuageuse est trop importante, lorsque l’image est prise de nuit, ou qu’il s’agît d’une petite île isolée.

**Niveau L1GS** : Pour ce niveau, aucune donnée externe, ni MNT ni point de contrôle, n’a été utilisée pour les corrections. Seules les données enregistrées par le capteur ont été utilisées. Il s’agît du niveau de traitement le moins fiable des trois. En pratique ce sont essentiellement les zones d’inlandsis comme l’Antarctique qui sont fournies sous ce niveau de traitement.

.. note::
	Il est rare de tomber sur une image qui ne soit pas au moins en *L1GT*. Néanmoins il peut arriver de tomber sur en *L1GS*, dans ce cas, le décalage par rapport à la réalité peut être de plusieurs dizaines de kilomètres. Si votre image ne tombe pas sur la zone présentie, vérifiez son niveau de traitement, c'est souvent la cause du problème.

Lors de la fourniture d'une image, ces trois niveaux sont regroupés dans deux catégories, appelées *Tier* : *Tier 1 (T1)* et *Tier 2(T2)*. La catégorie *Tier 1* regroupe les "meilleures" images de niveau *L1TP*. La catégorie *Tier 2* regroupe les moins bonnes images *L1TP* ainsi que toutes les images de nivau *L1GT* et *L1GS*. Au final, si nous souhaitons n'utiliser que les images à la qualité la plus sûre, i faut utiliser celles du *Tier 1*.

Les images en niveau L1 sont téléchargeables sur le portail *EarthExplorer* dans le menu :menuselection:`Data Sets --> Landsat --> Landsat Collection 2 Level-1`.

Si des images en niveau L1 sont choisies, l'utilisateur devra faire au moins une correction radiométrique afin d'obtenir la réflectance en haut de l'atmosphère (*TOA*) avant toute analyse. Une correction atmosphérique pourra également être faite afin d'obtenir des images en réflectance de surface.

..  _data_Landsat_L2:

Niveau L2 - Réflectances de surface
......................................

Les images de `niveau L2`_ sont non seulement ortho-rectifiées mais fournies en réflectance de surface. Les corrections radiométriques et atmosphériques ont été réalisées par l'organisme fournisseur. Autant les corrections radiométriques sont faciles à mettre en œuvre, autant les corrections atmosphériques sont plus délicates. L'avantage avec ce niveau est que ces corrections atmosphériques ont été faites grâce à des modèles élaborés. C'est le niveau à privilégier pour des études diachroniques. Notons que la bande panchromatique n'est pas fournie en réflectance de surface.

.. tip::
	En plus des réflectances de surface, les températures de surface sont également fournies. Ces températures sont dérivées des bandes 10 et 11 (de OLI) correspondant à l'infrarouge thermique. La fourniture de ce produit est appréciable car il n'est pas évident à construire par soi-même.

Les images en niveau L2 sont téléchargeables sur le portail *EarthExplorer* dans le menu :menuselection:`Data Sets --> Landsat --> Landsat Collection 2 Level-2`.

.. warning::
	Les données fournies en Level 2 ne sont pas directement exploitables. Les réflectances de surface associées aux pixels ont été "étalées" sur une gamme de valeurs permettant de stocker des valeurs entières et non réelles (des nombres entiers et pas des nombres à virgules), voir ci-après pour les détails.

Avant de manipuler les images en niveau L2, les valeurs de chaque pixel doivent être rapportées à une "vraie" valeur de réflectance comprise entre 0 et 1. Selon `la documentation`_ fournie par l'USGS, cette transformation se fait en multipliant la valeur de chaque pixel, de chaque bande spectrale, par 0.0000275 et en lui ôtant 0.2, comme indiqué sur l'équation suivante.

.. math::
    RS = pixel \times 0.0000275 - 0.2

Avec *RS* pour *Réflectance de Surface*

Cette relation n'est valable que pour les pixels qui sont initialement dans la gamme de validité. C'est-à-dire dont la valeur est comprise entre 7273 et 43636 pour toutes les bandes sauf la bande 6. Pour cette dernière, la gamme de validité est de 1 à 65535 (cf page 12 de `la documentation`_). Après application de l'équation, les pixels en dehors de la plage de validité auront des valeurs légèrement inférieures à 0 ou légèrement supérieures à 1. Il sera alors possible de forcer ces valeurs à 0 ou 1.

.. tip::
	Nous proposons un modèle QGIS qui permet de faire ce ré-étalonnage facilement et ce forçage à 0 et 1, à télécharger ici : :download:`Landsat C2L2 <_static/Landsat_C2L2_RS.model3>` (faire un clic droit et *Enregistrer le lien sous...*). Une fois le modèle téléchargé, il suffit d'aller dans la *Boîte à outils de traitements*, de cliquer sur l'icône *Modèles* |icone_modeles|, de sélectionner ``Ajouter un modèle à la boîte à outils...`` et d'aller chercher le modèle téléchargé nommé *Landsat_C2L2_RS.model3*. Le modèle se trouve alors dans l'arborescence ``Modèles``. Il n'y a plus qu'à l'exécuter. Il est même possible de l'exécuter par lot afin de traiter plusieurs bandes spectrales en même temps.

En plus des bandes en réflectance de surface, une bande de contrôle de qualité est également fournie. Dans cette bande, chaque pixel est associée à une valeur qui renseigne sur la qualité de la mesure pour le pixel donné. Pour chaque pixel il est ainsi possible de savoir si ce pixel était masqué par des nuages, des cirrus ou encore des aérosols. Cette bande est très utile pour ne réaliser les traitements que sur les pixels effectivement valides. Les correspondances entre la valeur du pixel et sa qualité sont fournies dans `la documentation`_ à la page 14.

Enfin, un avantage de ce produit en niveau L2 est la possibilité de ne télécharger que la ou les bandes spectrales d'intérêt. En effet, contrairement au niveau L1 où nous sommes obligés de télécharger l'ensemble des bandes spectrales et des fichiers associés, ici nous pouvons simplement télécharger les bandes 4 et 5 si nous ne sommes intéressés que par un calcul de NDVI.

..  _data_Landsat_L2_ST:

Niveau L2 - Températures de surface
......................................

À partir de la mise en route du capteur TM, Landsat capture des images dans l'infrarouge thermique. À partir de ces données enregistrées dans cette bande, il est possible de dériver des températures de surface (*Land Surface Temperature* ou *LST* en anglais). Ce calcul n'est pas évident, mais le niveau *L2* fournit un raster déjà exprimé en température de surface. Il s'agit de la bande *B10* du niveau *L2*. Le nom de la bande se termine d'ailleurs par le suffixe *ST* comme *Surface Temperature*.

.. note::
	La température de surface n'est pas la température de l'air. C'est simplement la température telle qu'on pourrait la mesurer en appliquant un thermomètre directement au contact de la surface. C'est souvent une température plus chaude que celle de l'air. Enfin, il faut bien garder en tête que c'est une température instantanée prise au moment du passage du satellite et non une température représentative d'une journée.

Il faut simplement faire attention à recalibrer les valeurs avant d'utiliser ce produit. En effet, comme pour les réflectances de surface *L2*, le raster a été modifié afin de stocker des entiers et non pas de flottants. Un simple calcul permet de retransformer ce raster en *vrai* raster de températures de surface. Ce calcul est décrit dans `la documentation`_ officielle des produits *L2* (page 12). Le calcul à effectuer est le suivant :

.. math::
    TS = pixel \times 0.00341802 + 149

Avec *TS* pour *Température de Surface*, exprimée en kelvins.

.. warning::
	Notons bien que ce calcul fournit les températures de surface en kelvin. Or il est souvent plus pertinent de travailler en degrés Celsius. Il suffit donc de retrancher 272.15 au calcul précédent.

Ainsi, nous appliquons le calcul suivant pour directement obtenir un raster des températures de surface exprimés en degrés Celsius :

.. math::
    TS = pixel \times 0.00341802 + 149 - 272.15

..  _data_Landsat_L3:

Niveau L3
.............

Les images de niveau L3 sont des produits prêts à être analysés. Il peut s'agir de l'extraction `des surfaces en eau`_, `du couvert neigeux`_ ou `des surfaces brûlées`_. Ces images sont à réserver pour des analyses très précises. Les images en niveau L3 sont téléchargeables sur le portail *EarthExplorer* dans le menu :menuselection:`Data Sets --> Landsat --> Landsat Collection 1 Level-3`.

Métadonnées et nom des images Landsat
++++++++++++++++++++++++++++++++++++++++

Lorsque des images Landsat sont téléchargées, nous récupérons en fait un répertoire dans lequel se trouve un raster,au format *.tif*, par bande spectrale, plus éventuellement un ou plusieurs rasters de *qualité*. Nous retrouvons également un fichier texte avec l'extension *.MTL* qui contient des métadonnées utiles aux pré-traitements.

Les métadonnées
..................

Ce fichier *.MTL* est un fichier texte contenant diverses informations sur les conditions d'acquisition de l'image. Nous y retrouvons des informations relatives au SCR employé, l'angle du Soleil au-dessus de l'horizon et par rapport au nord, l'heure de prise de vue, les conditions nuageuses, ... C'est sur ce fichier que se basera le calcul des *corrections radiométriques* pour passer des *comptes numériques* à la *réflectance en haut de l'atmosphère (TOA)*.

..  _nom_landsat`:

Nom des images Landsat
........................

Le nom des images suivent les conventions suivantes : LXSS_LLLL_PPPRRR_YYYYMMDD_yyyymmdd_CC_TX, où :

* L = Landsat
* X = Capteur (*C* = OLI/TIRS combinés, *O* = OLI seul, *T* = TIRS seul, *E* = ETM+, *T” = *TM, *M* = MSS)
* SS = Satellite (*07* = Landsat 7, *08* = Landsat 8)
* LLL = Niveau de traitement (L1TP/L1GT/L1GS)
* PPP = WRS path
* RRR = WRS row
* YYYYMMDD = Date d'acquisition au format année, mois, jour
* yyyymmdd - Date de traitement au format année, mois, jour
* CC = Numéro de collection (01 ou 02)
* TX = Catégorie de collection (*RT* = Temps réel, *T1* = Tier 1, *T2* = Tier 2)

Puis à la fin du nom des rasters nous retrouvons le numéro de bande de *B1* à *B11* par exemple. Pour les produits de niveau L2, nous trouvons un *SR* qui signifie *Surface Reflectance* juste avant le numéro de bande.

Télécharger les images Landsat
+++++++++++++++++++++++++++++++++

Les images Landsat sont disponibles notamment sur le portail `EarthExplorer`_ de l'USGS (:ref:`data_portail_images`).

..  _data_Sentinel_2:

Les images Sentinel-2
************************

Le programme Sentinel-2 est un programme d'observation de la Terre par satellite piloté par l'Agence Spatiale Européenne (ESA). La mission, lancée en juin 2015, est toujours en activité et est prévue pour durer. Cette mission est constituée de deux satellites quasi identiques : Sentinel-2A et Sentinel-2B (:numref:`vue-S2`). Chaque satellite a un temps de retour sur une même zone de 10 jours. Le temps de retour est donc de 5 jours en combinant les deux satellites. Sentinel-2A a été mis en orbite le 23 juin 2015 et Sentinel-2B le 7 mars 2017.

.. figure:: figures/fig_Sentinel-2.jpg
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: vue-S2
    
    Image d'artiste d'un satellite Sentinel-2 (ESA).

..  _data_Sentinel_2_MSI:

Capteur MSI
+++++++++++++++++++
Les deux satellites Sentinel-2 embarquent le capteur *Multi Spectral Instrument* (*MSI*). Ce capteur prend des images dans 13 bandes spectrales différentes à des résolutions spatiales allant de 10 mètres à 20 mètres ou 60 mètres selon la bande considérée.


.. list-table:: Capteur MSI. *PIR : Proche Infrarouge ; MIR Moyen Infrarouge*
   :widths: 15 25 25 25	25
   :header-rows: 1

   * - Bande
     - Domaine
     - λ centrale (nm)
     - largeur λ (nm)
     - Résolution (m)
   * - 1
     - "Côtier"
     - 442.7
     - 21
     - 60
   * - 2
     - Bleu
     - 492.4
     - 66
     - 10
   * - 3
     - Vert
     - 559.8
     - 36
     - 10
   * - 4
     - Rouge
     - 664.6
     - 31
     - 10
   * - 5
     - Red Edge 1
     - 704.1
     - 15
     - 20
   * - 6
     - Red Edge 2
     - 740.5
     - 15
     - 20
   * - 7
     - Red Edge 3
     - 782.8
     - 20
     - 20
   * - 8
     - PIR
     - 832.8
     - 106
     - 10
   * - 8A
     - PIR étroit
     - 864.7
     - 21
     - 20
   * - 9
     - Vapeur d'eau
     - 945.1
     - 20
     - 60
   * - 10
     - MIR Cirrus
     - 1373.5
     - 31
     - 60
   * - 11
     - MIR 1
     - 1613.7
     - 91
     - 20
   * - 12
     - MIR 2
     - 2202.4
     - 175
     - 20

Ce capteur est très intéressant de par son temps de retour très court, sa résolution spatiale fine et ses nombreuses bandes spectrales, notamment dans les domaines du *Red Edge* et du proche infrarouge. Cette bonne résolution radiométrique fait de ce capteur un très bon outil pour le suivi de la végétation et des cultures.

Tuiles Sentinel-2
+++++++++++++++++++++
Les images Sentinel-2 sont capturées selon des *tuiles* régulières dans l'espace, sur le même principe que les *Path and Row* de Landsat. Ainsi, si nous nous intéressons à l'évolution d'une région donnée dans le temps, nous pouvons télécharger la tuile de la région pour les différentes dates d'intérêt. La logique d'organisation est un peu moins claire que dans le cas de Landsat. Chaque tuile est fournie dans une projection UTM / WGS84 en utilisant le `Military Grid Reference System`_ (MGRS). Dans ce système, le globe est découpée en 60 *tranches* dans le sens Nord Sud. Chaque *tranche* mesure donc 6° de longitude (:math:`60 \times 6°`). Puis chaque *tranche* est découpée en *bandeau* dans le sens de la latitude. Chaque bandeau mesure 8° de latitude sauf pour les régions les plus proches des Pôles. Ensuite, chaque zone ainsi définie est découpée en tuiles de 100 km :math:`\times` 100 km. Chacune de ces tuiles est codée par deux lettres indiquant la position sur l'axe Ouest - Est puis Sud - Nord.

Par exemple, la ville de Paris se trouve sur la tuile **31UDQ** qui se comprend de la façon suivante :

* *31* signifie que nous sommes sur la zone UTM 31
* *U* signifie que nous sommes sur la latitude du bandeau *U*
* *D* indique la position de la tuile sur l'axe Ouest - Est au sein de cette zone 31U
* *Q* indique la position de la tuile sur l'axe Sud - Nord au sein de cette zone 31U

Des détails peuvent être trouvés sur le site de `Kalideos`_ et du `Cesbio`_. Et vous trouverez ici les tuiles Sentinel-2 pour la France métropolitaine :download:`Tuiles Sentinel-2 Fance <_static/S2_tuiles_France.gpkg>`

..  _data_Sentinel_2_niveaux:

Niveaux de traitements Sentinel-2
+++++++++++++++++++++++++++++++++++++
Les images Sentinel-2 sont distribuées sous trois (principaux) différents niveaux de traitements. Pour chacun de ces traitements, les images sont ortho-rectifiées : 

* *Niveau L1C* : les images sont corrigées des effets radiométriques, il s'agît ainsi des réflectances en haut de l'atmosphère (TOA). C'est notamment le niveau fourni sur le portail `EarthExplorer`_. Une correction atmosphérique peut être ensuite faite manuellement par l'utilisateur pour obtenir les réflectances de surface.

* *Niveau L2A* : les images sont corrigées des effets radiométriques et atmosphériques. Il s'agît donc des réflectances de surface. Un masque des nuages et des ombres des nuages est en plus fourni. Ce niveau est disponible sur le `site de Theia`_.

* *Niveau L2B* : il s'agît de variables géophysiques dérivées des images comme le LAI, le fAPAR, la teneur en chlorophylle, ...

* *Niveau L3A* : il s'agît de synthèses temporelles de réflectances de surface sur une période donnée, par exemple mensuelle. Sur ce type d'image, chaque pixel est la "meilleure" valeur mesurée sur un mois donné. Ça permet d'obtenir une image en réflectance de surface sans nuages, sans ombres de nuages et sans aérosols (si au moins une image non nuageuse a été trouvée sur le mois bien sûr). Le désavantage est que chaque pixel peut présenter une valeur de réflectance d'une date différente du pixel voisin. Ce type d'images est donc bien pour le suivi des évolutions relativement lentes comme l'urbanisation ou la déforestation mais ne sera pas suffisant pour le suivi des évolutions rapides comme les inondations ou les incendies. Ce niveau L3A est également disponible sur le `site de Theia`_.

Télécharger les données Sentinel-2
++++++++++++++++++++++++++++++++++++++
Ces images sont librement distribuées par l'ESA. Il est donc possible de les télécharger et de les utiliser librement dans tout contexte. Les images sont téléchargeables sur le portail de `données de Theia`_, `celui de l'ESA`_ ou `EarthExplorer`_. (:ref:`data_telecharger_S2`).

..  _data_portail_images:
              
Télécharger des images satellites
--------------------------------------

Il existe différents portails pour télécharger plus ou moins facilement et plus ou moins gratuitement des images satellites. Les images les plus utilisées sont celles issues du programme Landsat (NASA) suivies, entre autres, des images issues des programmes Sentinel (ESA). Ces deux programmes mettent leurs images à disposition du grand public gratuitement. D'autres images, comme celles du programme MODIS (NASA) sont également gratuites. Nous présentons ici quelques portails utiles pour télécharger ces images.

EarthExplorer
*****************
Le portail `EarthExplorer`_ est sans aucun doute le portail le plus complet et le plus ergonomique. Il est maintenu conjointement par la NASA et l'`USGS`_ (*United States Geological Survey*). Il met à disposition de très nombreuses données parmi lesquelles les images Landsat, à différents niveaux de traitements, les images MODIS, les images Sentinel-2 et le MNT global SRTM. L'utilisation du portail et le téléchargement des données est gratuit mais une inscription est obligatoire. Cette inscription nécessite une adresse mail qui fera office de login. Le portail se présente comme montré sur la figure suivante (:numref:`portail-eo`).

.. figure:: figures/fig_portail_eo.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: portail-eo
    
    La page d'accueil du portail EarthExplorer.

Sur cette page d'accueil nous trouvons en haut à droite le menu pour s'identifier (*Login*, 1), les onglets qui vont nous permettre de rentrer des paramètres de recherche et d'accéder aux images (2) et un fond de carte qui va nous permettre de sélectionner une zone géographique (3). Une fois identifié, notre identifiant apparaît en haut à droite et nous pouvons maintenant explorer les données et les télécharger.

Emprise géographique
++++++++++++++++++++++++
La première chose à faire est de sélectionner l'emprise géographique sur laquelle nous souhaitons obtenir des images. Cette sélection se passe dans le premier onglet ``Search Criteria``, en utilisant soit le panneau de l'onglet ``Geocoder`` soit le panneau de l'onglet ``Polygon`` (:numref:`eo-localisation`).

.. figure:: figures/fig_portail_eo_localisation.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-localisation
    
    Sélection de l'emprise géographique.

L'onglet ``Geocoder`` permet de zoomer directement soit sur une entité géographique via le menu ``Feature (GNIS)``, soit sur une adresse via le menu ``Adress/Place``, soit par un identifiant de Path and Row Landsat via le menu ``Path/Row``. Dans ce même panneau, il est également possible d'importer un fichier KML ou shapefile de notre emprise via l'onglet ``KML\Shapefile Upload``.

Mais la méthode la plus simple est de zoomer sur son emprise dans le panneau central et d'utiliser l'onglet ``Polygon`` du deuxième panneau. Par exemple, si nous souhaitons des images sur Ouagadougou (Burkina), nous zoomons sur cette ville, puis dans l'onglet ``Polygon``, nous sélectionnons le menu ``Use Map``. L'emprise visible à l'écran est ainsi définie comme zone de sélection et apparaît en rouge (:numref:`eo-selection`).

.. figure:: figures/fig_portail_eo_selection.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-selection
    
    Sélection de l'emprise géographique selon la carte visible à l'écran.

.. note::
	Cette méthode de sélection selon l'emprise de la vue cartographique est vraiment la plus simple à utiliser.

Il est ensuite possible de peaufiner manuellement cette zone de sélection. Il suffit de dézoomer un peu puis de déplacer à l'aide de la souris les coins du rectangle (:numref:`eo-selection-2`). Cette manipulation peut être utile pour restreindre le champ de recherche. En effet, toutes les images qui intersectent, même légèrement, le polygone de sélection seront proposées au téléchargement.

.. figure:: figures/fig_portail_eo_selection_2.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-selection-2
    
    Modification manuelle de la zone de sélection.

Nous pouvons noter qu'il est également possible de définir une zone de sélection circulaire via l'onglet ``Circle``. Il faut alors entrer les coordonnées décimales du centre du cercle et le rayon du cercle en mètres. Il est aussi possible d'importer un shapefile de la zone d'intérêt depuis notre ordinateur via l'onglet ``Predefined Area``. Notons que ce shapefile ne doit pas contenir trop de sommets.

.. warning::
	La zone de sélection ne doit pas être trop étendue car dans les résultats, pas plus de 100 images sont proposées à la fois au téléchargement.

Emprise temporelle et nuages
++++++++++++++++++++++++++++++++
Une fois l'emprise géographique définie, il est nécessaire de paramétrer l'emprise temporelle souhaitée. Cela se fait avec le panneau qui contient l'onglet ``Date Range`` (:numref:`eo-date`). 

.. figure:: figures/fig_portail_eo_date.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-date
    
    Emprise temporelle de la recherche d'images.

La date de début de la période qui nous intéresse doit être renseignée dans la case ``Search from:`` et la date de fin dans la case ``to:``. Il n'est pas nécessaire d'entrer les dates à la main, il est possible d'utiliser le fonction de calendrier disponible en cliquant sur l'icône |icone_eo_calendrier|. Il est également possible de ne chercher des images que pour certains mois de cette période via le menu ``Search months:``. C'est une option intéressante si nous souhaitons des images seulement en saison sèche, ou au moment du développement des cultures, ou juste des images hivernales ...

Dans ce même panneau, il est bien de préciser quelle couverture nuageuse maximale nous autorisons dans les images proposées. Dans l'idéal, nous souhaitons 0 nuage mais dans les faits c'est assez rare, d'autant plus que les scènes Landsat ou Sentinel-2 (et encore plus MODIS), couvrent des portions de territoires relativement importantes. Cette valeur peut être mise à 15 ou 20 % dans un premier temps (*Cloud Cover Range: 0% - 15%*). Si 20 % de l'image est couverte de nuages, il y a une chance pour que notre zone d'intérêt se situe dans les 80 % sans nuages. Évidemment ce critère a une importance différente selon la zone géographique. Pour Ouagadougou, nous trouverons facilement une image sans nuages mais pour Singapour en zone équatoriale, ce sera beaucoup plus compliquée. C'est à l'utilisateur de jongler avec ce seuil.

.. tip::
	Même si certains nuages sont présents sur notre zone d'étude, il existe des méthodes pour les *masquer* à posteriori et ainsi les enlever de l'analyse de télédétection qui sera faite à partir de l'image.

Choix des images à télécharger
+++++++++++++++++++++++++++++++++++
Une fois ces réglages préliminaires effectués, il est maintenant temps de choisir les images que nous souhaitons télécharger. Ce choix s'effectue dans l'onglet ``Data Sets``, deuxième onglet du panneau de gauche. Comme nous pouvons le voir, il y a de très (très) nombreux types d'images disponibles. Il est rare de tous les utiliser, nous allons simplement présenter rapidement comment télécharger des données Landsat, Sentinel-2, MODIS et SRTM.

Images Landsat
...................
Les images Landsat sont un peu la raison d'être de ce portail et ce sont probablement les images les plus téléchargées. Pour y avoir accès, il faut déplier le menu ``Landsat``. Ensuite les deux sous menus qui vont nous intéresser sont ``Landsat Collection 2 Level-2`` et ``Landsat Collection 2 Level-1`` (:numref:`eo-Landsat`). Des détails sur les images Landsat et leurs caractéristiques sont donnés sur la fiche dédiée : :ref:`data_Landsat`.

.. figure:: figures/fig_portail_eo_Landsat.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-Landsat
    
    Choix des images Landsat.

**Landsat Collection 2 Level-2**
    
Les images du sous menu ``Landsat Collection 2 Level-2`` correspondent aux images en réflectances de surface, déjà corrigées des effets radiométriques et atmosphériques. Des détails sur les niveaux de traitements des images Landsat sont donnés sur la fiche dédiée : :ref:`data_Landsat_traitements`. Nous y trouvons les images Landsat 8 OLI, Landsat 7 ETM+ et Landsat 4-5 TM. Des informations détaillées sur les niveaux de traitements et autres aspects sont disponibles en cliquant sur l'icône d'information |icone_eo_information|. Il est également possible de voir la couverture mondiale d'un produit en cliquant sur l'icône sous forme de carte |icone_eo_emprise| (:numref:`eo-L7`). Les images téléchargées de cette collection sont accompagnées d'un raster localisant les pixels sous nuages, ombres de nuages ou aérosols, appelé *raster de qualité*. Ce qui permet de les éliminer lors de l'analyse.

.. figure:: figures/fig_portail_eo_emprise_L7.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-L7
    
    Emprise mondiale des images ETM+ en Level-2.

.. warning::
	Les images fournies dans la Collection Level-2 ne sont pas utilisables telles quelles. Il est nécessaire de leur appliquer un étalonnage (simple), tel que décrit dans la section consacrée aux différents niveaux de traitements des images Landsat : :ref:`data_Landsat_L2`.

**Landsat Collection 2 Level-1**

Les images du sous menu ``Landsat Collection 2 Level-1`` correspondent aux images en radiance sur lesquelles c'est à l'utilisateur de faire les corrections radiométriques et atmosphériques. Les images téléchargées de cette collection contiennent tout de même un raster localisant les pixels se trouvant sous les nuages ou les ombres des nuages. Notons que pour les images les plus anciennes, c'est-à-dire les images Landsat 1-5 MSS, seul le niveau L1 est disponible. Le portail ne met pas à disposition ces images directement en réflectance de surface.

.. note::
	**Quelle collection choisir ?** Dans la plupart des cas, la collection en Level 2 correspondant aux réflectances de surface est un bon choix. Les algorithmes de corrections utilisés par l'USGS et la NASA sont performants. L'intérêt du Level 1 est de rester maître des corrections et savoir exactement quelle méthode est employée.

Mettons que nous souhaitons des images Landsat 8 et 7 en réflectance de surface sur notre zone de Ouagadougou, il suffit de sélectionner les lignes *Landsat 8 OLI/TIRS C2 L2* et *Landsat 7 ETM+ C2 L2* dans le sous menu ``Landsat Collection 2 Level-2``. Il ne reste plus qu'à cliquer sur le menu ``Results >>`` en bas du panneau de gauche. Les résultats de la recherche sont alors visibles dans l'onglet ``Results`` (:numref:`eo-results-Landsat`).

.. figure:: figures/fig_portail_eo_results_Landsat.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-results-Landsat
    
    Résultats de la recherche des images Landsat sur la zone d'intérêt.

Les résultats sont affichés par défaut 10 par 10 et pas plus de 100 résultats sont affichés à la fois. Si la requête dépasse les 100 résultats, il faut découper l'emprise spatiale ou temporelle en plusieurs parties. Les résultats sont classés par capteur. Sur la figure précédente (:numref:`eo-results-Landsat`), ce sont les images Landsat 8 OLI qui correspondent aux critères de recherche qui sont présentées. Nous accédons aux images Landsat 7 ETM+ demandées en dépliant le menu qui se trouve sous ``Data Set``. Les résultats sont rangés du plus récent au plus ancien et une petite vignette donne un aperçu de chaque scène. Pour chaque scène nous trouvons son identifiant (*ID*), sa date d'acquisition (*Date Acquired*), son *Path* et son *Row*. En cliquant sur l'icône d'empreinte |icone_eo_empreinte|, nous pouvons voir l'emprise de la scène par rapport à notre zone d'intérêt, et en cliquant sur l'icône d'image |icone_eo_image| nous pouvons afficher un aperçu de la scène en vraies couleurs afin de se rendre compte visuellement de sa qualité, surtout vis-à-vis des nuages (:numref:`eo-empr-aper`). 

.. figure:: figures/fig_portail_eo_empreinte_apercu.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-empr-aper
    
    Empreinte (A) et aperçu (B) d'une scène Landsat par rapport à la zone d'intérêt.

En cliquant sur l'icône en forme de feuille de papier |icone_eo_metadonnees|, nous pouvons accéder aux métadonnées de l'image, comme le pourcentage précis de couverture nuageuse. Pour télécharger l'image souhaitée, il suffit alors de cliquer sur l'icône de téléchargement |icone_eo_telecharger|. La fenêtre de téléchargement s'ouvre (:numref:`eo-choix`).

.. figure:: figures/fig_portail_eo_choix.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-choix
    
    Choix de téléchargement d'une scène Landsat en Level 2.

Une scène Landsat de cette Collection 2 Level-2 va contenir de nombreux rasters et fichiers texte de métadonnées. Parmi ces rasters, deux grands produits sont disponibles : les réflectances de surface et les températures de surface. Il est possible de télécharger tous les rasters, i.e. toutes les bandes spectrales et les rasters annexes, concernant les réflectances de surface en cliquant sur ``Download All Files Now`` de la ligne ``Level-2 Surface Reflectance Bands``. Mais l'archive correspondante sera très volumineuse car elle contiendra toutes les bandes spectrales ainsi que différents rasters de qualité, 13 rasters au total. Il est plus pertinent de ne télécharger que les bandes spectrales et les rasters qui nous intéressent, en les choisissant en cliquant sur ``Select Files``. Par exemple, si nous souhaitons simplement calculer un NDVI, nous n'avons besoin que des bandes du rouge et du proche infrarouge soit les bandes 4 et 5 de Landsat 8. Nous pouvons également télécharger le raster de qualité afin de masquer les nuages et leurs ombres. Au final nous ne téléchargeons que 3 rasters au lieu de près d'une vingtaine. Il suffit de télécharger les rasters *...B4.tif*, *...B5.tif* et *...QA_PIXEL.tif* (raster de qualité) en les sélectionnant, comme présenté dans la figure suivante (:numref:`eo-select-bands`).

.. figure:: figures/fig_portail_eo_select_bands.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-select-bands

    Choix des rasters à télécharger.

Il n'y a plus qu'à cliquer sur ``Download All Selected Now`` et une archive *zip* contenant les rasters sélectionnés est téléchargé.

.. warning::
	Les bandes panchromatiques ne sont pas distribuées en réflectance de surface et ne sont donc pas proposées au téléchargement dans cette Collection 2 Level-2. Il faut aller les récupérer depuis la Collection 2 Level-1.

.. note::
	Le téléchargement des images de la *Collection 2 Level-1* se fait de la même manière.

Pour les températures de surface, la manipulation est identique, il suffit de choisir la ligne ``Level-2 Surface Temperature Bands`` de la fenêtre de téléchargement (:numref:`eo-choix`). Dans cette même fenêtre, il est également possible de télécharger les bands une par une en cliquant sur ``Product Option``, la fenêtre suivante s'ouvre et il suffit alors de télécharger la bande qui nous intéresse (:numref:`eo-bande-par-bande`).

.. figure:: figures/fig_portail_eo_telechargement.png
    :width: 55em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-bande-par-bande

    Téléchargement bande par bande.

Les rasters des bandes spectrales et autres produits Landsat téléchargés sont au format GeoTIFF. Ces rasters sont géoréférencés en WGS84 UTM zone (:ref:`intro-SCR`). Une image Landsat de Paris sera par exemple en WGS84 UTM zone 31 Nord (EPSG 32631).


Images MODIS
...............
Il est également possible de télécharger des images MODIS via ce portail. Le paramétrage de l'emprise géographique et temporelle se fait de la même manière que pour les images Landsat. Une fois les paramètres réglés, dans l'onglet ``Data Sets``, il suffit de déplier le menu ``NASA LPDAAC Collections`` pour avoir accès à tous les produits MODIS. De très nombreux produits sont dérivés des images MODIS aussi bien pour les surfaces continentales, les océans que l'atmosphère. Chaque produit est identifié par un code. La signification des codes est fourni sur le site des `produits MODIS`_. Ainsi, si nous souhaitons par exemple télécharger les images en réflectance de surface à une résolution de 250 m agrégées sur 8 jours, nous déplions le sous menu ``MODIS Land Surface Reflectance`` et choisissons les produits *MOD09Q1* (satellite *Terra*) et *MYD09Q* (satellite *Aqua*) (:numref:`eo-Modis`).

.. figure:: figures/fig_portail_eo_Modis.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-Modis
    
    Arborescence des produits MODIS.

Les images MODIS sont ici téléchargées au format *.hdf* qui est un format propre aux images MODIS. Ce format est néanmoins géré par la plupart des logiciels de géomatique sans problèmes.

MNT SRTM
............
Via ce portail, il est aussi possible de télécharger le MNT global issue de la mission SRTM. Ce MNT, connu simplement sous le nom de *SRTM*, a été relevé au mois de février 2000 grâce à un capteur radar embarqué sur la navette spatiale étasunienne. Comme il s'agît d'un produit pris une seule fois, il n'est pas nécessaire de régler l'emprise temporelle, seule l'emprise spatiale importe. Elle se définit comme pour les images satellites. Plusieurs niveaux de traitements du SRTM sont disponibles. Certains sont à 90 m de résolution et d'autres à 30 m. Certains présentent des valeurs manquantes, i.e. des trous, et d'autres ont été traités pour combler ces valeurs manquantes.

Les différents niveaux de traitements sont disponibles dans l'onglet ``Data Sets``, dans le sous menu ``Digital Elevation`` puis dans ``SRTM`` (:numref:`eo-srtm`). Le niveau le plus abouti est le SRTM à 30 m de résolution sans valeurs manquantes (ou peu). Ce niveau est nommé *SRTM 1 Arc-Second Global*. Au téléchargement, il est conseillé de choisir le format GeoTIFF. Le raster téléchargé est en *EPSG:4326 - WGS 84* et correspond à un territoire de à peu près 100 km sur 100 km.

.. figure:: figures/fig_portail_eo_srtm.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: eo-srtm
    
    Arborescence des produits SRTM.

.. |icone_eo_calendrier| image:: figures/icone_eo_calendrier.png
              :width: 20 px

.. |icone_eo_information| image:: figures/icone_eo_information.png
              :width: 20 px

.. |icone_eo_emprise| image:: figures/icone_eo_emprise.png
              :width: 20 px

.. |icone_eo_empreinte| image:: figures/icone_eo_empreinte.png
              :width: 15 px

.. |icone_eo_image| image:: figures/icone_eo_image.png
              :width: 15 px

.. |icone_eo_metadonnees| image:: figures/icone_eo_metadonnees.png
              :width: 20 px

.. |icone_eo_telecharger| image:: figures/icone_eo_telecharger.png
              :width: 20 px

.. |icone_eo_telecharger_bande| image:: figures/icone_eo_telecharger_bande.png
              :width: 70 px

.. |icone_modeles| image:: figures/icone_modeles.png
              :width: 20 px

.. _EarthExplorer: https://earthexplorer.usgs.gov/
.. _USGS: https://www.usgs.gov/
.. _produits MODIS: https://modis.gsfc.nasa.gov/data/dataprod/
.. _documentation du fournisseur: https://www.usgs.gov/landsat-missions/using-usgs-landsat-level-1-data-product
.. _méthode plus directe: https://www.usgs.gov/landsat-missions/using-usgs-landsat-level-1-data-product
.. _certains sites annexes: https://www.gisagmaps.com/landsat-8-atco/
.. _documentation pour effectuer ces différents traitements: https://www.usgs.gov/landsat-missions/using-usgs-landsat-level-1-data-product
.. _unités astronomiques: https://www.futura-sciences.com/sciences/definitions/univers-unite-astronomique-63/
.. _éphémérides trouvables en ligne: https://www.usgs.gov/media/files/earth-sun-distance-astronomical-units-days-year
.. _base physique 6S: https://grass.osgeo.org/grass78/manuals/i.atcorr.html
.. _documentation de l'outil utilisé: https://grass.osgeo.org/grass78/manuals/i.landsat.toar.html
.. _EarthExplorer: https://earthexplorer.usgs.gov/
.. _Virginia Tower Norwod: https://www.technologyreview.com/2021/06/29/1025732/the-woman-who-brought-us-the-world/
.. _à ce lien: https://www.usgs.gov/core-science-systems/nli/landsat/landsat-shapefiles-and-kml-files
.. _la documentation: https://www.usgs.gov/media/files/landsat-8-9-collection-2-level-2-science-product-guide
.. _niveau L1: https://www.usgs.gov/centers/eros/science/usgs-eros-archive-landsat-archives-landsat-8-9-operational-land-imager-and?qt-science_center_objects=0#qt-science_center_objects
.. _niveau L2: https://www.usgs.gov/centers/eros/science/usgs-eros-archive-landsat-archives-landsat-8-9-olitirs-collection-2-level-2?qt-science_center_objects=0#qt-science_center_objects
.. _des surfaces en eau: https://www.usgs.gov/centers/eros/science/usgs-eros-archive-landsat-landsat-level-3-dynamic-surface-water-extent-dswe?qt-science_center_objects=0#qt-science_center_objects
.. _du couvert neigeux: https://www.usgs.gov/centers/eros/science/usgs-eros-archive-landsat-landsat-level-3-fractional-snow-covered-area-fsca?qt-science_center_objects=0#qt-science_center_objects
.. _des surfaces brûlées: https://www.usgs.gov/centers/eros/science/usgs-eros-archive-landsat-landsat-level-3-burned-area-ba-science-product?qt-science_center_objects=0#qt-science_center_objects
.. _données de Theia: https://catalogue.theia-land.fr/
.. _celui de l'ESA: https://scihub.copernicus.eu/dhus/#/home
.. _EarthExplorer: https://earthexplorer.usgs.gov/
.. _Military Grid Reference System: https://en.wikipedia.org/wiki/Military_Grid_Reference_System#/media/File:Universal_Transverse_Mercator_zones.svg
.. _Kalideos: https://littoral.kalideos.fr/drupal/fr/content/tuilage-sentinel-2
.. _Cesbio: https://labo.obs-mip.fr/multitemp/les-tuiles-de-sentinel-2-comment-ca-marche/
.. _site de Theia: https://catalogue.theia-land.fr/
.. _façon officielle: https://fr.wikipedia.org/wiki/Spectre_%C3%A9lectromagn%C3%A9tique#Usages_et_classification
.. _longueurs d'ondes qui correspondent aux principales couleurs: https://fr.wikipedia.org/wiki/Spectre_visible#Longueurs_d'onde_approximatives_des_couleurs_spectrales
.. _Ixion: https://climserv.ipsl.polytechnique.fr/ixion/
.. _image Wikimedia Commons: https://commons.wikimedia.org/wiki/File:628643main_Fam_Portrait_466_2.jpg
.. _Corona: https://en.wikipedia.org/wiki/Corona_%28satellite%29
.. _Kiruna: https://www.esa.int/Enabling_Support/Operations/ESA_Ground_Stations/Kiruna_station
.. _Laponie: https://www.google.fr/maps/place/67%C2%B051'25.7%22N+20%C2%B057'51.6%22E/@68.2471108,15.2092732,724017m/data=!3m1!1e3!4m5!3m4!1s0x0:0x0!8m2!3d67.8571278!4d20.964325?hl=fr
.. _Collection 2 Level 2: https://www.usgs.gov/core-science-systems/nli/landsat/landsat-collection-2-level-2-science-products
